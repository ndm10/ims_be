﻿using IMS.Business;
using IMS.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IMS.Presentation;

// [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Manager + "," + SD.Role_Interviewer + "," + SD.Role_HR)]
[Route("api/[controller]")]
public class DashboardController : ControllerBase
{
    private readonly IDashboardService _dashboardService;

    public DashboardController(IDashboardService dashboardService)
    {
        _dashboardService = dashboardService;
    }

    [HttpGet]
    public IActionResult GetListCardItem()
    {
        var response = new BaseSuccess<List<CardItemDTO>>();
        var cardItemCandidate = _dashboardService.GetCandidatesParameter();
        var cardItemJob = _dashboardService.GetJobsParameter();
        var cardItemInterview = _dashboardService.GetInterviewsParameter();
        var cardItemOffer = _dashboardService.GetOffersParameter();
        response.Data = new List<CardItemDTO>{
            cardItemCandidate,
            cardItemJob,
            cardItemInterview,
            cardItemOffer
        };
        return Ok(response);
    }

    [HttpGet("countInterview")]
    public IActionResult CountInterview(int dateType)
    {
        var response = new BaseSuccess<List<ChartDTO>>();
        var countInterview = _dashboardService.CountInterview(dateType);
        response.Data = countInterview;
        return Ok(response);
    }

    [HttpGet("successCandidate")]
    public IActionResult SuccessInterviews(int dateType)
    {
        var response = new BaseSuccess<List<ChartDTO>>();
        var countInterview = _dashboardService.SuccessCandidate(dateType);
        response.Data = countInterview;
        return Ok(response);
    }
}
