﻿using IMS.Business;
using IMS.Business.DTO.Candidate;
using IMS.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IMS.Presentation;

[Authorize(Roles = SD.Role_Admin + "," + SD.Role_Manager + "," + SD.Role_Interviewer + "," + SD.Role_HR)]
[Route("api/[controller]")]
[ApiController]
public class CandidateController : ControllerBase
{
    private readonly ICandidateService _candidateService;

    public CandidateController(ICandidateService candidateService)
    {
        _candidateService = candidateService;
    }

    [HttpGet("{id}")]
    public IActionResult GetCandidateById(Guid id)
    {
        var response = new BaseSuccess<CandidateDTO>();
        var candidate = _candidateService.GetById(id, "Attrs,Position,OwnerHR");
        if (candidate == null)
        {
            response.IsSuccess = false;
            response.MessageResponse = "Candidate not found";
        }
        else
        {
            response.Data = candidate;
        }
        return Ok(response);
    }

    [HttpGet("search")]
    public IActionResult GetCandidateBySearchAndStatus(string? search, CandidateStatusEnum? status, int pageIndex, int pageSize)
    {
        var response = new BaseSuccess<Paginated<CandidateListDTO>>();
        var candidates = _candidateService.GetCandidateBySearchAndStatus(search, status, pageIndex, pageSize);
        if (candidates == null)
        {
            response.IsSuccess = false;
            response.MessageResponse = "List candidates not found";
        }
        else
        {
            response.Data = candidates;
        }
        return Ok(response);
    }

    [HttpGet("without-ban")]
    public IActionResult GetCandidatesWithoutBanStatus()
    {
        var response = new BaseSuccess<List<CandidateOfferDTO>>();
        var candidates = _candidateService.GetCandidatesWithoutBanStatus();
        if (candidates == null)
        {
            response.IsSuccess = false;
            response.MessageResponse = "List candidates not found";
        }
        else
        {
            response.Data = candidates;
        }
        return Ok(response);
    }

    [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Manager + "," + SD.Role_HR)]
    [HttpPost]
    public IActionResult CreateCandidate([FromForm] CandidateCreateDTO candidateCreateDTO)
    {
        var response = new BaseSuccess<CandidateDTO>();
        var newCandidate = _candidateService.Add(candidateCreateDTO);
        if (newCandidate == null)
        {
            response.IsSuccess = false;
            response.MessageResponse = "Can't add new candidate";
        }
        else
        {
            response.Data = newCandidate;
        }
        return Ok(response);
    }

    [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Manager + "," + SD.Role_HR)]
    [HttpPut("update")]
    public IActionResult UpdateCandidate([FromForm] CandidateUpdateDTO candidateUpdateDTO)
    {
        var response = new BaseSuccess<string>();
        var isUpdate = _candidateService.Update(candidateUpdateDTO);
        if (!isUpdate)
        {
            response.IsSuccess = false;
            response.MessageResponse = "Can't update candidate";
        }
        else
        {
            response.Data = "Update candidate successfully";
        }
        return Ok(response);
    }

    [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Manager + "," + SD.Role_HR)]
    [HttpPut("ban")]
    public IActionResult BanCandidate(Guid id)
    {
        var response = new BaseSuccess<string>();
        var isBan = _candidateService.BanCandidate(id);
        if (!isBan)
        {
            response.IsSuccess = false;
            response.MessageResponse = "Candidate has already banned";
        }
        else
        {
            response.Data = "Ban candidate successfully";
        }
        return Ok(response);
    }

    [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Manager + "," + SD.Role_HR)]
    [HttpDelete]
    public IActionResult DeleteCandidate(Guid id)
    {
        var response = new BaseSuccess<string>();
        var isDelete = _candidateService.Delete(id);
        if (!isDelete)
        {
            response.IsSuccess = false;
            response.MessageResponse = "Can't delete candidate";
        }
        else
        {
            response.Data = "Delete candidate successfully";
        }
        return Ok(response);
    }

    [Authorize(Roles = SD.Role_Admin + "," + SD.Role_Manager + "," + SD.Role_HR)]
    [HttpGet("filter-fullname-and-status")]
    public IActionResult GetCandidate(string? textSearch,int? status, int? pageIndex, int? pageSize)
    {
        var response = _candidateService.GetCandidateByFilterByFullName(textSearch,status, pageIndex, pageSize);
        if(!response.IsSuccess)
        {
            return BadRequest(response.MessageResponse);
        }
        return Ok(response);
    }
}
