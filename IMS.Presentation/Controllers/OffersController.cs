﻿using IMS.Business;
using IMS.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;

namespace IMS.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OffersController : ControllerBase
    {
        private readonly IOfferService _offerService;
        private readonly IEmailSender _mailService;
        public OffersController(IOfferService offerService, IEmailSender mailService)
        {
            _offerService = offerService;
            _mailService = mailService;
        }
        [HttpPost]
        public async Task<IActionResult> CreateOffer(OfferCreateDTO offerCreateDTO)
        {
            var response = new BaseSuccess<OfferDTO>();
            var newOffer = await _offerService.AddAsync(offerCreateDTO);
            if (newOffer == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can't add new offer";
            }
            else
            {
                response.Data = newOffer;
            }
            return Ok(response);
        }
        [HttpGet("{id}")]
        public IActionResult GetOfferById(Guid id)
        {

            var response = new BaseSuccess<OfferDTO>();
            var offer = _offerService.GetById(id);
            if (offer == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Offer not found";
            }
            else
            {
                response.Data = offer;
            }
            return Ok(response);
        }

        [HttpGet("search")]
        //[Authorize(Roles = $"{SD.Role_HR},{SD.Role_Manager},{SD.Role_Admin}")]
        public IActionResult GetOfferBySearchAndStatus([FromQuery] string? search, [FromQuery] OfferStatusEnum? status, [FromQuery] Guid? departmentId, [FromQuery] int pageIndex, [FromQuery] int pageSize)
        {
            var response = new BaseSuccess<Paginated<OfferDTO>>();
            var paginatedResult = _offerService.GetOfferBySearchAndStatus(search, status, departmentId, pageIndex, pageSize);
            if (paginatedResult == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "List offer not found";
            }
            else
            {
                response.Data = paginatedResult;
            }
            return Ok(response);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateOffer(OfferUpdateDTO offerUpdateDTO)
        {
            var response = new BaseSuccess<string>();
            var isUpdate = await _offerService.UpdateAsync(offerUpdateDTO);
            if (!isUpdate)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can't update offer";
            }
            else
            {
                response.Data = "Update offer successfully";
            }
            return Ok(response);
        }
        [HttpPatch("approve/{id}")]
        [Authorize(Roles = $"{SD.Role_Manager},{SD.Role_Admin}")]
        public async Task<IActionResult> ApproveOffer(Guid id)
        {
            var response = new BaseSuccess<string>();
            var isUpdate = await _offerService.UpdateStatusOfferAsync(id, OfferStatusEnum.Approved);
            if (!isUpdate)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can't approve offer";
            }
            else
            {
                response.Data = "Approve offer successfully";
            }
            return Ok(response);
        }

        [HttpPatch("reject/{id}")]
        [Authorize(Roles = $"{SD.Role_Manager},{SD.Role_Admin}")]
        public async Task<IActionResult> RejectOffer(Guid id)
        {
            var response = new BaseSuccess<string>();
            var isUpdate = await _offerService.UpdateStatusOfferAsync(id, OfferStatusEnum.Rejected);
            if (!isUpdate)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can't reject offer";
            }
            else
            {
                response.Data = "Reject offer successfully";
            }
            return Ok(response);
        }

        [HttpPatch("cancel/{id}")]
        [Authorize(Roles = $"{SD.Role_HR},{SD.Role_Manager},{SD.Role_Admin}")]
        public async Task<IActionResult> CancelOffer(Guid id)
        {
            var response = new BaseSuccess<string>();
            var isUpdate = await _offerService.UpdateStatusOfferAsync(id, OfferStatusEnum.Cancelled);
            if (!isUpdate)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can't cancel offer";
            }
            else
            {
                response.Data = "Cancel offer successfully";
            }
            return Ok(response);
        }
        [HttpPatch("sent-to-candidate/{id}")]
        [Authorize(Roles = $"{SD.Role_HR},{SD.Role_Manager},{SD.Role_Admin}")]
        public async Task<IActionResult> SentToCandidateOffer(Guid id)
        {
            var response = new BaseSuccess<string>();
            var isUpdate = await _offerService.UpdateStatusOfferAsync(id, OfferStatusEnum.WaitingForResponse);
            if (!isUpdate)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can't sent to candidate";
            }
            else
            {
                var offer = _offerService.GetById(id);
                var subject = $"{offer.Candidate.FullName}'s Job Opportunity";
                var message = $"Dear {offer.Candidate.FullName}, we have a job opportunity for you."
                               + $"We are pleased to inform you that you have been selected for the position of {offer.Position.Name} with a salary of {offer.BasicSalary}. "
                               + $"Your contract will commence on {offer.ContractPeriodStart} and end on {offer.ContractPeriodEnd}.\n"
                               + $"Best regards,\n Interview Management System";
                await _mailService.SendEmailAsync(offer.Candidate.Email, subject, message);
                response.Data = "Sent to candidate successfully";
            }
            return Ok(response);
        }
        [HttpPatch("accept/{id}")]
        [Authorize(Roles = $"{SD.Role_HR},{SD.Role_Manager},{SD.Role_Admin}")]
        public async Task<IActionResult> AcceptedOffer(Guid id)
        {
            var response = new BaseSuccess<string>();
            var isUpdate = await _offerService.UpdateStatusOfferAsync(id, OfferStatusEnum.AcceptedOffer);
            if (!isUpdate)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can't accept offer";
            }
            else
            {
                response.Data = "Accept offer successfully";
            }
            return Ok(response);
        }
        [HttpPatch("decline/{id}")]
        [Authorize(Roles = $"{SD.Role_HR},{SD.Role_Manager},{SD.Role_Admin}")]
        public async Task<IActionResult> DeclinedOffer(Guid id)
        {
            var response = new BaseSuccess<string>();
            var isUpdate = await _offerService.UpdateStatusOfferAsync(id, OfferStatusEnum.DesclinedOffer);
            if (!isUpdate)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can't decline offer";
            }
            else
            {
                response.Data = "Decline offer successfully";
            }
            return Ok(response);
        }
        [HttpGet("excel/export-excel")]
        public IActionResult ExportExcel([FromQuery] DateTime? fromDate, [FromQuery] DateTime? toDate)
        {
            var excel = _offerService.ExportExcel(fromDate, toDate);
            using MemoryStream ms = new();
            excel.SaveAs(ms);
            ms.Seek(0, SeekOrigin.Begin);
            string fromDateString = fromDate?.ToString("yyyy-MM-dd") ?? "start";
            string toDateString = toDate?.ToString("yyyy-MM-dd") ?? "end";
            return File(ms.ToArray(), "application/vnd.openxalformats-officedocument.spreadsheetml.sheet", $"offer-list-from:{fromDateString}to:{toDateString}.xlsx");
        }
    }
}
