﻿using IMS.Business;
using IMS.Business.DTO.Schedule;
using IMS.Business.Service.Interface;
using IMS.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace IMS.Presentation.Controllers
{
    [Route("api/interview-schedule")]
    [ApiController]
    public class InterviewScheduleController(IScheduleService scheduleService) : ControllerBase
    {
        /// <summary>
        /// Create schedule
        /// </summary>
        /// <param name="scheduleCreateDto"></param>
        /// <returns></returns>
        [Authorize(Roles = SD.Role_Admin + "," + SD.Role_HR + "," + SD.Role_Manager)]
        [HttpPost]
        public async Task<IActionResult> AddSchedule([FromBody] ScheduleCreateDTO scheduleCreateDto)
        {
            // Get current user id login
            var createdById = User.FindFirst(ClaimTypes.NameIdentifier);

            // Add schedule
            var response = await scheduleService.AddSchedule(scheduleCreateDto, createdById);

            // Check if add schedule success
            if (response.IsSuccess)
            {
                return StatusCode(StatusCodes.Status200OK, response);

            }

            return StatusCode(StatusCodes.Status400BadRequest, response);
        }

        /// <summary>
        /// Get all schedules with filter and paging
        /// </summary>
        /// <param name="textSearch"></param>
        /// <param name="status"></param>
        /// <param name="interviewer"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize(Roles = SD.Role_Admin + "," + SD.Role_HR + "," + SD.Role_Manager + "," + SD.Role_Interviewer)]
        [HttpGet]
        public IActionResult GetSchedules(string? textSearch, int? status, Guid? interviewer, int? pageIndex, int? pageSize)
        {
            // Get current user id login
            var userId = User.FindFirst(ClaimTypes.NameIdentifier);

            var schedules = scheduleService.GetSchedules(textSearch, status, interviewer, pageIndex, pageSize, userId);

            return Ok(schedules);
        }

        /// <summary>
        /// Get schedule by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = SD.Role_Admin + "," + SD.Role_HR + "," + SD.Role_Manager + "," + SD.Role_Interviewer)]
        [HttpGet("{id:guid}")]
        public IActionResult GetScheduleById(Guid id)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier);

            var apiResponse = scheduleService.GetScheduleById(id, userId);

            if (!apiResponse.IsSuccess)
            {
                return BadRequest(apiResponse.MessageResponse);
            }

            return Ok(apiResponse);
        }

        /// <summary>
        /// Update schedule details for Admin, Manager, Recruiter
        /// </summary>
        /// <param name="scheduleUpdateDto"></param>
        /// <returns></returns>
        [Authorize(Roles = SD.Role_Admin + "," + SD.Role_HR + "," + SD.Role_Manager)]
        [HttpPut("update-schedule")]
        public IActionResult UpdateSchedule([FromBody] ScheduleUpdateDTO scheduleUpdateDto)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier);

            var apiResponse = scheduleService.UpdateScheduleDetails(userId, scheduleUpdateDto);

            if (apiResponse.IsSuccess == false)
            {
                return BadRequest(apiResponse.MessageResponse);
            }

            return Ok(apiResponse);
        }

        /// <summary>
        /// Update schedule result for Interviewer
        /// </summary>
        /// <param name="scheduleUpdateResultDto"></param>
        /// <returns></returns>
        [Authorize(Roles = SD.Role_Interviewer)]
        [HttpPut("update-schedule-result")]
        public IActionResult UpdateScheduleResult([FromBody] ScheduleUpdateResultDTO scheduleUpdateResultDto)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier);

            var response = scheduleService.UpdateScheduleResult(userId, scheduleUpdateResultDto);

            if (response.IsSuccess == false)
                return BadRequest(response.MessageResponse);

            return Ok(response.MessageResponse);
        }

        [Authorize(Roles = SD.Role_Admin + "," + SD.Role_HR + "," + SD.Role_Manager)]
        [HttpPut("cancel-schedule")]
        public IActionResult CancelSchedule(ScheduleCancelDTO scheduleCancelDto)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier);

            var response = scheduleService.CancelSchedule(userId, scheduleCancelDto);

            if (response.IsSuccess == false)
                return BadRequest(response.MessageResponse);

            return Ok(response.MessageResponse);
        }

        /// <summary>
        /// Reminder schedule for candidate when the interview is coming
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = SD.Role_Admin + "," + SD.Role_HR + "," + SD.Role_Manager + "," + SD.Role_Interviewer)]
        [HttpPost("{id:guid}")]
        public IActionResult Reminder(Guid id)
        {
            return BadRequest();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("candidate/{id}")]
        public IActionResult GetScheduleByCandidateId(Guid id)
        {

            var response = new BaseSuccess<IEnumerable<ScheduleDTO>>();
            var schedules = scheduleService.GetScheduleByCandidateId(id);
            if (schedules == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "List schedules not found";
            }
            else
            {
                response.Data = schedules;
            }
            return Ok(response);
        }

    }
}
