﻿using IMS.Business;
using IMS.Business.DTO;
using IMS.Business.DTO.Authenticate;
using IMS.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IMS.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        [Route("user-list")]
        [Authorize(Roles = SD.Role_Admin)]
        public async Task<IActionResult> Search([FromQuery] SearchUserRequest searchUserRequest)
        {
            var response = new BaseSuccess<Paginated<UserDTO>>();
            var data = await _userService.GetUsersAsync(searchUserRequest);

            if (data == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "List user not found";
            }
            else
            {
                response.Data = data;
            }
            return Ok(response);
        }

        [HttpGet]
        [Route("get-users-not-ban-by-role-and-username")]
        [Authorize(Roles = SD.Role_Admin + "," + SD.Role_HR + "," + SD.Role_Manager)]
        public async Task<IActionResult> Search(string role, string? textSearch, int? pageIndex, int? pageSize)
        {
            var response = await _userService.GetUsersNotBanByRoleAndUsername(role, textSearch, pageIndex, pageSize);
            if (!response.IsSuccess)
                return BadRequest(response.MessageResponse);
            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserById(Guid id)
        {
            var response = new BaseSuccess<UserDTO>();
            var data = await _userService.GetUserByIdAsync(id);

            if (data == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "User not found";
            }
            else
            {
                response.Data = data;
            }
            return Ok(response);
        }

        [HttpPost]
        [Authorize(Roles = SD.Role_Admin)]
        public async Task<IActionResult> Create(UserCreateDTO userCreateDTO)
        {
            var response = new BaseSuccess<UserDTO>();
            var data = await _userService.CreateUser(userCreateDTO);

            if (data == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can not add user";
            }
            else
            {
                response.Data = data;
            }
            return Ok(response);
        }
        [HttpPut]
        [Authorize(Roles = SD.Role_Admin)]
        public async Task<IActionResult> Update(UserUpdateDTO userUpdateDTO)
        {
            var response = new BaseSuccess<UserDTO>();
            var data = await _userService.UpdateUser(userUpdateDTO);
            if (data == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Can not edit user";
            }
            else
            {
                response.Data = data;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("lock-unlock")]
        [Authorize(Roles = SD.Role_Admin)]
        public async Task<IActionResult> LockUnlock(LockUnLockRequest request)
        {
            var response = new BaseSuccess<UserDTO>();
            var data = await _userService.LockUnlock(request);
            if (data == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "Lock-Unlock failed";
            }
            else
            {
                response.Data = data;
                response.MessageResponse = "Update successfully";
            }
            return Ok(response);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("forgot-password")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordRequest request)
        {
            var response = await _userService.ForgotPassword(request);
            if (response.IsSuccess == true)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }
        [HttpPost("reset-password")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordRequest request)
        {
            var isSuccess = await _userService.ResetPassword(request);
            if (isSuccess)
                return Ok(new ResetPasswordResponse() { IsSuccess = true });
            return BadRequest(new ResetPasswordResponse() { IsSuccess = false });
        }
        [HttpGet("role")]
        public async Task<IActionResult> GetUserByRole(string roleName)
        {
            var response = new BaseSuccess<List<AppUserDTO>>();
            var users = await _userService.GetUsersByRoleAsync(roleName);
            if (users == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "List user not found";
            }
            else
            {
                response.Data = users;
            }
            return Ok(response);
        }
    }
}
