﻿using IMS.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IMS.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        [HttpGet]
        [Route("test")]
        [Authorize(Roles = SD.Role_Admin)]
        public IActionResult Test()
        {
            return Ok("Test Diplay Interview Role Infomation");
        }
    }
}
