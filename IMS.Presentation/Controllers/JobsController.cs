﻿using IMS.Business;
using IMS.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace IMS.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : ControllerBase
    {
        private readonly IJobService _jobService;

        public JobsController(IJobService jobService)
        {
            _jobService = jobService;
        }
        [HttpGet]
        public IActionResult Filter(string? search, JobStatusEnum? jobStatus, int pageIndex, int pageSize)
        {
            var result = _jobService.FilterJob(search, jobStatus, pageIndex, pageSize);
            return Ok(new BaseSuccess<Paginated<JobDTO>>()
            {
                Data = result
            });
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            return Ok(new BaseSuccess<JobDTO>()
            {
                Data = _jobService.GetById(id, "Attrs")
            });
        }

        [HttpPost]
        public IActionResult Create(JobCreateDTO jobCreateDTO)
        {
            return Ok(new BaseSuccess<JobDTO>()
            {
                Data = _jobService.Add(jobCreateDTO)
            });
        }

        [HttpPut]
        public async Task<IActionResult> Update(JobUpdateDTO jobUpdateDTO)
        {
            var result = await _jobService.UpdateAsync(jobUpdateDTO);
            return Ok(new BaseSuccess<bool>()
            {
                Data = result
            });
        }

        [HttpDelete]
        public IActionResult Delete(Guid id)
        {
            var result = _jobService.Delete(id);
            return Ok(new BaseSuccess<bool>()
            {
                Data = result
            });
        }

        [HttpGet("excel/export")]
        public IActionResult ExportExcel(string? searchKey, JobStatusEnum? jobStatus)
        {
            var excel = _jobService.ExportExcel(searchKey, jobStatus);
            using MemoryStream ms = new();
            excel.SaveAs(ms);
            return File(ms.ToArray(), "application/vnd.openxalformats-officedocument.spreadsheetml.sheet", "job-list.xlsx");
        }

        [HttpPost("excel/import")]
        public async Task<IActionResult> ImportExcel(IFormFile file)
        {
            var result = await _jobService.ImportExcel(file);
            return Ok(new BaseSuccess<ExcelValidatedForm>()
            {
                Data = result
            });
        }

        [HttpGet("excel/template")]
        public IActionResult DowloadExcelTemplate()
        {
            var excel = _jobService.GetExcelTemplate();
            using MemoryStream ms = new();
            excel.SaveAs(ms);
            return File(ms.ToArray(), "application/vnd.openxalformats-officedocument.spreadsheetml.sheet", "job-list-template.xlsx");
        }

        [HttpGet("excel/validated")]
        public IActionResult DowloadExcelValidated(string excelFileName)
        {
            var excel = _jobService.GetExcelValidated(excelFileName);
            using MemoryStream ms = new();
            excel.SaveAs(ms);
            return File(ms.ToArray(), "application/vnd.openxalformats-officedocument.spreadsheetml.sheet", "job-list-validated.xlsx");
        }
    }
}
