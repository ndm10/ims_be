﻿using IMS.Business;
using IMS.Business.Service.Interface;
using IMS.Data;
using Microsoft.AspNetCore.Mvc;

namespace IMS.Presentation
{
    [Route("api/[controller]")]
    public class CommonValueController : ControllerBase
    {
        private readonly IPositionService _positionService;
        private readonly IDepartmentService _departmentService;
        private readonly IAttributeValueService _attributeValueService;
        private readonly IUserService _userService;

        public CommonValueController(IPositionService positionService, IDepartmentService departmentService, IAttributeValueService attributeValueService, IUserService userService)
        {
            _positionService = positionService;
            _departmentService = departmentService;
            _attributeValueService = attributeValueService;
            _userService = userService;
        }

        [HttpGet("departments")]
        public IActionResult GetDepartments()
        {
            var response = new BaseSuccess<IEnumerable<DepartmentDTO>>();
            var departments = _departmentService.GetAll();
            if (departments == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "List departments not found";
            }
            else
            {
                response.Data = departments;
            }
            return Ok(response);
        }
        [HttpGet("roles")]
        public async Task<IActionResult> GetRoles()
        {
            var response = new BaseSuccess<List<string>>();
            var users = await _userService.GetAllRoles();
            if (users == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "List user not found";
            }
            else
            {
                response.Data = users;
            }
            return Ok(response);
        }
        [HttpGet("positions")]
        public IActionResult GetPositions()
        {
            var response = new BaseSuccess<IEnumerable<PositionDTO>>();
            var positions = _positionService.GetAll();
            if (positions == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "List positions not found";
            }
            else
            {
                response.Data = positions;
            }
            return Ok(response);
        }

        [HttpGet("attributes")]
        public IActionResult GetSkills(AttributeTypeEnum attributeTypeEnum)
        {
            var response = new BaseSuccess<IEnumerable<AttributeDTO>>();
            var skills = _attributeValueService.GetByType(attributeTypeEnum);
            if (skills == null)
            {
                response.IsSuccess = false;
                response.MessageResponse = "List attributes not found";
            }
            else
            {
                response.Data = skills;
            }
            return Ok(response);
        }
    }
}
