﻿using IMS.Business;
namespace IMS.Presentation
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ResponseMiddleware
    {
        private readonly RequestDelegate _next;

        public ResponseMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }

            catch (UserException ex)
            {
                // ngoại lệ cho người dùng
                Console.WriteLine(ex);
                HandleException(context, ex);
            }
            catch (AggregateException ex)
            {
                // Xử lý các ngoại lệ bên trong AggregateException
                foreach (var innerEx in ex.InnerExceptions)
                {
                    HandleException(context, innerEx);
                }
            }
            catch (Exception ex)
            {
                // ngoại lệ do hệ thống
                Console.WriteLine(ex);
                HandleException(context, ex);
            }
        }

        private async void HandleException(HttpContext context, Exception ex)
        {
            Console.WriteLine(ex);

            if (ex is UserException userEx)
            {
                if (!context.Response.HasStarted)
                {
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = StatusCodes.Status400BadRequest;
                    await context.Response.WriteAsync(text: new BaseException
                    {
                        Errors = userEx.Errors,
                        IsSuccess = false,
                        MessageResponse = "Fail"
                    }.ToString() ?? "");
                }
            }
            else
            {
                if (!context.Response.HasStarted)
                {
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                    await context.Response.WriteAsync(text: new BaseException
                    {
                        Errors = ["System Error"],
                        IsSuccess = false,
                        MessageResponse = "Thất bại"
                    }.ToString() ?? "");
                }
            }
        }
    }
}