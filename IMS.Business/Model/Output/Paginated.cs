﻿namespace IMS.Business
{
    public class Paginated<TEntity>
    {
        public int PageIndex { get; set; }

        public int TotalPages { get; set; }

        public int PageSize { get; set; }

        public long TotalRecord { get; set; }

        public List<TEntity> Items { get; set; } = new List<TEntity>();
        public bool HasPreviousPage => PageIndex > 1;

        public bool HasNextPage => PageIndex < TotalPages;

        public Paginated()
        {
        }

        public static Paginated<TEntity> Create(IQueryable<TEntity> query, int pageIndex, int pageSize)
        {
            pageSize = pageSize < 1 ? 10 : pageSize;
            pageIndex = pageIndex < 1 ? 1 : pageIndex;
            var entities = query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            var totalPage = (int)Math.Ceiling(query.Count() / (double)pageSize);
            var result = new Paginated<TEntity>
            {
                PageIndex = pageIndex,
                TotalPages = totalPage,
                PageSize = pageSize,
                TotalRecord = query.Count(),
                Items = entities
            };
            return result;
        }
    }
}
