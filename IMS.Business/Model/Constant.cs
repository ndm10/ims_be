﻿namespace IMS.Business.Model
{
    public static class Constant
    {
        public const string ADMIN_ROLE = "Admin";
        public const string CANDIDATE_ROLE = "Candidate";
        public const string RECRUITER_ROLE = "Recruiter";
        public const string INTERVIEWER_ROLE = "Interviewer";
        public const string MANAGER_ROLE = "Manager";


        public const string ADMIN_ROLE_NORMALIZED = "ADMIN";
        public const string CANDIDATE_ROLE_NORMALIZED = "CANDIDATE";
        public const string RECRUITER_ROLE_NORMALIZED = "RECRUITER";
        public const string INTERVIEWER_ROLE_NORMALIZED = "INTERVIEWER";
        public const string MANAGER_ROLE_NORMALIZED = "MANAGER";

    }
}
