﻿using IMS.Data;

namespace IMS.Business;

public class AttributeMapper : BaseMapper<Attribute, AttributeDTO, AttributeDTO, AttributeDTO>
{
    public AttributeMapper()
    {
        CreateMap<AttributeValue, AttributeDTO>()
            .ForMember(dest => dest.Type, opt => opt.MapFrom(src => (AttributeTypeEnum)src.Type))
            .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));
    }
}
