﻿using AutoMapper;
namespace IMS.Business
{
    public abstract class BaseMapper<Entity, EntityDTO, EntityCreateDTO, EntityUpdateDTO> : Profile
    {
        public BaseMapper()
        {
            CreateMap<Entity, Entity>();
            CreateMap<EntityDTO, Entity>().ReverseMap()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null)); // Giữ nguyên giá trị nếu không trùng trường
            CreateMap<EntityCreateDTO, Entity>().ReverseMap()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null)); // Giữ nguyên giá trị nếu không trùng trường
            CreateMap<EntityUpdateDTO, Entity>().ReverseMap()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null)); // Giữ nguyên giá trị nếu không trùng trường
        }
    }
}
