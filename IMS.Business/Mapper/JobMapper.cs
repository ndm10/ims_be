﻿using IMS.Data;


namespace IMS.Business
{
    public class JobMapper : BaseMapper<Job, JobDTO, JobCreateDTO, JobUpdateDTO>
    {
        public JobMapper()
        {
            CreateMap<Job, JobDTO>()
            .ForMember(dest => dest.AttributeDTOs, opt => opt.MapFrom(src => src.Attrs.Select(attr => new AttributeDTO
            {
                Id = attr.Id,
                Type = (AttributeTypeEnum)attr.Type,
                Value = attr.Value ?? ""
            }).ToList()))
            .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));

            //CreateMap<JobCreateDTO, Job>()
            //.ForMember(dest => dest.Attrs, opt => opt.MapFrom(src => MapJobCreateDTOToJob(src.Skills, src.Levels, src.Benefits)))
            //.ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));
        }


        //private ICollection<AttributeValue> MapJobCreateDTOToJob(List<Guid>? skillIds, List<Guid>? levelIds, List<Guid>? benefitIds)
        //{
        //    var attributeValues = _unitOfWork.GetRepository<IAttributeValueRepository>()
        //                                .GetQuery(x => !(skillIds == null || skillIds.Contains(x.Id)
        //                                        || (levelIds == null || levelIds.Contains(x.Id)
        //                                        || benefitIds == null || benefitIds.Contains(x.Id))));

        //    return [.. attributeValues];
        //}
    }
}
