﻿using IMS.Business.DTO.Notification;
using IMS.Data.Entity;

namespace IMS.Business.Mapper
{
    public class NotificationMapper : BaseMapper<Notification, NotificationDTO, NotificationCreateDTO, NotificationUpdateDTO>
    {
    }
}
