﻿
using AutoMapper;
using IMS.Data;

namespace IMS.Business
{
    public class PositionMapper : Profile
    {
        public PositionMapper()
        {
            CreateMap<Position, PositionDTO>().ReverseMap();
        }
    }
}
