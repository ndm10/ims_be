﻿using AutoMapper;
using IMS.Data;

namespace IMS.Business
{
    public class DepartmentMapper : Profile
    {
        public DepartmentMapper()
        {
            CreateMap<Department, DepartmentDTO>().ReverseMap();
        }
    }
}
