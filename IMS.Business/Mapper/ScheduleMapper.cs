﻿using IMS.Business.DTO.Schedule;
using IMS.Data;

namespace IMS.Business.Mapper
{
    public class ScheduleMapper : BaseMapper<Schedule, ScheduleDTO, ScheduleCreateDTO, ScheduleUpdateDTO>
    {
    }
}
