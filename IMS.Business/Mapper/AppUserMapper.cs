﻿using AutoMapper;
using IMS.Business.DTO;
using IMS.Data;

namespace IMS.Business.Mapper
{
    public class AppUserMapper : Profile
    {
        public AppUserMapper()
        {
            CreateMap<ApplicationUser, AppUserDTO>();

        }

    }
}
