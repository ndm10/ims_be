﻿using IMS.Business.DTO.ApplicationUser;
using IMS.Business.DTO.Candidate;
using IMS.Data;

namespace IMS.Business;

public class CandidateMapper : BaseMapper<Candidate, CandidateDTO, CandidateCreateDTO, CandidateUpdateDTO>
{
    public CandidateMapper()
    {
        CreateMap<Candidate, CandidateListDTO>().ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));
        CreateMap<Candidate, CandidateDTO>()
            .ForMember(dest => dest.Skills, opt => opt.MapFrom(src => MappingAttributes(src.Attrs, AttributeTypeEnum.Skill)))
            .ForMember(dest => dest.HighestLevel, opt => opt.MapFrom(src => MappingAttributes(src.Attrs, AttributeTypeEnum.HighestLevel).FirstOrDefault()))
            .ForMember(dest => dest.Position, opt => opt.MapFrom(src => new PositionDTO
            {
                Id = src.Position.Id,
                Name = src.Position.Name ?? ""
            }))
            .ForMember(dest => dest.OwnerHR, opt => opt.MapFrom(src => new ApplicationUserDTO
            {
                Id = src.OwnerHR.Id,
                FullName = src.OwnerHR.FullName ?? ""
            }))
            .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));
        CreateMap<Candidate, CandidateOfferDTO>();
    }

    private List<AttributeDTO> MappingAttributes(ICollection<AttributeValue> sourceAttributes, AttributeTypeEnum attrEnum)
    {
        if (sourceAttributes == null)
        {
            throw new UserException(["Source attributes cannot be null"]);
        }

        return sourceAttributes
            .Where(attr => attr.Type == (int)attrEnum)
            .Select(attr => new AttributeDTO
            {
                Id = attr.Id,
                Value = attr.Value ?? ""
            })
            .ToList();
    }
}
