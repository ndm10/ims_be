﻿using IMS.Business.DTO.ApplicationUser;
using IMS.Data;

namespace IMS.Business.Mapper
{
    public class ApplicationUserMapper : BaseMapper<ApplicationUser, ApplicationUserDTO, ApplicationUserCreateDTO, ApplicationUserUpdateDTO>
    {

    }
}
