﻿using IMS.Data;
namespace IMS.Business
{
    public class OfferMapper : BaseMapper<Offer, OfferDTO, OfferCreateDTO, OfferUpdateDTO>
    {
        public OfferMapper()
        {
            CreateMap<Offer, OfferDTO>()
            .ForMember(dest => dest.AttributeDTOs, opt => opt.MapFrom(src => src.Attrs.Select(attr => new AttributeDTO
            {
                Id = attr.Id,
                Type = (AttributeTypeEnum)attr.Type,
                Value = attr.Value ?? ""
            }).ToList()))
            .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));


            CreateMap<OfferCreateDTO, Offer>()
            .ForMember(
                dest => dest.DueDate,
                opt => opt.MapFrom(src => DateOnly.FromDateTime(src.DueDate))
            );
            CreateMap<OfferUpdateDTO, Offer>()
            .ForMember(
                dest => dest.DueDate,
                opt => opt.MapFrom(src => DateOnly.FromDateTime(src.DueDate))
            );
        }
    }
}
