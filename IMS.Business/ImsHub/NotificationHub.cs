﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Reflection;
namespace IMS.Business.ImsHub
{
    [Authorize]
    public class NotificationHub() : Hub
    {
        /// <summary>
        /// Method for client to join group (This method using Javascript)
        /// </summary>
        /// <param name="groupName">Name of group to join</param>
        /// <returns></returns>
        public async Task JoinGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }

        /// <summary>
        /// Method for client to leave group (This method using Javascript)
        /// </summary>
        /// <param name="groupName">Name of group to join</param>
        /// <returns></returns>
        public async Task LeaveGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }
    }
}
