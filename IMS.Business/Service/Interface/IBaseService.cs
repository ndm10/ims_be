﻿using IMS.Data;

namespace IMS.Business
{
    public interface IBaseService<TEntity, TEntityDTO, TEntityCreateDTO, TEntityUpdateDTO>
        where TEntity : BaseEntity
    {
        /// <summary>
        /// Adds a new Entity.
        /// </summary>
        /// <param name="entityCreate">The entityDTO to be added.</param>
        /// <returns>A entityDTO object representing the newly added entity.</returns>
        TEntityDTO Add(TEntityCreateDTO entityCreate);

        Task<TEntityDTO> AddAsync(TEntityDTO entityCreate);

        int AddRange(IEnumerable<TEntityCreateDTO> entityCreateList);

        Task<int> AddRangeAsync(IEnumerable<TEntityCreateDTO> entityCreateList);

        /// <summary>
        /// Updates a entity.
        /// </summary>
        /// <param name="entityUpdate">The entityUpdateDTO to be updated.</param>
        /// <returns>A boolean value indicating whether update successful.</returns>
        bool Update(TEntityUpdateDTO entityUpdate);

        Task<bool> UpdateAsync(TEntityUpdateDTO entityUpdate);

        /// <summary>
        /// Soft deletes an entity
        /// </summary>
        /// <param name="id">The id of candidate</param>
        /// <returns>A boolean value indicating whether soft delete successful</returns>
        bool Delete(Guid id);

        Task<bool> DeleteAsync(Guid id);

        /// <summary>
        /// Get a entity
        /// </summary>
        /// <param name="id">The id of entity</param>
        /// <returns>A EntityDTO object</returns>
        TEntityDTO? GetById(Guid id, string includeProperties = "");

        Task<TEntityDTO?> GetByIdAsync(Guid id);

        IEnumerable<TEntityDTO> GetAll(bool isGetDeleted = false);

        Task<IEnumerable<TEntityDTO>> GetAllAsync(bool isGetDeleted = false);

        // /// <summary>
        // /// trả về kết quả với theo các điều kiện và sắp xếp,phân trang
        // /// </summary>
        // /// <param name="filter">x=>x.Name.Contains("abc")</param>
        // /// <param name="orderBy">q => q.OrderByDescending(c => c.Name);</param>
        // /// <param name="includeProperties">"Products", "Authors, Category, Publisher"</param>
        // /// <param name="isGetDeleted"></param>
        // /// <param name="pageIndex"></param>
        // /// <param name="pageSize"></param>
        // /// <returns></returns>
        // Paginated<TEntityDTO> Filter(Expression<Func<TEntityDTO, bool>>? filter = null,
        //     Func<IQueryable<TEntityDTO>, IOrderedQueryable<TEntityDTO>>? orderBy = null,
        //     string includeProperties = "", bool isGetDeleted = false, int pageIndex = 1, int pageSize = 10);
    }
}
