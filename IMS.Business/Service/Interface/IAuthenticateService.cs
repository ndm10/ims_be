﻿using InterviewManagerment.Business;

namespace IMS.Business
{
    public interface IAuthenticateService
    {
        Task<LoginResponseDto> LoginAsync(LoginRequestDto request);
        Task<LoginResponseDto> GenerateNewAccessToken(TokenModel tokenModel);
    }
}
