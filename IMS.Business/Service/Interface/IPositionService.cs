﻿
using IMS.Data;

namespace IMS.Business.Service.Interface
{
    public interface IPositionService : IBaseService<Position, PositionDTO, PositionDTO, PositionDTO>
    {
    }
}
