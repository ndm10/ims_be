﻿
using IMS.Data;

namespace IMS.Business.Service.Interface
{
    public interface IDepartmentService : IBaseService<Department, DepartmentDTO, DepartmentDTO, DepartmentDTO>
    {
    }
}
