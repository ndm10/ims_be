﻿using IMS.Business.DTO.Schedule;
using IMS.Data;
using System.Security.Claims;

namespace IMS.Business.Service.Interface
{
    public interface IScheduleService
    {
        public Task<BaseSuccess<Schedule>> AddSchedule(ScheduleCreateDTO scheduleCreateDTO, Claim? userLoginId);
        public BaseSuccess<ScheduleDTO> GetScheduleById(Guid id, Claim? userLoginId);
        public BaseSuccess<Paginated<ScheduleDTO>> GetSchedules(string? textSearch, int? status, Guid? interviewer, int? pageIndex, int? pageSize, Claim? userLoginId);
        public List<ScheduleDTO> GetScheduleByCandidateId(Guid id);
        public BaseSuccess<Schedule> UpdateScheduleDetails(Claim? userId, ScheduleUpdateDTO scheduleUpdateDTO);
        public BaseSuccess<Schedule> UpdateScheduleResult(Claim? userId, ScheduleUpdateResultDTO scheduleUpdateResultDTO);
        public BaseSuccess<Schedule> CancelSchedule(Claim? userId, ScheduleCancelDTO scheduleCancelDTO);
    }
}
