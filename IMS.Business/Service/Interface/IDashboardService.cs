﻿using IMS.Data;

namespace IMS.Business;

public interface IDashboardService
{
    CardItemDTO GetCandidatesParameter();
    CardItemDTO GetJobsParameter();
    CardItemDTO GetInterviewsParameter();
    CardItemDTO GetOffersParameter();
    List<ChartDTO> CountInterview(int dateType);
    List<ChartDTO> SuccessCandidate(int dateType);
}
