﻿using IMS.Business.DTO.ApplicationUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Business
{
    public interface IApplicationUserService
    {
        ApplicationUserDTO? GetById(Guid id, string includeProperties = "");
    }
}
