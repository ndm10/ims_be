﻿using IMS.Business.DTO.Candidate;
using IMS.Data;

namespace IMS.Business;

public interface ICandidateService : IBaseService<Candidate, CandidateDTO, CandidateCreateDTO, CandidateUpdateDTO>
{

    /// <summary>
    /// Get a list of candidates based on the search and status, with pagination support.
    /// </summary>
    /// <param name="search">Optional search term to filter candidates by all properties</param>
    /// <param name="status">Optional status to filter candidates by their status or default to all status</param>
    /// <param name="index">The page index</param>
    /// <param name="size">The number of records</param>
    /// <returns>A list of CandidateDTO</returns>
    Paginated<CandidateListDTO> GetCandidateBySearchAndStatus(string? search, CandidateStatusEnum? status, int index, int size);

    /// <summary>
    /// Get a list of candidates who do not have a banned status
    /// </summary>
    /// <returns>A list of CandidateDTO representing the candidates without a banned status.</returns>
    List<CandidateOfferDTO> GetCandidatesWithoutBanStatus();

    /// <summary>
    /// Ban a candidate.
    /// </summary>
    /// <param name="id">The id of candidate</param>
    /// <returns>A boolean value indicating whether ban successful</returns>
    bool BanCandidate(Guid id);

    /// <summary>
    /// Get a list of candidates based on the search name, with pagination support.
    /// </summary>
    /// <param name="textSearch">Optional search term to filter candidates by full name</param>
    /// <param name="pageIndex">The page index</param>
    /// <param name="pageSize">The number of records</param>
    /// <returns></returns>
    BaseSuccess<Paginated<CandidateListDTO>> GetCandidateByFilterByFullName(string? textSearch,int? status, int? pageIndex, int? pageSize);
}
