﻿using IMS.Data;

namespace IMS.Business;

public interface IAttributeValueService : IBaseService<AttributeValue, AttributeDTO, AttributeDTO, AttributeDTO>
{
    IEnumerable<AttributeDTO> GetByType(AttributeTypeEnum attributeTypeEnum);
}
