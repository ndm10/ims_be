﻿using IMS.Data;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;

namespace IMS.Business
{
    public interface IJobService : IBaseService<Job, JobDTO, JobCreateDTO, JobUpdateDTO>
    {
        bool Delete(Guid id);

        JobDTO Add(JobCreateDTO entityCreate);

        bool Update(JobUpdateDTO entityUpdate);

        Task<bool> UpdateAsync(JobUpdateDTO entityUpdate);

        Paginated<JobDTO> FilterJob(string? searchKey, JobStatusEnum? jobStatus, int pageIndex = 1, int pageSize = 10);

        JobDTO? GetById(Guid id, string includeProperties = "");

        ExcelPackage ExportExcel(string? searchKey, JobStatusEnum? jobStatus);

        Task<ExcelValidatedForm> ImportExcel(IFormFile excelFile);

        ExcelPackage GetExcelTemplate();

        ExcelPackage GetExcelValidated(string excelFileName);
    }
}
