﻿using IMS.Business.DTO;
using IMS.Business.DTO.ApplicationUser;

namespace IMS.Business
{
    public interface IUserService
    {
        Task<UserDTO?> CreateUser(UserCreateDTO userCreateDTO);
        Task<UserDTO> GetUserByIdAsync(Guid id);
        Task<Paginated<UserDTO>> GetUsersAsync(SearchUserRequest searchUserRequest);
        Task<UserDTO?> LockUnlock(LockUnLockRequest request);
        Task<UserDTO?> UpdateUser(UserUpdateDTO userUpdateDTO);
        Task<ForgotPasswordResponse> ForgotPassword(ForgotPasswordRequest request);
        Task<bool> ResetPassword(ResetPasswordRequest request);
        Task<List<AppUserDTO>> GetUsersByRoleAsync(string roleName);
        Task<List<string>> GetAllRoles();
        Task<BaseSuccess<Paginated<ApplicationUserDTO>>> GetUsersNotBanByRoleAndUsername(string role, string? textSearch, int? pageIndex, int? pageSize);
    }
}
