﻿
using IMS.Data;
using OfficeOpenXml;

namespace IMS.Business
{
    public interface IOfferService : IBaseService<Offer, OfferDTO, OfferCreateDTO, OfferUpdateDTO>
    {

        Task<OfferDTO> AddAsync(OfferCreateDTO offerCreateDTO);

        Paginated<OfferDTO> GetOfferBySearchAndStatus(string? search, OfferStatusEnum? status, Guid? departmentId, int index, int size);

        Task<bool> UpdateStatusOfferAsync(Guid offerId, OfferStatusEnum status);
        OfferDTO GetById(Guid id);

        ExcelPackage ExportExcel(DateTime? fromDate, DateTime? toDate);
    }
}
