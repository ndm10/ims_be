﻿using AutoMapper;
using IMS.Business.Service.Interface;
using IMS.Data;

namespace IMS.Business.Service.Implement
{
    public class DepartmentService : BaseServices<Department, DepartmentDTO, DepartmentDTO, DepartmentDTO>, IDepartmentService
    {
        public DepartmentService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }
    }
}
