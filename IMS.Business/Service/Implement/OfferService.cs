﻿using AutoMapper;
using IMS.Data;
using IMS.Data.Repository.Interface;
using IMS.Data.Utility.Enum;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System.Linq.Expressions;
using System.Security.Claims;

namespace IMS.Business;

public class OfferService : BaseServices<Offer, OfferDTO, OfferCreateDTO, OfferUpdateDTO>, IOfferService
{
    private readonly ILogger<OfferRepository> _logger;
    private readonly IConfiguration _configuration;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IHttpContextAccessor _httpContextAccessor;
    public OfferService(
        UnitOfWork unitOfWork,
        ILogger<OfferRepository> logger,
        IConfiguration configuration,
        IMapper mapper,
        UserManager<ApplicationUser> userManager,
        IHttpContextAccessor httpContextAccessor) : base(unitOfWork, mapper)
    {
        _logger = logger;
        _userManager = userManager;
        _configuration = configuration;
        _httpContextAccessor = httpContextAccessor;
    }
    #region create offer
    protected async Task ValidateBusinessLogicCreate(Offer entity)
    {
        var offerCandidate = _unitOfWork.GetRepository<ICandidateRepository>().Get().Select(c => new Candidate { Id = c.Id, Status = c.Status })
                                            .Where(c => c.Status != (int)CandidateStatusEnum.Banned).ToList()
                                            .FirstOrDefault(c => c.Id == entity.CandidateId);
        if (offerCandidate == null)
        {
            throw new UserException(["Candidate's id is not exist"]);
        }
        var offerDepartment = _unitOfWork.GetRepository<IDepartmentRepository>().GetById(entity.DepartmentId);
        if (offerDepartment == null)
        {
            throw new UserException(["Department's id is not exist"]);
        }
        var offerPosition = _unitOfWork.GetRepository<IPositionRepository>().GetById(entity.PositionId);
        if (offerPosition == null)
        {
            throw new UserException(["Position's id is not exist"]);
        }
        var approver = await _userManager.FindByIdAsync(entity.ManagerId.ToString());
        if (approver == null)
        {
            throw new UserException(["Approver's id is not exist"]);
        }
        var recruiter = await _userManager.FindByIdAsync(entity.RecruiterId.ToString());
        if (recruiter == null)
        {
            throw new UserException(["Recruiter's id is not exist"]);
        }
        if (!await _userManager.IsInRoleAsync(approver, SD.Role_Manager))
        {
            throw new UserException(["Approver is not in the Manager role"]);
        }
        if (!await _userManager.IsInRoleAsync(recruiter, SD.Role_HR))
        {
            throw new UserException(["Recruiter is not in the HR role"]);
        }

        if (entity.ScheduleId.HasValue && entity.ScheduleId != Guid.Empty)
        {
            var schedule = _unitOfWork.GetRepository<IScheduleRepository>().GetById(entity.ScheduleId.Value);

            if (schedule == null)
            {
                throw new UserException(["Schedule's id is not exist"]);
            }
        }
        entity.ScheduleId = null;
    }
    public async Task<OfferDTO> AddAsync(OfferCreateDTO offerCreateDTO)
    {
        if (offerCreateDTO == null)
        {
            throw new UserException(["Created entity is empty"]);
        }
        var entity = _mapper.Map<Offer>(offerCreateDTO);
        //Validate business logic
        await ValidateBusinessLogicCreate(entity);
        //Edit offer Before Create
        entity.Id = Guid.NewGuid();
        entity.CreatedAt = DateTimeOffset.UtcNow;
        var userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        entity.CreatedBy = new Guid(userId);
        entity.Status = OfferStatusEnum.WaitingForApproval;
        var candidateOffer = _unitOfWork.GetRepository<ICandidateRepository>().GetById(entity.CandidateId);
        candidateOffer.Status = (int)CandidateStatusEnum.WaitingForApproval;
        _unitOfWork.GetRepository<ICandidateRepository>().Update(candidateOffer);
        var attributeValues = _unitOfWork.GetRepository<IAttributeValueRepository>()
                                        .GetQuery(x => offerCreateDTO.LevelsId.Contains(x.Id));
        foreach (var attributeValue in attributeValues)
        {
            entity.Attrs.Add(attributeValue);
        }
        _unitOfWork.GetRepository<IOfferRepository>().Add(entity);
        _unitOfWork.SaveChanges();
        return _mapper.Map<OfferDTO>(entity);

    }
    #endregion
    #region get detail offer
    public OfferDTO GetById(Guid id)
    {

        var offer = _unitOfWork.GetRepository<IOfferRepository>().GetById(id, "Approver,Department,Position,Attrs,Recruiter");
        var candidateOffer = _unitOfWork.GetRepository<ICandidateRepository>().Get().Select(c => new Candidate { Id = c.Id, FullName = c.FullName, Status = c.Status }).FirstOrDefault(c => c.Id == offer.CandidateId);
        offer.Candidate = candidateOffer;
        offer.Schedule = _unitOfWork.GetRepository<IScheduleRepository>().Get(includeProperties: "Interviewers").Select(s => new Schedule { Id = s.Id, Title = s.Title, Interviewers = s.Interviewers }).FirstOrDefault(s => s.Id == offer.ScheduleId);
        if (offer == null)
        {
            throw new UserException(["Entity doesn't exist"]);
        }
        return _mapper.Map<OfferDTO>(offer);
    }
    #endregion

    #region get offer by filter
    public Paginated<OfferDTO> GetOfferBySearchAndStatus(string? search, OfferStatusEnum? status, Guid? departmentId, int index, int size)
    {
        Expression<Func<Offer, bool>> filter = c => (search == null
                                                        || c.Candidate.FullName.Contains(search)
                                                        || c.Candidate.Email.Contains(search)
                                                        || c.Approver.FullName.Contains(search)
                                                        || c.Note.Contains(search))
                                                    && (status == null ? true : c.Status == status)
                                                    && (departmentId == null ? true : c.DepartmentId == departmentId);
        Func<IQueryable<Offer>, IOrderedQueryable<Offer>> orderByDescendingUpdate = q => q.OrderByDescending(c => c.UpdatedAt);

        var query = _unitOfWork.GetRepository<IOfferRepository>().Get(filter, orderByDescendingUpdate, "Candidate,Position,Department,Approver", false);
        var entitiesPaginated = Paginated<Offer>.Create(query, index, size);
        var offerDTOs = _mapper.Map<List<OfferDTO>>(entitiesPaginated.Items);
        return new Paginated<OfferDTO>
        {
            PageIndex = entitiesPaginated.PageIndex,
            TotalPages = entitiesPaginated.TotalPages,
            PageSize = entitiesPaginated.PageSize,
            TotalRecord = entitiesPaginated.TotalRecord,
            Items = offerDTOs
        };

    }
    #endregion
    #region update offer
    protected async Task ValidateBusinessLogicUpdateAsync(Offer entity)
    {
        if (entity == null)
        {
            throw new UserException(["Entity does not exist in the system"]);
        }
        if (entity.Status != OfferStatusEnum.WaitingForApproval)
        {
            throw new UserException(["Entity cannot be edited"]);
        }
        var offerCandidate = _unitOfWork.GetRepository<ICandidateRepository>().Get().Select(c => new Candidate { Id = c.Id, Status = c.Status })
                                            .Where(c => c.Status != (int)CandidateStatusEnum.Banned).ToList()
                                            .FirstOrDefault(c => c.Id == entity.CandidateId);
        if (offerCandidate == null)
        {
            throw new UserException(["Candidate's id is not exist"]);
        }
        var offerDepartment = _unitOfWork.GetRepository<IDepartmentRepository>().GetById(entity.DepartmentId);
        if (offerDepartment == null)
        {
            throw new UserException(["Department's id is not exist"]);
        }
        var offerPosition = _unitOfWork.GetRepository<IPositionRepository>().GetById(entity.PositionId);
        if (offerPosition == null)
        {
            throw new UserException(["Position's id is not exist"]);
        }
        var approver = await _userManager.FindByIdAsync(entity.ManagerId.ToString());
        if (approver == null)
        {
            throw new UserException(["Approver's id is not exist"]);
        }
        var recruiter = await _userManager.FindByIdAsync(entity.RecruiterId.ToString());
        if (recruiter == null)
        {
            throw new UserException(["Recruiter's id is not exist"]);
        }
        if (!await _userManager.IsInRoleAsync(approver, SD.Role_Manager))
        {
            throw new UserException(["Approver is not in the Manager role"]);
        }
        if (!await _userManager.IsInRoleAsync(recruiter, SD.Role_HR))
        {
            throw new UserException(["Recruiter is not in the HR role"]);
        }
        if (entity.ScheduleId.HasValue && entity.ScheduleId != Guid.Empty)
        {
            var schedule = _unitOfWork.GetRepository<IScheduleRepository>().GetById(entity.ScheduleId.Value);

            if (schedule == null)
            {
                throw new UserException(["Schedule's id is not exist"]);
            }
        }
        entity.ScheduleId = null;

    }
    public override async Task<bool> UpdateAsync(OfferUpdateDTO updateDTO)
    {
        var offerRepository = _unitOfWork.GetRepository<IOfferRepository>();
        var offer = offerRepository.GetById(updateDTO.Id, "Attrs");
        if (offer == null)
        {
            throw new Exception("Offer not found");
        }
        _mapper.Map(updateDTO, offer);
        await ValidateBusinessLogicUpdateAsync(offer);
        //edit data before update
        offer.UpdatedAt = DateTimeOffset.UtcNow;
        var userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        offer.UpdatedBy = new Guid(userId);
        var attributeValues = _unitOfWork.GetRepository<IAttributeValueRepository>()
                                        .GetQuery(x => updateDTO.LevelsId.Contains(x.Id));
        offer.Attrs!.Clear();
        foreach (var attributeValue in attributeValues)
        {
            offer.Attrs.Add(attributeValue);
        }
        offerRepository.Update(offer);
        return _unitOfWork.SaveChanges() > 0;
    }
    #endregion
    #region update status offer
    public async Task<bool> UpdateStatusOfferAsync(Guid offerId, OfferStatusEnum status)
    {
        var recentOffer = _unitOfWork.GetRepository<IOfferRepository>().GetById(offerId);
        var candidateOffer = _unitOfWork.GetRepository<ICandidateRepository>().GetById(recentOffer.CandidateId);
        if (recentOffer == null)
        {
            throw new UserException(["Entity does not exist in the system"]);
        }

        var recentStatus = recentOffer.Status;
        if (!IsValidStatusChange(recentStatus, status))
        {
            throw new UserException(["Entity has invalid status"]);
        }
        ChangeCandidateStatus(recentOffer, candidateOffer, status);
        var userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        recentOffer.UpdatedBy = new Guid(userId);
        recentOffer.UpdatedAt = DateTimeOffset.UtcNow;
        _unitOfWork.GetRepository<IOfferRepository>().Update(recentOffer);
        _unitOfWork.GetRepository<ICandidateRepository>().Update(candidateOffer);
        return _unitOfWork.SaveChanges() > 0;
    }
    private void ChangeCandidateStatus(Offer recentOffer, Candidate candidate, OfferStatusEnum status)
    {
        switch (status)
        {
            case OfferStatusEnum.WaitingForApproval:
                recentOffer.Status = OfferStatusEnum.WaitingForApproval;
                candidate.Status = (int)CandidateStatusEnum.WaitingForApproval;
                break;
            case OfferStatusEnum.Approved:
                recentOffer.Status = OfferStatusEnum.Approved;
                candidate.Status = (int)CandidateStatusEnum.ApprovedOffer;
                break;
            case OfferStatusEnum.Rejected:
                recentOffer.Status = OfferStatusEnum.Rejected;
                candidate.Status = (int)CandidateStatusEnum.RejectedOffer;
                break;
            case OfferStatusEnum.WaitingForResponse:
                recentOffer.Status = OfferStatusEnum.WaitingForResponse;
                candidate.Status = (int)CandidateStatusEnum.WaitingForResponse;
                break;
            case OfferStatusEnum.AcceptedOffer:
                recentOffer.Status = OfferStatusEnum.AcceptedOffer;
                candidate.Status = (int)CandidateStatusEnum.AcceptedOffer;
                break;
            case OfferStatusEnum.DesclinedOffer:
                recentOffer.Status = OfferStatusEnum.DesclinedOffer;
                candidate.Status = (int)CandidateStatusEnum.DeclinedOffer;
                break;
            case OfferStatusEnum.Cancelled:
                recentOffer.Status = OfferStatusEnum.Cancelled;
                candidate.Status = (int)CandidateStatusEnum.CancelledOffer;
                break;
            default:
                throw new ArgumentException($"Unknown status: {status}");
        }
    }
    private bool IsValidStatusChange(OfferStatusEnum recentStatus, OfferStatusEnum newStatus)
    {
        var validChangesFromWaitingForApproval = new[] { OfferStatusEnum.Approved, OfferStatusEnum.Rejected };
        var validChangesFromWaitingForResponse = new[] { OfferStatusEnum.AcceptedOffer, OfferStatusEnum.DesclinedOffer };

        return (recentStatus == OfferStatusEnum.WaitingForApproval && validChangesFromWaitingForApproval.Contains(newStatus))
                || (recentStatus == OfferStatusEnum.Approved && newStatus == OfferStatusEnum.WaitingForResponse)
                || (recentStatus == OfferStatusEnum.WaitingForResponse && validChangesFromWaitingForResponse.Contains(newStatus))
                || newStatus == OfferStatusEnum.Cancelled;
    }
    #endregion

    #region export excel
    public ExcelPackage ExportExcel(DateTime? fromDate, DateTime? toDate)
    {
        Expression<Func<Offer, bool>> filter = o =>
         (!fromDate.HasValue || o.CreatedAt >= fromDate.Value) &&
         (!toDate.HasValue || o.CreatedAt <= toDate.Value);

        Func<IQueryable<Offer>, IOrderedQueryable<Offer>> order =
            q => q.OrderByDescending(j => j.UpdatedAt).ThenBy(j => j.CreatedAt);

        var offers = _unitOfWork.GetRepository<IOfferRepository>().Get(filter, order, includeProperties: "Candidate,Approver,Department,Position,Schedule,Attrs,Recruiter");
        var offerDTOs = _mapper.Map<List<OfferDTO>>(offers);

        string currentProjectPath = Directory.GetCurrentDirectory();

        // Đường dẫn tương đối
        string relativePath = _configuration["TemplateExcelPath:Offer"] ?? throw new Exception("Cannot find template excel.");

        // Ghép đường dẫn tương đối vào đường dẫn project
        string templateFilePath = currentProjectPath + relativePath;

        FileInfo file = new(templateFilePath);

        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

        ExcelPackage excelPackage = new(file);

        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Offer"];

        int rowNumber = 2;
        int index = 1;
        foreach (var item in offerDTOs)
        {
            //STT
            worksheet.Cells[rowNumber, 1].Value = index;
            index++;
            // Candidate id offer 
            worksheet.Cells[rowNumber, 2].Value = item.CandidateId;
            // Candidate name offer 
            worksheet.Cells[rowNumber, 3].Value = item.Candidate.FullName;
            // Approver name offer 
            worksheet.Cells[rowNumber, 4].Value = item.Approver.FullName;
            // Contract type offer 
            worksheet.Cells[rowNumber, 5].Value = Enum.GetName(typeof(ContractTypeEnum), item.ContractType);
            // Position offer 
            worksheet.Cells[rowNumber, 6].Value = item.Position.Name;
            // Level
            List<string> levels = item.AttributeDTOs == null ? [] : item.AttributeDTOs.Where(a => a.Type == AttributeTypeEnum.Level).Select(a => a.Value).ToList();
            worksheet.Cells[rowNumber, 7].Value = string.Join(", ", levels);
            // Department
            worksheet.Cells[rowNumber, 8].Value = item.Department.Name;
            // Recruiter
            worksheet.Cells[rowNumber, 9].Value = item.Recruiter.FullName;
            // Interviewer

            var scheduleId = item.ScheduleId;
            var schedule = _unitOfWork.GetRepository<IScheduleRepository>().GetById(scheduleId, "Interviewers");
            if (schedule != null)
            {
                worksheet.Cells[rowNumber, 10].Value = string.Join(", ", schedule.Interviewers.Select(i => i.FullName));
            }
            worksheet.Cells[rowNumber, 10].Value = string.Empty;
            // Contract start from
            worksheet.Cells[rowNumber, 11].Value = item.ContractPeriodStart.Value.ToString("yyyy-MM-dd");
            // Contract end 
            worksheet.Cells[rowNumber, 12].Value = item.ContractPeriodEnd.Value.ToString("yyyy-MM-dd");
            //Salary 
            worksheet.Cells[rowNumber, 13].Value = item.BasicSalary;
            //Interview Note 
            worksheet.Cells[rowNumber, 14].Value = schedule != null ? schedule.Note : string.Empty;
            //Note 
            worksheet.Cells[rowNumber, 15].Value = item.Note;
            rowNumber++;
        }

        return excelPackage;
    }
    #endregion
}
