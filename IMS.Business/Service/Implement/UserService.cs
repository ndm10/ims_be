﻿using AutoMapper;
using IMS.Business.DTO;
using IMS.Business.DTO.ApplicationUser;
using IMS.Data;
using IMS.Data.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Claims;
using System.Text;

namespace IMS.Business
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly IEmailSender _emailSender;
        protected readonly UnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(UserManager<ApplicationUser> userManager,
                            RoleManager<ApplicationRole> roleManager,
                            IMapper mapper,
                            UnitOfWork unitOfWork,
                            IConfiguration configuration,
                            IEmailSender emailSender,
                            IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _configuration = configuration;
            _emailSender = emailSender;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<Paginated<UserDTO>> GetUsersAsync(SearchUserRequest searchUserRequest)
        {
            var query = _userManager.Users.AsQueryable();

            if (!string.IsNullOrEmpty(searchUserRequest.Keyword))
            {
                query = query.Where(u => u.UserName.Contains(searchUserRequest.Keyword));
            }

            if (!string.IsNullOrEmpty(searchUserRequest.RoleName))
            {
                var usersInRole = await _userManager.GetUsersInRoleAsync(searchUserRequest.RoleName);
                query = query.Where(u => usersInRole.Contains(u));
            }

            var paginatedUsers = Paginated<ApplicationUser>.Create(query, searchUserRequest.PageIndex, searchUserRequest.PageSize);

            // Return Task<Paginated<UserDTO>>. Also get department name and role name of the user. Don't use automapper to map the ApplicationUser to UserDTO

            return new Paginated<UserDTO>
            {
                Items = paginatedUsers.Items.Select(u => new UserDTO
                {
                    Id = u.Id,
                    UserName = u.UserName,
                    Email = u.Email,
                    Status = u.LockoutEnd.HasValue ? (u.LockoutEnd.Value > DateTimeOffset.Now ? "DeActivated" : "Activate") : "Activate",
                    Gender = u.Gender ? "Male" : "Female",
                    PhoneNumber = u.PhoneNumber,
                    DepartmentName = _unitOfWork.GetRepository<IDepartmentRepository>().GetById(u.DepartmentId).Name,
                    RoleName = _userManager.GetRolesAsync(u).Result.FirstOrDefault()
                }).ToList(),
                PageIndex = paginatedUsers.PageIndex,
                PageSize = paginatedUsers.PageSize,
                TotalPages = paginatedUsers.TotalPages,
                TotalRecord = paginatedUsers.TotalRecord,
            };
        }

        public async Task<UserDTO> GetUserByIdAsync(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());

            if (user == null)
            {
                throw new Exception("User not found");
            }

            var userDto = new UserDTO
            {
                Id = user.Id,
                UserName = user.UserName,
                FullName = user.FullName,
                DateOfBirth = user.DateOfBirth,
                Email = user.Email,
                Status = user.LockoutEnd.HasValue ? (user.LockoutEnd.Value > DateTimeOffset.Now ? "DeActivated" : "Activate") : "Activate",
                Gender = user.Gender ? "Male" : "Female",
                PhoneNumber = user.PhoneNumber,
                DepartmentId = user.DepartmentId,
                DepartmentName = _unitOfWork.GetRepository<IDepartmentRepository>().GetById(user.DepartmentId).Name,
                RoleName = (await _userManager.GetRolesAsync(user)).FirstOrDefault(),
                Note = user.Note,
                Address = user.Address
            };

            return userDto;
        }

        public async Task<UserDTO?> CreateUser(UserCreateDTO userCreateDTO)
        {
            string userName = await GennerateUserNameAsync(userCreateDTO.FullName);

            // Create the ApplicationUser
            var userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var user = new ApplicationUser
            {
                UserName = userName,
                Email = userCreateDTO.Email,
                FullName = userCreateDTO.FullName,
                Gender = userCreateDTO.Gender,
                PhoneNumber = userCreateDTO.PhoneNumber,
                DepartmentId = userCreateDTO.DepartmentId,
                Address = userCreateDTO.Address,
                Note = userCreateDTO.Note,
                DateOfBirth = userCreateDTO.DateOfBirth,
                LockoutEnd = (bool)userCreateDTO.Status ? null : new DateTimeOffset(DateTime.Now.AddYears(100)),
                CreatedAt = DateTimeOffset.UtcNow,
                CreatedBy = Guid.Parse(userId)
            };

            await ValidateUserAsync(user);
            // Create the user with the generated password
            string password = GeneratePassword();
            var result = await _userManager.CreateAsync(user, password);

            if (result.Succeeded)
            {
                // Assign the role to the user
                if (!string.IsNullOrEmpty(userCreateDTO.RoleName))
                {
                    await _userManager.AddToRoleAsync(user, userCreateDTO.RoleName);
                }

                await _emailSender.SendEmailAsync(user.Email, 
                    "Account Information", 
                    "<h1>This is your acccount information</h1>" +
                             $"<p>Account: {user.Email}, Password: {password}</p>");

                var userDto = ToDTOAsync(user).Result;
                return userDto;
            }
            else
            {
                throw new Exception("Create user failed");
            }
        }

        public async Task<bool> ValidateUserAsync(ApplicationUser user)
        {
            var emailExists = await _userManager.Users.AnyAsync(u => u.Email == user.Email && u.Id != user.Id);
            if (emailExists)
            {
                throw new UserException(["Email is existed"]);
            }
            if (user.PhoneNumber is not null)
            {
                var phoneNumberExists = await _userManager.Users.AnyAsync(u => u.PhoneNumber == user.PhoneNumber && u.Id != user.Id);
                if (phoneNumberExists)
                {
                    throw new UserException(["Phone is existed"]);
                }
            }

            return true;
        }

        private string GeneratePassword()
        {
            //const string upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //const string lowerCase = "abcdefghijklmnopqrstuvwxyz";
            //const string number = "0123456789";
            //const string specialCharacters = "!@#$%^&*()";
            //string allCharacters = upperCase + lowerCase + number + specialCharacters;
            //Random random = new Random();
            //StringBuilder passwordBuilder = new StringBuilder();
            //bool hasSpecialCharacters = false;
            //bool hasDigit = false;
            //bool hasUpperCase = false;
            //bool hasLowerCase = false;

            //while (passwordBuilder.Length < 8 
            //    || !hasSpecialCharacters 
            //    || !hasDigit 
            //    || !hasLowerCase 
            //    || !hasUpperCase)
            //{
            //    int randomIndex = random.Next(0, allCharacters.Length);
            //    char randomChar = allCharacters[randomIndex];
            //    passwordBuilder.Append(randomChar);

            //    if (specialCharacters.Contains(randomChar))
            //    {
            //        hasSpecialCharacters = true;
            //    }

            //    if (char.IsDigit(randomChar))
            //    {
            //        hasDigit = true;
            //    }
            //    if (char.IsUpper(randomChar))
            //    {
            //        hasUpperCase = true;
            //    }
            //    if (char.IsLower(randomChar))
            //    {
            //        hasLowerCase = true;
            //    }
            //}

            //return passwordBuilder.ToString();
            return "Admin@123";
        }

        public async Task<UserDTO?> UpdateUser(UserUpdateDTO userUpdateDTO)
        {
            var user = await _userManager.FindByIdAsync(userUpdateDTO.Id.ToString());
            var userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (user == null)
            {
                throw new UserException(["User not found"]);
            }
            if (user.FullName != userUpdateDTO.FullName)
            {
                user.UserName = await GennerateUserNameAsync(userUpdateDTO.FullName);
            }

            user.FullName = userUpdateDTO.FullName;
            user.DateOfBirth = userUpdateDTO.DateOfBirth ?? null;
            user.Email = userUpdateDTO.Email;
            user.PhoneNumber = userUpdateDTO.PhoneNumber;
            user.DepartmentId = userUpdateDTO.DepartmentId;
            user.Note = userUpdateDTO.Note;
            user.Gender = userUpdateDTO.Gender;
            user.Address = userUpdateDTO.Address;
            user.UpdatedAt = DateTimeOffset.UtcNow;
            user.UpdatedBy = Guid.Parse(userId);

            await ValidateUserAsync(user);
            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                await UpdateRoleByUIDAsync(user.Id.ToString(), userUpdateDTO.RoleName);
                return await ToDTOAsync(user);
            }
            else
            {
                throw new Exception("Update user failed");
            }
        }

        private async Task UpdateRoleByUIDAsync(string uid, string newRole)
        {
            var user = await _userManager.FindByIdAsync(uid);
            if (user == null)
            {
                // Handle the case where the user is not found
                return;
            }
            // Get the user's current roles
            var currentRoles = await _userManager.GetRolesAsync(user);
            // Remove the user from their current roles
            await _userManager.RemoveFromRolesAsync(user, currentRoles);
            // Add the user to the new role
            await _userManager.AddToRoleAsync(user, newRole);
        }

        private async Task<string> GennerateUserNameAsync(string fullName)
        {
            string[] nameParts = fullName.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            string firstName = nameParts[^1];
            string initials = string.Join("", nameParts.Take(nameParts.Length - 1).Select(n => n[0]));
            int numberOfExistUser = await _userManager.Users.CountAsync(u => u.FullName == fullName);
            if (numberOfExistUser > 0)
            {
                int incrementalNumber = await _userManager.Users.CountAsync(u => u.FullName == fullName) + 1;
                return $"{firstName}{initials}{incrementalNumber}".ToLower();
            }
            else
            {
                return $"{firstName}{initials}".ToLower();
            }

        }

        private async Task<UserDTO> ToDTOAsync(ApplicationUser user)
        {
            return new UserDTO
            {
                Id = user.Id,
                UserName = user.UserName,
                FullName = user.FullName,
                DateOfBirth = user.DateOfBirth,
                Email = user.Email,
                Status = user.LockoutEnd.HasValue ? (user.LockoutEnd.Value > DateTimeOffset.Now ? "DeActivated" : "Activate") : "Activate",
                Gender = user.Gender ? "Male" : "Female",
                PhoneNumber = user.PhoneNumber,
                DepartmentId = user.DepartmentId,
                DepartmentName = _unitOfWork.GetRepository<IDepartmentRepository>().GetById(user.DepartmentId).Name,
                RoleName = (await _userManager.GetRolesAsync(user)).FirstOrDefault()
            };
        }

        public async Task<UserDTO?> LockUnlock(LockUnLockRequest request)
        {
            var objFromDb = await _userManager.FindByIdAsync(request.Uid);
            var userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (objFromDb == null)
            {
                throw new Exception("User not found");
            }

            if (objFromDb.LockoutEnd != null && objFromDb.LockoutEnd > DateTime.Now)
            {
                //user is currently locked and we need to unlock them
                objFromDb.LockoutEnd = DateTime.Now;
            }
            else
            {
                objFromDb.LockoutEnd = DateTime.Now.AddYears(1000);
            }
            objFromDb.UpdatedAt = DateTimeOffset.UtcNow;
            objFromDb.UpdatedBy = Guid.Parse(userId);
            _userManager.UpdateAsync(objFromDb).GetAwaiter().GetResult();

            return await ToDTOAsync(objFromDb);
        }

        public async Task<ForgotPasswordResponse> ForgotPassword(ForgotPasswordRequest request)
        {
            ForgotPasswordResponse response = new ForgotPasswordResponse();

            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user != null)
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var encodedToken = Encoding.UTF8.GetBytes(token);
                var sendToken = WebEncoders.Base64UrlEncode(encodedToken);
                string url = $"{_configuration["NgWebAppUrl"]}/reset-password?email={request.Email}&token={sendToken}";
                response.IsSuccess = true;
                await _emailSender.SendEmailAsync(request.Email, "Reset Password", "<h1>Follow the instructions to reset your password</h1>" +
                               $"<p>To reset your password <a href='{url}'>Click here</a></p>");
                return response;
            }
            response.IsSuccess = false;
            return response;
        }

        public async Task<bool> ResetPassword(ResetPasswordRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                throw new UserException(["User does not exist"]);
            }
            if (request.NewPassword != request.ConfirmPassword)
            {
                throw new UserException(["Password and confirm password does't not match"]);
            }
            var decodedToken = WebEncoders.Base64UrlDecode(request.Token);
            string pureToken = Encoding.UTF8.GetString(decodedToken);
            var result = await _userManager.ResetPasswordAsync(user, pureToken, request.NewPassword);
            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<List<AppUserDTO>> GetUsersByRoleAsync(string roles)
        {
            string[] roleNames = roles.Split(',', StringSplitOptions.RemoveEmptyEntries);
            List<ApplicationUser> allUsers = new List<ApplicationUser>();

            foreach (var roleName in roleNames)
            {
                var role = await _roleManager.FindByNameAsync(roleName.Trim());

                if (role == null)
                {
                    throw new Exception($"Role '{roleName}' not exist");
                }
                var usersInRole = await _userManager.GetUsersInRoleAsync(roleName.Trim());
                allUsers.AddRange(usersInRole);
            }
            return _mapper.Map<List<AppUserDTO>>(allUsers);
        }

        public async Task<List<string>> GetAllRoles()
        {
            return await _roleManager.Roles.Select(r => r.Name).ToListAsync();
        }

        public async Task<BaseSuccess<Paginated<ApplicationUserDTO>>> GetUsersNotBanByRoleAndUsername(string role,string? textSearch, int? pageIndex, int? pageSize)
        {
            var query = _userManager.Users.AsQueryable();

            var usersInRole = await _userManager.GetUsersInRoleAsync(role);

            // Filter user by role
            query = query.Where(u => usersInRole.Contains(u) && (u.LockoutEnd == null || u.LockoutEnd < DateTimeOffset.UtcNow));

            // Filter user by user name
            if (textSearch != null)
                query = query.Where(u => u.UserName.Contains(textSearch));

            // Order interviewer
            query = query.OrderBy(u => u.UserName);

            pageIndex ??= 1;
            pageSize ??= 10;

            var entitiesPaginated = Paginated<ApplicationUser>.Create(query, pageIndex.Value, pageSize.Value);

            var interviewerDto = _mapper.Map<List<ApplicationUserDTO>>(entitiesPaginated.Items);

            var interviewerPaginated = new Paginated<ApplicationUserDTO>
            {
                PageIndex = entitiesPaginated.PageIndex,
                TotalPages = entitiesPaginated.TotalPages,
                PageSize = entitiesPaginated.PageSize,
                TotalRecord = entitiesPaginated.TotalRecord,
                Items = interviewerDto
            };

            return new BaseSuccess<Paginated<ApplicationUserDTO>>()
            {
                IsSuccess = true,
                Data = interviewerPaginated
            };
        }
    }
}
