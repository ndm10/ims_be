using IMS.Data;
using InterviewManagerment.Business;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Security.Cryptography;

namespace IMS.Business
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ITokenRepository _tokenRepository;

        public AuthenticateService(UserManager<ApplicationUser> userManager,
            ITokenRepository tokenRepository)
        {
            this._userManager = userManager;
            this._tokenRepository = tokenRepository;
        }

        public async Task<LoginResponseDto> LoginAsync(LoginRequestDto request)
        {
            // Check Email
            var identityUser = await _userManager.FindByEmailAsync(request.Email);

            if (identityUser is not null)
            {
                if (await _userManager.IsLockedOutAsync(identityUser))
                {
                    throw new UserException(["User is locked"]);
                }

                // Check Password
                var checkPasswordResult = await _userManager.CheckPasswordAsync(identityUser, request.Password);
                
                if (checkPasswordResult)
                {
                    var roles = await _userManager.GetRolesAsync(identityUser);

                    // Create a Token and Response
                    var jwtToken = _tokenRepository.CreateJwtToken(identityUser, roles.ToList());

                    var response = new LoginResponseDto()
                    {
                        Token = jwtToken,
                        RefreshToken = GenerateRefreshToken(),
                    };

                    //Update user
                    identityUser.RefreshToken = response.RefreshToken;
                    identityUser.RefreshTokenExpirationDateTime = DateTime.Now.AddDays(30);

                    await _userManager.UpdateAsync(identityUser);
                    return response;
                }
            }

            throw new UserException(["Email or Password Incorrect"]);
        }

        public async Task<LoginResponseDto> GenerateNewAccessToken(TokenModel tokenModel)
        {
            if (tokenModel == null)
            {
                throw new UserException(["Invalid client request"]);
            }
            string? jwtToken = tokenModel.Token;
            string? refreshToken = tokenModel.RefreshToken;

            ClaimsPrincipal? principal =
                _tokenRepository.GetPrincipalFromExpiredToken(jwtToken);

            if (principal == null)
            {
                throw new UserException(["Invalid jwt access token"]);
            }

            string? email = principal.FindFirstValue(ClaimTypes.Email);

            ApplicationUser? user = await _userManager.FindByEmailAsync(email);

            if (await _userManager.IsLockedOutAsync(user))
            {
                throw new UserException(["User is locked"]);
            }

            if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpirationDateTime <= DateTime.Now)
            {
                throw new UserException(["Invalid refresh token"]);
            }

            // Create a Token and Response
            var roles = await _userManager.GetRolesAsync(user);
            var newJwtToken = _tokenRepository.CreateJwtToken(user, roles.ToList());

            var response = new LoginResponseDto()
            {
                Token = newJwtToken,
                RefreshToken = GenerateRefreshToken(),
            };

            //Update user
            user.RefreshToken = response.RefreshToken;

            await _userManager.UpdateAsync(user);
            return response;
        }

        private string GenerateRefreshToken()
        {
            byte[] bytes = new byte[64];
            var randomNumberGenerator = RandomNumberGenerator.Create();
            randomNumberGenerator.GetBytes(bytes);

            return Convert.ToBase64String(bytes);
        }
    }
}
