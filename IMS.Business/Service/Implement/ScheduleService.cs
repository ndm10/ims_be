﻿using AutoMapper;
using IMS.Business.DTO.Schedule;
using IMS.Business.Model;
using IMS.Business.Service.Interface;
using IMS.Data;
using IMS.Data.Repository.Interface;
using IMS.Data.Utility.Enum;
using LinqKit;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Security.Claims;
using IMS.Business.Helper.HubHelper;

namespace IMS.Business.Service.Implement
{
    public class ScheduleService(UnitOfWork unitOfWork,
        IMapper mapper,
        UserManager<ApplicationUser> userManager,
        NotificationHubHelper notificationHub) : IScheduleService
    {
        public async Task<BaseSuccess<Schedule>> AddSchedule(ScheduleCreateDTO scheduleCreateDto, Claim? userLoginId)
        {
            var scheduleRepository = unitOfWork.GetRepository<IScheduleRepository>();
            var response = new BaseSuccess<Schedule>();

            // Check if userLoginId is null
            if (userLoginId == null)
            {
                response.MessageResponse = "Require login";
                response.IsSuccess = false;
                return response;
            }


            // Parse id of current login user to Guid type
            var createdBy = Guid.Parse(userLoginId.Value);

            // Check Recruiter Owner is Recruiter or not
            var recruiterOwner = userManager.FindByIdAsync(scheduleCreateDto.RecruiterOwnerId.ToString()).Result;
            if (recruiterOwner == null)
            {
                response.MessageResponse = "Recruiter not found";
                response.IsSuccess = false;
                return response;
            }
            var roles = userManager.GetRolesAsync(recruiterOwner).Result;
            if (!roles.Contains("Recruiter"))
            {
                response.MessageResponse = "Recruiter Owner is not valid";
                response.IsSuccess = false;
                return response;
            }

            // Check if Schedule with Candidate and Job is existed or not
            var scheduleExist = scheduleRepository.FindByCandidateAndJob(scheduleCreateDto.CandidateId, scheduleCreateDto.JobId);
            if (scheduleExist != null)
            {
                response.MessageResponse = "Schedule with candidate and job already exist";
                response.IsSuccess = false;
                return response;
            }

            // Check if Schedule date of Candidate is overlapping or not
            Expression<Func<Schedule, bool>> filterCandidate = s => s.CandidateId == scheduleCreateDto.CandidateId;
            bool isCandidateOverLapping = scheduleRepository.IsOverLapping(filterCandidate, scheduleCreateDto.ScheduleDate, scheduleCreateDto.StartTime, scheduleCreateDto.EndTime);
            if (isCandidateOverLapping)
            {
                response.MessageResponse = "Candidate is overlapping with another schedule";
                response.IsSuccess = false;
                return response;
            }

            // Mapping ScheduleCreateDTO to Schedule
            var schedule = mapper.Map<Schedule>(scheduleCreateDto);

            // Set status for Schedule and other properties
            schedule.Status = (int)ScheduleStatusEnum.Open;
            schedule.IsDeleted = false;
            schedule.CreatedAt = DateTimeOffset.UtcNow;
            schedule.Id = Guid.NewGuid();
            schedule.CreatedBy = createdBy;

            // Get all interviewers
            foreach (var interviewerId in scheduleCreateDto.InterviewerId)
            {
                // Check interviewer is existed or not
                var interviewer = userManager.FindByIdAsync(interviewerId.ToString()).Result;
                if (interviewer == null)
                {
                    response.MessageResponse = "Interviewer not found";
                    response.IsSuccess = false;
                    return response;
                }

                // Check interviewer is role Interviewer or not
                roles = userManager.GetRolesAsync(interviewer).Result;
                if (!roles.Contains("Interviewer"))
                {
                    response.MessageResponse = "Interviewer is not valid";
                    response.IsSuccess = false;
                    return response;
                }

                // Check interviewer is overlapping with another schedule or not
                Expression<Func<Schedule, bool>> filterInterviewer = s => s.Interviewers.Any(i => i.Id == interviewerId);
                var isInterviewerOverLapping = scheduleRepository.IsOverLapping(filterInterviewer, scheduleCreateDto.ScheduleDate, scheduleCreateDto.StartTime, scheduleCreateDto.EndTime);
                if (isInterviewerOverLapping)
                {
                    response.MessageResponse = $"Interviewer {interviewer.UserName} is overlapping with another schedule";
                    response.IsSuccess = false;
                    return response;
                }

                schedule.Interviewers.Add(interviewer);
            }

            // Add Schedule to database
            scheduleRepository.Add(schedule);
            unitOfWork.SaveChanges();

            response.MessageResponse = "Create schedule success";
            response.IsSuccess = true;

            // Notify to all interviewers and recruiter owner (if not create by role recruiter owner)
            foreach (var interviewer in schedule.Interviewers)
            {
                await notificationHub.SendMessageToUser("Notify", interviewer.Id, $"You have a new interview schedule");
            }
            // Check if schedule is created by recruiter owner or not
            if (schedule.RecruiterOwnerId != createdBy)
            {
                await notificationHub.SendMessageToUser("Notify", schedule.RecruiterOwnerId, $"You have a new interview schedule");
            }

            return response;
            return response;
        }

        public BaseSuccess<ScheduleDTO> GetScheduleById(Guid id, Claim? userLoginId)
        {
            // Check userLoginId is null or not
            if (userLoginId == null)
            {
                return new BaseSuccess<ScheduleDTO>
                {
                    MessageResponse = "Require login",
                    IsSuccess = false,
                };
            }

            var userLoginIdValue = userLoginId.Value;

            // Get role of user
            var user = userManager.FindByIdAsync(userLoginIdValue).Result;
            if (user == null)
            {
                return new BaseSuccess<ScheduleDTO>
                {
                    MessageResponse = "Error with authenticate",
                    IsSuccess = false,
                };
            }
            var roles = userManager.GetRolesAsync(user).Result;

            // Find schedule by id
            var schedule = unitOfWork.GetRepository<IScheduleRepository>().FindById(id);

            // Check if schedule is exist
            if (schedule == null)
            {
                return new BaseSuccess<ScheduleDTO>
                {
                    MessageResponse = "Schedule not found",
                    IsSuccess = false,
                };
            }


            // Check role of user have permission to view or not
            if (roles.Contains(Constant.RECRUITER_ROLE))
            {
                if (schedule.RecruiterOwnerId != Guid.Parse(userLoginIdValue))
                {
                    return new BaseSuccess<ScheduleDTO>
                    {
                        MessageResponse = "You do not have permission to view this schedule",
                        IsSuccess = false,
                    };
                }
            }
            else if (roles.Contains(Constant.INTERVIEWER_ROLE))
            {
                if (schedule.Interviewers.All(i => i.Id != Guid.Parse(userLoginIdValue)))
                {
                    return new BaseSuccess<ScheduleDTO>
                    {
                        MessageResponse = "You do not have permission to view this schedule",
                        IsSuccess = false,
                    };
                }
            }


            // Mapping Schedule to ScheduleDTO
            var scheduleDto = mapper.Map<ScheduleDTO>(schedule);

            return new BaseSuccess<ScheduleDTO>
            {
                Data = scheduleDto,
                IsSuccess = true,
            };
        }

        public BaseSuccess<Paginated<ScheduleDTO>> GetSchedules(string? textSearch, int? status, Guid? interviewer,
            int? pageIndex, int? pageSize, Claim? userLoginId)
        {
            var scheduleRepository = unitOfWork.GetRepository<IScheduleRepository>();

            // Check userLoginId is null or not
            if (userLoginId == null)
            {
                return new BaseSuccess<Paginated<ScheduleDTO>>
                {
                    IsSuccess = false,
                    MessageResponse = "Require login"
                };
            }

            var userLoginIdValue = userLoginId.Value;

            // Get role of current user login
            var user = userManager.FindByIdAsync(userLoginIdValue).Result;
            if (user == null)
            {
                return new BaseSuccess<Paginated<ScheduleDTO>>
                {
                    IsSuccess = false,
                    MessageResponse = "Error with authenticate"
                };
            }
            var roles = userManager.GetRolesAsync(user).Result;


            Expression<Func<Schedule, bool>> predicate = x => true;

            //Filter schedule list by role
            if (roles.Contains(Constant.RECRUITER_ROLE))
            {
                predicate = predicate.And(x => x.RecruiterOwnerId.ToString() == userLoginIdValue);
            }
            else if (roles.Contains(Constant.INTERVIEWER_ROLE))
            {
                predicate = predicate.And(x => x.Interviewers.Any(i => i.Id.ToString() == userLoginIdValue));
            }

            // Filter by textSearch and status and interviewer
            if (!string.IsNullOrEmpty(textSearch))
            {
                var pattern = $"%{textSearch}%";
                predicate = predicate.And(x => EF.Functions.Like(x.Title, pattern)
                                                || EF.Functions.Like(x.Candidate.FullName, pattern)
                                                || EF.Functions.Like(x.Job.Title, pattern));
            }
            if (status != null)
            {
                predicate = predicate.And(x => x.Status == status);
            }
            if (interviewer != null)
            {
                predicate = predicate.And(x => x.Interviewers.Any(i => i.Id == interviewer));
            }

            // Include properties
            Expression<Func<Schedule, object>> jobInclude = x => x.Job;
            Expression<Func<Schedule, object>> interviewersInclude = x => x.Interviewers;
            Expression<Func<Schedule, object>> candidateInclude = x => x.Candidate;
            Expression<Func<Schedule, object>> recruiterOwnerInclude = x => x.RecruiterOwner;
            Expression<Func<Schedule, object>> createdByUserInclude = x => x.CreatedByUser;
            Expression<Func<Schedule, object>> updatedByUserInclude = x => x.UpdatedByUser;

            // Get schedules
            var query = scheduleRepository.GetInclude(
                                            filter: predicate,
                                            orderBy: OrderBy,
                                            isGetDeleted: false,
                                            includes: [jobInclude, interviewersInclude, candidateInclude, recruiterOwnerInclude, createdByUserInclude, updatedByUserInclude]
                                            );

            // Set value for pageIndex and pageSize
            pageIndex ??= 1;
            pageSize ??= 10;

            // Get schedule by paging
            var schedulesPaginated = Paginated<Schedule>.Create(query, pageIndex.Value, pageSize.Value);

            // Map Schedule to ScheduleDTO
            var schedulesDto = mapper.Map<List<ScheduleDTO>>(schedulesPaginated.Items);

            var schedulesDtoPaginated = new Paginated<ScheduleDTO>
            {
                PageIndex = schedulesPaginated.PageIndex,
                PageSize = schedulesPaginated.PageSize,
                TotalPages = schedulesPaginated.TotalPages,
                TotalRecord = schedulesPaginated.TotalRecord,
                Items = schedulesDto,
            };

            return new BaseSuccess<Paginated<ScheduleDTO>>
            {
                IsSuccess = true,
                Data = schedulesDtoPaginated
            };

            // Order by
            IOrderedQueryable<Schedule> OrderBy(IQueryable<Schedule> x) =>
                x.OrderByDescending(schedule => schedule.CreatedAt).ThenByDescending(schedule => schedule.UpdatedAt);
        }

        public List<ScheduleDTO> GetScheduleByCandidateId(Guid id)
        {
            // Find candidate by id
            var candidate = unitOfWork.GetRepository<ICandidateRepository>().GetById(id);

            // Check if candidate is existed
            if (candidate != null)
            {
                Expression<Func<Schedule, bool>> filter = s => s.CandidateId == id;
                var query = unitOfWork.GetRepository<IScheduleRepository>().Get(filter);
                return mapper.Map<List<ScheduleDTO>>(query.ToList());
            }

            throw new UserException(["Candidate not found"]);
        }

        public BaseSuccess<Schedule> UpdateScheduleDetails(Claim? userId, ScheduleUpdateDTO scheduleUpdateDto)
        {
            var scheduleRepository = unitOfWork.GetRepository<IScheduleRepository>();

            // Check userLoginId is null or not
            if (userId == null)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Require login"
                };
            }

            var userLoginIdValue = userId.Value;

            // Get role of current user login
            var user = userManager.FindByIdAsync(userLoginIdValue).Result;
            if (user == null)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Error with authenticate"
                };
            }
            var roles = userManager.GetRolesAsync(user).Result;

            // Get current schedule by id
            var schedule = scheduleRepository.FindById(scheduleUpdateDto.Id);

            // Check if schedule is exist or not
            if (schedule == null)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Schedule not found"
                };
            }

            // Check current status of schedule, if status is not Open, does not allow update
            if (schedule.Status != (int)ScheduleStatusEnum.Open)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Can not update this schedule"
                };
            }

            // Check role of user is had permission to update
            if (roles.Contains(Constant.RECRUITER_ROLE))
            {
                if (schedule.RecruiterOwnerId != Guid.Parse(userLoginIdValue))
                {
                    return new BaseSuccess<Schedule>
                    {
                        IsSuccess = false,
                        MessageResponse = "You not have permission to edit this schedule"
                    };
                }
            }
            else if (roles.Contains(Constant.MANAGER_ROLE))
            {
                if (schedule.CreatedBy != Guid.Parse(userLoginIdValue))
                {
                    return new BaseSuccess<Schedule>
                    {
                        IsSuccess = false,
                        MessageResponse = "You not have permission to edit this schedule"
                    };
                }
            }

            // Update schedule with new information
            schedule.Title = scheduleUpdateDto.Title;
            schedule.JobId = scheduleUpdateDto.JobId;
            schedule.CandidateId = scheduleUpdateDto.CandidateId;
            schedule.ScheduleDate = scheduleUpdateDto.ScheduleDate;
            schedule.StartTime = scheduleUpdateDto.StartTime;
            schedule.EndTime = scheduleUpdateDto.EndTime;
            schedule.Location = scheduleUpdateDto.Location;
            schedule.RecruiterOwnerId = scheduleUpdateDto.RecruiterOwnerId;
            schedule.Note = scheduleUpdateDto.Note;
            schedule.MeetingId = scheduleUpdateDto.MeetingId;
            schedule.Result = scheduleUpdateDto.Result;

            schedule.UpdatedAt = DateTime.UtcNow;
            schedule.UpdatedBy = Guid.Parse(userLoginIdValue);

            // Remove all old interviewer
            schedule.Interviewers = new List<ApplicationUser>();

            // Get all interviewers and update with new interviewer
            foreach (var interviewerId in scheduleUpdateDto.InterviewerId)
            {
                var interviewer = userManager.FindByIdAsync(interviewerId.ToString()).Result;
                if (interviewer == null)
                {
                    return new BaseSuccess<Schedule>
                    {
                        MessageResponse = "Interviewer not found",
                        IsSuccess = false,
                    };
                }

                // Check interviewer is role Interviewer or not
                roles = userManager.GetRolesAsync(interviewer).Result;
                if (!roles.Contains("Interviewer"))
                {
                    return new BaseSuccess<Schedule>
                    {
                        MessageResponse = "Interviewer is not valid",
                        IsSuccess = false,
                    };
                }

                schedule.Interviewers.Add(interviewer);
            }

            // Update schedule to database
            scheduleRepository.Update(schedule);
            unitOfWork.SaveChanges();

            return new BaseSuccess<Schedule>
            {
                IsSuccess = true,
                MessageResponse = "Update schedule successful"
            };
        }

        public BaseSuccess<Schedule> UpdateScheduleResult(Claim? userId, ScheduleUpdateResultDTO scheduleUpdateResultDto)
        {
            var scheduleRepository = unitOfWork.GetRepository<IScheduleRepository>();

            // Check userLoginId is null or not
            if (userId == null)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Require login"
                };
            }

            var userLoginIdValue = userId.Value;

            // Get role of current user login
            var user = userManager.FindByIdAsync(userLoginIdValue).Result;
            if (user == null)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Error with authenticate"
                };
            }

            // Get current schedule by id
            var schedule = scheduleRepository.FindById(scheduleUpdateResultDto.Id);

            // Check if schedule is exist or not
            if (schedule == null)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Schedule not found"
                };
            }

            // Check current status of schedule, if status is not Open, does not allow update
            if (schedule.Status != (int)ScheduleStatusEnum.Open)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Can not update this schedule"
                };
            }

            // Update schedule result
            schedule.Note = scheduleUpdateResultDto.Note ?? schedule.Note;
            schedule.Result = scheduleUpdateResultDto.Result;
            schedule.UpdatedAt = DateTime.UtcNow;
            schedule.UpdatedBy = user.Id;

            // Update schedule
            scheduleRepository.Update(schedule);
            unitOfWork.SaveChanges();

            return new BaseSuccess<Schedule>
            {
                IsSuccess = true,
                MessageResponse = "Update schedule result successful"
            };
        }

        public BaseSuccess<Schedule> CancelSchedule(Claim? userId, ScheduleCancelDTO scheduleCancelDto)
        {
            var scheduleRepository = unitOfWork.GetRepository<IScheduleRepository>();

            // Check userLoginId is null or not
            if (userId == null)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Require login"
                };
            }

            var userLoginIdValue = userId.Value;

            // Get role of current user login
            var user = userManager.FindByIdAsync(userLoginIdValue).Result;
            if (user == null)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Error with authenticate"
                };
            }
            var roles = userManager.GetRolesAsync(user).Result;

            // Get current schedule by id
            var schedule = scheduleRepository.FindById(scheduleCancelDto.Id);

            // Check if schedule is exist or not
            if (schedule == null)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Schedule not found"
                };
            }

            // If role is Recruiter then check schedule belong to this recruiter or not
            // Check role of user is had permission to update
            if (roles.Contains(Constant.RECRUITER_ROLE))
            {
                if (schedule.RecruiterOwnerId != Guid.Parse(userLoginIdValue))
                {
                    return new BaseSuccess<Schedule>
                    {
                        IsSuccess = false,
                        MessageResponse = "You not have permission to edit this schedule"
                    };
                }
            }

            // Check current status of schedule, if status is not Open , does not allow update
            if (schedule.Status != (int)ScheduleStatusEnum.Open)
            {
                return new BaseSuccess<Schedule>
                {
                    IsSuccess = false,
                    MessageResponse = "Can not cancel this schedule"
                };
            }

            // Update schedule status to cancel
            schedule.Status = (int)ScheduleStatusEnum.Cancelled;
            schedule.UpdatedAt = DateTime.UtcNow;
            schedule.UpdatedBy = user.Id;

            // Update schedule
            scheduleRepository.Update(schedule);
            unitOfWork.SaveChanges();

            return new BaseSuccess<Schedule>
            {
                IsSuccess = true,
                MessageResponse = "Cancel schedule successful"
            };
        }
    }
}
