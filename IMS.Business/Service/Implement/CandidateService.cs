﻿using AutoMapper;
using IMS.Business.DTO.Candidate;
using IMS.Data;
using IMS.Data.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;
using System.Security.Claims;

namespace IMS.Business;

public class CandidateService : BaseServices<Candidate, CandidateDTO, CandidateCreateDTO, CandidateUpdateDTO>, ICandidateService
{
    private readonly ILogger<CandidateRepository> _logger;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public CandidateService(
        UnitOfWork unitOfWork,
        ILogger<CandidateRepository> logger,
        IMapper mapper,
        UserManager<ApplicationUser> userManager,
        IHttpContextAccessor httpContextAccessor) : base(unitOfWork, mapper)
    {
        _logger = logger;
        _userManager = userManager;
        _httpContextAccessor = httpContextAccessor;
    }

    public Paginated<CandidateListDTO> GetCandidateBySearchAndStatus(string? search, CandidateStatusEnum? status, int pageIndex, int pageSize)
    {
        Expression<Func<Candidate, bool>> filter = c => (search == null
                                                        || c.FullName.Contains(search)
                                                        || c.Address.Contains(search)
                                                        || c.PhoneNumber.Contains(search)
                                                        || c.Email.Contains(search)
                                                        || c.Note.Contains(search))
                                                    && (status == null ? true : c.Status == (int)status);
        Func<IQueryable<Candidate>, IOrderedQueryable<Candidate>> orderByDescendingUpdate = q => q.OrderByDescending(c => c.UpdatedBy != null ? c.UpdatedAt : c.CreatedAt);

        var query = _unitOfWork.GetRepository<ICandidateRepository>().Get(filter, orderByDescendingUpdate, "Attrs,Position,OwnerHR", false);
        var entitiesPaginated = Paginated<Candidate>.Create(query, pageIndex, pageSize);
        var candidateDTOs = _mapper.Map<List<CandidateListDTO>>(entitiesPaginated.Items);
        return new Paginated<CandidateListDTO>
        {
            PageIndex = entitiesPaginated.PageIndex,
            TotalPages = entitiesPaginated.TotalPages,
            PageSize = entitiesPaginated.PageSize,
            TotalRecord = entitiesPaginated.TotalRecord,
            Items = candidateDTOs
        };
    }

    public List<CandidateOfferDTO> GetCandidatesWithoutBanStatus()
    {
        Expression<Func<Candidate, bool>> filter = c => c.Status != (int)CandidateStatusEnum.Banned;
        Func<IQueryable<Candidate>, IOrderedQueryable<Candidate>> orderByDescendingUpdate = q => q.OrderByDescending(c => c.UpdatedBy);

        var candidates = _unitOfWork.GetRepository<ICandidateRepository>().Get(filter, orderByDescendingUpdate).Select(c => new CandidateOfferDTO() { Id = c.Id, Email = c.Email, FullName = c.FullName }).ToList();
        return _mapper.Map<List<CandidateOfferDTO>>(candidates);
    }

    public override CandidateDTO Add(CandidateCreateDTO createDTO)
    {
        CheckCreatedDataDTO(createDTO);
        ChangeCreatedDataBeforeMapping(createDTO);

        var entity = _mapper.Map<Candidate>(createDTO);

        ChangeCreatedDataAfterMapping(createDTO, entity);

        _unitOfWork.GetRepository<ICandidateRepository>().Add(entity);
        _unitOfWork.SaveChanges();
        return _mapper.Map<CandidateDTO>(entity);
    }

    public override bool Update(CandidateUpdateDTO updateDTO)
    {
        CheckUpdatedDataDTO(updateDTO);
        ChangeUpdatedDataBeforeMapping(updateDTO);

        var oldEntity = _unitOfWork.GetRepository<ICandidateRepository>().GetById(updateDTO.Id, "Attrs,Position");
        if (oldEntity == null)
        {
            throw new UserException(["Candidate not found"]);
        }

        _mapper.Map(updateDTO, oldEntity);

        ChangeUpdatedDataAfterMapping(updateDTO, oldEntity);

        _unitOfWork.GetRepository<ICandidateRepository>().Update(oldEntity);
        return _unitOfWork.SaveChanges() > 0;
    }

    public override bool Delete(Guid id)
    {
        var entity = _unitOfWork.GetRepository<ICandidateRepository>().GetById(id) ??
                throw new UserException(["Entity doesn't exist"]);

        entity.IsDeleted = true;
        return _unitOfWork.SaveChanges() > 0;
    }

    public bool BanCandidate(Guid id)
    {
        var entity = _unitOfWork.GetRepository<ICandidateRepository>().GetById(id) ??
                throw new UserException(["Entity doesn't exist"]);

        var userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        entity.UpdatedAt = DateTimeOffset.Now;
        entity.UpdatedBy = new Guid(userId);
        entity.Status = (int)CandidateStatusEnum.Banned;
        return _unitOfWork.SaveChanges() > 0;
    }

    private void CheckCreatedDataDTO(CandidateCreateDTO createDTO)
    {
        if (createDTO == null)
        {
            throw new UserException(["Created entity is empty"]);
        }
        if (_unitOfWork.GetRepository<ICandidateRepository>()
                                        .GetQuery(x => x.PhoneNumber == createDTO.PhoneNumber).Any())
        {
            throw new UserException(["Phone number already exists"]);
        }
        if (_unitOfWork.GetRepository<ICandidateRepository>()
                                        .GetQuery(x => x.Email == createDTO.Email).Any())
        {
            throw new UserException(["Email already exists"]);
        }
    }

    private void ChangeCreatedDataBeforeMapping(CandidateCreateDTO createDTO)
    {
        if (createDTO.ResumeFile != null && createDTO.ResumeFile.Length > 0)
        {
            using (var ms = new MemoryStream())
            {
                createDTO.ResumeFile.CopyTo(ms);
                createDTO.Resume = ms.ToArray();
            }
        }
        else
        {
            throw new UserException(["Resume is required"]);
        }
    }

    private void ChangeCreatedDataAfterMapping(CandidateCreateDTO createDTO, Candidate entity)
    {
        entity.Id = Guid.NewGuid();
        var userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        entity.CreatedAt = DateTimeOffset.UtcNow;
        entity.CreatedBy = new Guid(userId);

        var ownerHR = _userManager.FindByIdAsync(createDTO.AccountId.ToString()).Result;

        if (ownerHR == null)
        {
            throw new UserException(["Owner HR is null"]);
        }
        entity.OwnerHR = ownerHR;
        var attributeValues = _unitOfWork.GetRepository<IAttributeValueRepository>()
                                        .GetQuery(x => createDTO.SkillsId.Contains(x.Id)
                                                || createDTO.HighestLevelId == x.Id);
        var position = _unitOfWork.GetRepository<IPositionRepository>()
                                        .GetById(createDTO.PositionId);
        if (position != null)
        {
            entity.Position = position;
        }
        foreach (var attributeValue in attributeValues)
        {
            entity.Attrs.Add(attributeValue);
        }
    }

    private void CheckUpdatedDataDTO(CandidateUpdateDTO updateDTO)
    {
        if (updateDTO == null)
        {
            throw new UserException(["Updated entity is empty"]);
        }
        if (_unitOfWork.GetRepository<ICandidateRepository>()
                                        .GetQuery(x => x.Id != updateDTO.Id && x.PhoneNumber == updateDTO.PhoneNumber).Any())
        {
            throw new UserException(["Phone number already exists"]);
        }
        if (_unitOfWork.GetRepository<ICandidateRepository>()
                                        .GetQuery(x => x.Id != updateDTO.Id && x.Email == updateDTO.Email).Any())
        {
            throw new UserException(["Email already exists"]);
        }
    }

    private void ChangeUpdatedDataBeforeMapping(CandidateUpdateDTO updateDTO)
    {
        if (updateDTO.ResumeFile != null && updateDTO.ResumeFile.Length > 0)
        {
            using (var ms = new MemoryStream())
            {
                updateDTO.ResumeFile.CopyTo(ms);
                updateDTO.Resume = ms.ToArray();
            }
        }
        else
        {
            throw new UserException(["Resume is required"]);
        }
    }

    private void ChangeUpdatedDataAfterMapping(CandidateUpdateDTO updateDTO, Candidate oldEntity)
    {
        oldEntity.Attrs.Clear();
        var userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        oldEntity.UpdatedAt = DateTimeOffset.UtcNow;
        oldEntity.UpdatedBy = new Guid(userId);
        var attributeValues = _unitOfWork.GetRepository<IAttributeValueRepository>()
                                        .GetQuery(x => updateDTO.SkillsId.Contains(x.Id)
                                                || updateDTO.HighestLevelId == x.Id);
        var position = _unitOfWork.GetRepository<IPositionRepository>()
                                        .GetById(updateDTO.PositionId);
        if (position != null)
        {
            oldEntity.Position = position;
        }
        foreach (var attributeValue in attributeValues)
        {
            oldEntity.Attrs.Add(attributeValue);
        }
        var ownerHR = _userManager.FindByIdAsync(updateDTO.AccountId.ToString()).Result;

        if (ownerHR == null)
        {
            throw new UserException(["Owner HR is null"]);
        }
        oldEntity.OwnerHR = ownerHR;
    }

    public BaseSuccess<Paginated<CandidateListDTO>> GetCandidateByFilterByFullName(string? textSearch,int? status, int? pageIndex, int? pageSize)
    {
        Expression<Func<Candidate, bool>> filter = c => (textSearch == null || c.FullName.Contains(textSearch) &&
                                                        (status == null || c.Status == status.Value));

        IOrderedQueryable<Candidate> OrderBy(IQueryable<Candidate> q)
            => q.OrderBy(c => c.FullName);

        Expression<Func<Candidate, object>> attrsInclude = x => x.Attrs;
        Expression<Func<Candidate, object>> positionInclude = x => x.Position;
        Expression<Func<Candidate, object>> ownerHrInclude = x => x.OwnerHR;

        var query = _unitOfWork.GetRepository<ICandidateRepository>()
            .GetInclude(filter, OrderBy, false, [attrsInclude,positionInclude,ownerHrInclude]);

        pageIndex ??= 1;
        pageSize ??= 10;

        var entitiesPaginated = Paginated<Candidate>.Create(query, pageIndex.Value, pageSize.Value);
        var candidatesDto = _mapper.Map<List<CandidateListDTO>>(entitiesPaginated.Items);

        var candidatePaginated = new Paginated<CandidateListDTO>
        {
            PageIndex = entitiesPaginated.PageIndex,
            TotalPages = entitiesPaginated.TotalPages,
            PageSize = entitiesPaginated.PageSize,
            TotalRecord = entitiesPaginated.TotalRecord,
            Items = candidatesDto
        };

        return new BaseSuccess<Paginated<CandidateListDTO>>()
        {
            IsSuccess = true,
            Data = candidatePaginated
        };
    }
}
