﻿using IMS.Data;
using IMS.Data.Repository.Interface;
using IMS.Data.Utility.Enum;
using Microsoft.EntityFrameworkCore;

namespace IMS.Business;

public class DashboardService : IDashboardService
{
    protected readonly UnitOfWork _unitOfWork;

    public DashboardService(UnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public CardItemDTO GetCandidatesParameter()
    {
        var totalCandidate = _unitOfWork.GetRepository<ICandidateRepository>().Get().LongCount();
        var currentDate = DateTime.Now;
        var previousDate = DateTime.Now.AddMonths(-1);

        var currentAvg = (float)_unitOfWork.GetRepository<ICandidateRepository>()
        .Get(x => x.CreatedAt.Year == currentDate.Year
                && x.CreatedAt.Month == currentDate.Month
                && x.CreatedAt.Day < currentDate.Day).LongCount() / currentDate.Day;

        var previousAvg = (float)_unitOfWork.GetRepository<ICandidateRepository>()
        .Get(x => x.CreatedAt.Year == previousDate.Year
                && x.CreatedAt.Month == previousDate.Month
                && x.CreatedAt.Day < previousDate.Day).LongCount() / previousDate.Day;

        double percentage = 0;

        if (currentAvg <= 0 && previousAvg <= 0)
        {
            percentage = 0;
        }
        else if (currentAvg <= 0)
        {
            percentage = -previousAvg * 100;
        }
        else if (previousAvg <= 0)
        {
            percentage = currentAvg * 100;
        }
        else
        {
            percentage = (currentAvg - previousAvg) / previousAvg * 100;
        }

        return new CardItemDTO
        {
            Total = totalCandidate,
            Percentage = percentage
        };
    }

    public CardItemDTO GetJobsParameter()
    {
        var totalJob = _unitOfWork.GetRepository<IJobRepository>().Get().LongCount();
        var currentDate = DateTime.Now;
        var previousDate = DateTime.Now.AddMonths(-1);

        var currentAvg = (float)_unitOfWork.GetRepository<IJobRepository>()
        .Get(x => x.CreatedAt.Year == currentDate.Year
                && x.CreatedAt.Month == currentDate.Month
                && x.CreatedAt.Day < currentDate.Day).LongCount() / currentDate.Day;

        var previousAvg = (float)_unitOfWork.GetRepository<IJobRepository>()
        .Get(x => x.CreatedAt.Year == previousDate.Year
                && x.CreatedAt.Month == previousDate.Month
                && x.CreatedAt.Day < previousDate.Day).LongCount() / previousDate.Day;

        double percentage = 0;

        if (currentAvg <= 0 && previousAvg <= 0)
        {
            percentage = 0;
        }
        else if (currentAvg <= 0)
        {
            percentage = -previousAvg * 100;
        }
        else if (previousAvg <= 0)
        {
            percentage = currentAvg * 100;
        }
        else
        {
            percentage = (currentAvg - previousAvg) / previousAvg * 100;
        }

        return new CardItemDTO
        {
            Total = totalJob,
            Percentage = percentage
        };
    }

    public CardItemDTO GetInterviewsParameter()
    {
        var totalInterview = _unitOfWork.GetRepository<IScheduleRepository>().Get().LongCount();
        var currentDate = DateTime.Now;
        var previousDate = DateTime.Now.AddMonths(-1);

        var currentAvg = (float)_unitOfWork.GetRepository<IJobRepository>()
        .Get(x => x.CreatedAt.Year == currentDate.Year
                && x.CreatedAt.Month == currentDate.Month
                && x.CreatedAt.Day < currentDate.Day).LongCount() / currentDate.Day;

        var previousAvg = (float)_unitOfWork.GetRepository<IJobRepository>()
        .Get(x => x.CreatedAt.Year == previousDate.Year
                && x.CreatedAt.Month == previousDate.Month
                && x.CreatedAt.Day < previousDate.Day).LongCount() / previousDate.Day;

        double percentage = 0;

        if (currentAvg <= 0 && previousAvg <= 0)
        {
            percentage = 0;
        }
        else if (currentAvg <= 0)
        {
            percentage = -previousAvg * 100;
        }
        else if (previousAvg <= 0)
        {
            percentage = currentAvg * 100;
        }
        else
        {
            percentage = (currentAvg - previousAvg) / previousAvg * 100;
        }

        return new CardItemDTO
        {
            Total = totalInterview,
            Percentage = percentage
        };
    }

    public CardItemDTO GetOffersParameter()
    {
        var totalOffer = _unitOfWork.GetRepository<IOfferRepository>().Get().LongCount();
        var currentDate = DateTime.Now;
        var previousDate = DateTime.Now.AddMonths(-1);

        var currentAvg = (float)_unitOfWork.GetRepository<IOfferRepository>()
        .Get(x => x.CreatedAt.Year == currentDate.Year
                && x.CreatedAt.Month == currentDate.Month
                && x.CreatedAt.Day < currentDate.Day).LongCount() / currentDate.Day;

        var previousAvg = (float)_unitOfWork.GetRepository<IOfferRepository>()
        .Get(x => x.CreatedAt.Year == previousDate.Year
                && x.CreatedAt.Month == previousDate.Month
                && x.CreatedAt.Day < previousDate.Day).LongCount() / previousDate.Day;

        double percentage = 0;

        if (currentAvg <= 0 && previousAvg <= 0)
        {
            percentage = 0;
        }
        else if (currentAvg <= 0)
        {
            percentage = -previousAvg * 100;
        }
        else if (previousAvg <= 0)
        {
            percentage = currentAvg * 100;
        }
        else
        {
            percentage = (currentAvg - previousAvg) / previousAvg * 100;
        }

        return new CardItemDTO
        {
            Total = totalOffer,
            Percentage = percentage
        };
    }

    public List<ChartDTO> CountInterview(int dateType)
    {
        DateTypeEnum dateTypeEnum = (DateTypeEnum)dateType;
        switch (dateTypeEnum)
        {
            case DateTypeEnum.Week:
                return CountInterviewByWeek();
            case DateTypeEnum.Month:
                return CountInterviewByMonth();
            case DateTypeEnum.Year:
                return CountInterviewByYear();
            default:
                throw new UserException(["Your request isn't exist"]);
        }
    }

    private List<ChartDTO> CountInterviewByWeek()
    {
        var chartDTOs = new List<ChartDTO>();
        var currentDate = DateTime.Now;
        var dayNumber = currentDate.DayOfWeek == 0 ? 8 : (int)currentDate.DayOfWeek;

        for (int i = dayNumber; i >= 1; i--)
        {
            var dayOfWeek = currentDate.AddDays(-i + 1);
            chartDTOs.Add(new ChartDTO
            {
                Label = dayOfWeek.DayOfWeek.ToString(),
                Value = _unitOfWork.GetRepository<IScheduleRepository>()
                                    .Get(x => x.CreatedAt.Year == dayOfWeek.Year
                                        && x.CreatedAt.Month == dayOfWeek.Month
                                        && x.CreatedAt.Day == dayOfWeek.Day).Count()
            });
        }
        return chartDTOs;
    }

    private List<ChartDTO> CountInterviewByMonth()
    {
        var chartDTOs = new List<ChartDTO>();
        var currentDate = DateTime.Now;
        var dayNumber = currentDate.Day;

        for (int i = dayNumber; i >= 1; i--)
        {
            var day = currentDate.AddDays(-i + 1);
            chartDTOs.Add(new ChartDTO
            {
                Label = day.Day.ToString(),
                Value = _unitOfWork.GetRepository<IScheduleRepository>()
                                    .Get(x => x.CreatedAt.Year == day.Year
                                        && x.CreatedAt.Month == day.Month
                                        && x.CreatedAt.Day == day.Day).Count()
            });
        }
        return chartDTOs;
    }

    private List<ChartDTO> CountInterviewByYear()
    {
        var chartDTOs = new List<ChartDTO>();
        var currentDate = DateTime.Now;
        var monthNumber = currentDate.Month;

        for (int i = monthNumber; i >= 1; i--)
        {
            var month = currentDate.AddMonths(-i + 1);
            chartDTOs.Add(new ChartDTO
            {
                Label = month.Month.ToString(),
                Value = _unitOfWork.GetRepository<IScheduleRepository>()
                                    .Get(x => x.CreatedAt.Year == month.Year
                                        && x.CreatedAt.Month == month.Month).Count()
            });
        }
        return chartDTOs;
    }

    public List<ChartDTO> SuccessCandidate(int dateType)
    {
        DateTypeEnum dateTypeEnum = (DateTypeEnum)dateType;
        switch (dateTypeEnum)
        {
            case DateTypeEnum.Week:
                return SuccessCandidateByWeek();
            case DateTypeEnum.Month:
                return SuccessCandidateByMonth();
            case DateTypeEnum.Year:
                return SuccessCandidateByYear();
            default:
                throw new UserException(["Your request isn't exist"]);
        }
    }

    private List<ChartDTO> SuccessCandidateByYear()
    {
        var chartDTOs = new List<ChartDTO>();
        var currentDate = DateTime.Now;
        var monthNumber = currentDate.Month;
        var startYear = currentDate.AddMonths(-monthNumber + 1);

        chartDTOs.Add(new ChartDTO
        {
            Label = "Hired Candidate",
            Value = _unitOfWork.GetRepository<IScheduleRepository>()
                                    .Get(x => x.CreatedAt >= startYear && x.CreatedAt <= currentDate && x.Status == (int)ScheduleStatusEnum.Interviewed && x.Result == true).Count()
        });
        chartDTOs.Add(new ChartDTO
        {
            Label = "Failed Candidate",
            Value = _unitOfWork.GetRepository<IScheduleRepository>()
                                    .Get(x => x.CreatedAt >= startYear && x.CreatedAt <= currentDate && x.Status == (int)ScheduleStatusEnum.Interviewed && x.Result == false).Count()
        });

        return chartDTOs;
    }

    private List<ChartDTO> SuccessCandidateByMonth()
    {
        var chartDTOs = new List<ChartDTO>();
        var currentDate = DateTime.Now;
        var dayNumber = currentDate.Day;
        var startWeek = currentDate.AddDays(-dayNumber + 1);

        chartDTOs.Add(new ChartDTO
        {
            Label = "Hired Candidate",
            Value = _unitOfWork.GetRepository<IScheduleRepository>()
                                    .Get(x => x.CreatedAt >= startWeek && x.CreatedAt <= currentDate && x.Status == (int)ScheduleStatusEnum.Interviewed && x.Result == true).Count()
        });
        chartDTOs.Add(new ChartDTO
        {
            Label = "Failed Candidate",
            Value = _unitOfWork.GetRepository<IScheduleRepository>()
                                    .Get(x => x.CreatedAt >= startWeek && x.CreatedAt <= currentDate && x.Status == (int)ScheduleStatusEnum.Interviewed && x.Result == false).Count()
        });

        return chartDTOs;
    }

    private List<ChartDTO> SuccessCandidateByWeek()
    {
        var chartDTOs = new List<ChartDTO>();
        var currentDate = DateTime.Now;
        var dayNumber = currentDate.DayOfWeek == 0 ? 8 : (int)currentDate.DayOfWeek;
        var startWeek = currentDate.AddDays(-dayNumber + 1);

        chartDTOs.Add(new ChartDTO
        {
            Label = "Hired Candidate",
            Value = _unitOfWork.GetRepository<IScheduleRepository>()
                                    .Get(x => x.CreatedAt >= startWeek && x.CreatedAt <= currentDate && x.Status == (int)ScheduleStatusEnum.Interviewed && x.Result == true).Count()
        });
        chartDTOs.Add(new ChartDTO
        {
            Label = "Failed Candidate",
            Value = _unitOfWork.GetRepository<IScheduleRepository>()
                                    .Get(x => x.CreatedAt >= startWeek && x.CreatedAt <= currentDate && x.Status == (int)ScheduleStatusEnum.Interviewed && x.Result == false).Count()
        });

        return chartDTOs;
    }
}
