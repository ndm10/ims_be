﻿using AutoMapper;
using IMS.Data;
using Microsoft.EntityFrameworkCore;
namespace IMS.Business
{
    public class BaseServices<TEntity, TEntityDTO, TEntityCreateDTO, TEntityUpdateDTO> : IBaseService<TEntity, TEntityDTO, TEntityCreateDTO, TEntityUpdateDTO>
        where TEntity : BaseEntity
    {
        protected readonly UnitOfWork _unitOfWork;
        protected readonly IBaseRepository<TEntity> _baseRepository;
        protected readonly IMapper _mapper;

        public BaseServices(UnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _baseRepository = _unitOfWork.GetRepository<IBaseRepository<TEntity>>();
            _mapper = mapper;
        }
        protected virtual void ValidateBusinessLogicCreate(TEntity entity)
        {
            return;
        }
        protected virtual TEntity EditDataBeforeExcuteCreate(TEntity entity)
        {
            entity.UpdatedAt = entity.CreatedAt;
            entity.UpdatedBy = entity.UpdatedBy;
            return entity;
        }

        protected virtual void ValidateBusinessLogicCreate(IEnumerable<TEntity> entities)
        {
            return;
        }
        protected virtual IEnumerable<TEntity> EditDataBeforeExcuteCreate(IEnumerable<TEntity> entities)
        {
            return entities;
        }

        protected virtual Task ValidateBusinessLogicCreateAsync(TEntity entity)
        {
            return Task.CompletedTask;
        }
        protected virtual Task<TEntity> EditDataBeforeExcuteCreateAsync(TEntity entity)
        {
            return Task.FromResult(entity);
        }

        protected virtual Task ValidateBusinessLogicCreateAsync(IEnumerable<TEntity> entities)
        {
            return Task.CompletedTask;
        }
        protected virtual Task<IEnumerable<TEntity>> EditDataBeforeExcuteCreateAsync(IEnumerable<TEntity> entities)
        {
            return Task.FromResult(entities);
        }

        protected virtual void ValidateBusinessLogicUpdate(TEntity entity)
        {
            return;
        }
        protected virtual TEntity EditDataBeforeExcuteUpdate(TEntity entity)
        {
            return entity;
        }

        protected virtual void ValidateBusinessLogicUpdate(IEnumerable<TEntity> entities)
        {
            return;
        }
        protected virtual IEnumerable<TEntity> EditDataBeforeExcuteUpdate(IEnumerable<TEntity> entities)
        {
            return entities;
        }

        protected virtual Task ValidateBusinessLogicUpdateAsync(TEntity entity)
        {
            return Task.CompletedTask;
        }
        protected virtual Task<TEntity> EditDataBeforeExcuteUpdateAsync(TEntity entity)
        {
            return Task.FromResult(entity);
        }

        protected virtual Task ValidateBusinessLogicUpdateAsync(IEnumerable<TEntity> entities)
        {
            return Task.CompletedTask;
        }
        protected virtual Task<IEnumerable<TEntity>> EditDataBeforeExcuteUpdateAsync(IEnumerable<TEntity> entities)
        {
            return Task.FromResult(entities);
        }

        public virtual TEntityDTO Add(TEntityCreateDTO entityCreate)
        {
            if (entityCreate == null)
            {
                throw new UserException(["Thực thể khởi tạo bị bỏ trống"]);
            }

            // map entity create to entity
            var entity = _mapper.Map<TEntity>(entityCreate);

            // validate business logic
            ValidateBusinessLogicCreate(entity);

            // edit before add
            var entityNeedCreate = EditDataBeforeExcuteCreate(entity);

            _baseRepository.Add(entityNeedCreate);
            _unitOfWork.SaveChanges();
            return _mapper.Map<TEntityDTO>(entity);
        }

        public virtual async Task<TEntityDTO> AddAsync(TEntityDTO entityCreate)
        {
            if (entityCreate == null)
            {
                throw new UserException(["Thực thể khởi tạo bị bỏ trống"]);
            }

            // map entity create to entity
            var entity = _mapper.Map<TEntity>(entityCreate);

            // validate business logic
            await ValidateBusinessLogicCreateAsync(entity);

            // edit before add
            var entityNeedCreate = await EditDataBeforeExcuteCreateAsync(entity);

            _baseRepository.Add(entityNeedCreate);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TEntityDTO>(entity);
        }

        public int AddRange(IEnumerable<TEntityCreateDTO> entityCreateList)
        {
            if (entityCreateList == null || !entityCreateList.Any())
            {
                throw new UserException(["Danh sác thực thể khởi tạo bị bỏ trống"]);
            }

            // map entity create to entity
            var entities = new List<TEntity>();

            foreach (var entityCreate in entityCreateList)
            {
                entities.Add(_mapper.Map<TEntity>(entityCreate));

            }

            // validate business logic
            ValidateBusinessLogicCreate(entities);

            // edit before add
            var entityNeedCreate = EditDataBeforeExcuteCreate(entities);

            // save in DB
            _baseRepository.Add(entityNeedCreate);
            _unitOfWork.SaveChanges();

            // map entity to entityDTO
            var entityCreatedList = new List<TEntityDTO>();

            foreach (var item in entityCreatedList)
            {
                entityCreatedList.Add(_mapper.Map<TEntityDTO>(item));
            }

            return entities.ToList().Count;
        }

        public async Task<int> AddRangeAsync(IEnumerable<TEntityCreateDTO> entityCreateList)
        {
            if (entityCreateList == null || !entityCreateList.Any())
            {
                throw new UserException(["Danh sác thực thể khởi tạo bị bỏ trống"]);
            }

            // map entity create to entity
            var entities = new List<TEntity>();

            foreach (var entityCreate in entityCreateList)
            {
                entities.Add(_mapper.Map<TEntity>(entityCreate));

            }

            // validate business logic
            await ValidateBusinessLogicCreateAsync(entities);

            // edit before add
            var entityNeedCreate = await EditDataBeforeExcuteCreateAsync(entities);

            // save in DB
            _baseRepository.Add(entityNeedCreate);

            await _unitOfWork.SaveChangesAsync();

            // map entity to entityDTO
            var entityCreatedList = new List<TEntityDTO>();

            foreach (var item in entityCreatedList)
            {
                entityCreatedList.Add(_mapper.Map<TEntityDTO>(item));
            }

            return entities.ToList().Count;
        }

        public virtual bool Update(TEntityUpdateDTO entityUpdate)
        {
            if (entityUpdate == null)
            {
                throw new UserException(["Thực thể cập nhật bị bỏ trống"]);
            }

            // map entity update to entity
            var entity = _mapper.Map<TEntity>(entityUpdate);

            // validate business logic
            ValidateBusinessLogicUpdate(entity);

            // edit before update
            var entityNeedUpdate = EditDataBeforeExcuteUpdate(entity);

            _baseRepository.Update(entityNeedUpdate);
            _unitOfWork.SaveChanges();

            return CheckTracking(EntityState.Modified);
        }

        public virtual async Task<bool> UpdateAsync(TEntityUpdateDTO entityUpdate)
        {
            if (entityUpdate == null)
            {
                throw new UserException(["Thực thể cập nhật bị bỏ trống"]);
            }

            // map entity update to entity
            var entity = _mapper.Map<TEntity>(entityUpdate);

            // validate business logic
            await ValidateBusinessLogicUpdateAsync(entity);

            // edit before update
            var entityNeedUpdate = await EditDataBeforeExcuteUpdateAsync(entity);

            _baseRepository.Update(entityNeedUpdate);
            await _unitOfWork.SaveChangesAsync();

            return CheckTracking(EntityState.Modified);
        }

        public virtual bool Delete(Guid id)
        {
            var entity = _baseRepository.GetById(id) ??
                throw new UserException(["Thực thể không tồn tại trong hệ thống"]);

            _baseRepository.Delete(entity);

            _unitOfWork.SaveChanges();

            return entity.IsDeleted;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var entity = _baseRepository.GetById(id) ??
                throw new UserException(["Thực thể không tồn tại trong hệ thống"]);

            _baseRepository.Delete(entity);
            await _unitOfWork.SaveChangesAsync();

            return CheckTracking(EntityState.Deleted);
        }

        public virtual TEntityDTO? GetById(Guid id, string includeProperties = "")
        {
            return _mapper.Map<TEntityDTO>(_baseRepository.GetById(id, includeProperties));
        }

        public virtual async Task<TEntityDTO?> GetByIdAsync(Guid id)
        {
            var entity = await _baseRepository.GetByIdAsync(id);
            return _mapper.Map<TEntityDTO>(entity);
        }

        public IEnumerable<TEntityDTO> GetAll(bool isIncludeDeleted = false)
        {
            var entities = new List<TEntity>();
            if (isIncludeDeleted == false)
            {
                entities = _baseRepository.GetQuery(x => x.IsDeleted == isIncludeDeleted).ToList();
            }
            else entities = _baseRepository.DbSet.ToList();

            var entityDTOs = new List<TEntityDTO>();
            foreach (var item in entities)
            {
                entityDTOs.Add(_mapper.Map<TEntityDTO>(item));
            }

            return entityDTOs;
        }

        public async Task<IEnumerable<TEntityDTO>> GetAllAsync(bool isIncludeDeleted = false)
        {
            var entities = new List<TEntity>();
            if (isIncludeDeleted == false)
            {
                entities = await _baseRepository.GetQuery(x => x.IsDeleted == isIncludeDeleted).ToListAsync();
            }
            else entities = await _baseRepository.DbSet.ToListAsync();

            var entityDTOs = new List<TEntityDTO>();
            foreach (var item in entityDTOs)
            {
                entityDTOs.Add(_mapper.Map<TEntityDTO>(item));
            }

            return entityDTOs;
        }

        /// <summary>
        /// Lấy các thực thể theo filter, order by, paging
        /// </summary>
        /// <param name="filter">x=>x.Name.Contains("abc")</param>
        /// <param name="orderBy">q => q.OrderByDescending(c => c.Name);</param>
        /// <param name="includeProperties">"Products", "Authors, Category, Publisher"</param>
        /// <param name="isGetDeleted"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        //public virtual Paginated<TEntityDTO> Filter(Expression<Func<TEntityDTO, bool>>? filter = null,
        //    Func<IQueryable<TEntityDTO>, IOrderedQueryable<TEntityDTO>>? orderBy = null,
        //    string includeProperties = "", bool isGetDeleted = false, int pageIndex = 1, int pageSize = 10)
        //{
        //    Expression<Func<TEntity, bool>>? entityFilter = null;
        //    if (filter != null)
        //    {
        //        entityFilter = ExpressionExtention.Convert<TEntityDTO, TEntity>(filter);
        //    }

        //    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? entityOrderBy = null;
        //    if (orderBy != null)
        //    {
        //        entityOrderBy = query =>
        //        {
        //            // Lấy dữ liệu từ database
        //            var dtoQuery = query.Select(e => _mapper.Map<TEntityDTO>(e)).AsQueryable();

        //            // Thực hiện sắp xếp trên DTO
        //            var orderedDtoQuery = orderBy(dtoQuery);

        //            // Chuyển đổi lại từ DTO sang Entity
        //            var entityQuery = orderedDtoQuery.Select(dto => _mapper.Map<TEntity>(dto)).ToList();

        //            // Trả về IQueryable<TEntity>
        //            return entityQuery.AsQueryable().Order();
        //        };
        //    }

        //    var query = _baseRepository.GetByPage(filter: entityFilter, orderBy: entityOrderBy,
        //        includeProperties: includeProperties, isGetDeleted: isGetDeleted, page: pageIndex, size: pageSize);

        //    // Chuyển đổi từ Entity sang DTO
        //    var dtoQuery = query.ToList().Select(e => _mapper.Map<TEntityDTO>(e));

        //    return new Paginated<TEntityDTO>().Create(dtoQuery, pageIndex, pageSize);
        //}

        /// <summary>
        /// kiểm tra sự thay đổi của các thực thể theo trạng thái
        /// </summary>
        /// <param name="state">Trạng thái của entity. ví dụ: EntityState.Deleted</param>
        /// <returns></returns>
        public bool CheckTracking(EntityState state)
        {
            var changedEntities = _unitOfWork.DbContext.ChangeTracker.Entries()
            .Where(e => e.State == state);

            return changedEntities.ToList().Count > 0;
        }


    }
}
