﻿using AutoMapper;
using IMS.Business.DTO.ApplicationUser;
using IMS.Data;
using IMS.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Business.Service.Implement
{
    public class ApplicationUserService : IApplicationUserService
    {
        protected readonly UnitOfWork _unitOfWork;
        protected readonly IApplicationUserRepository _applicationUserRepository;
        protected readonly IMapper _mapper;

        public ApplicationUserService(UnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _applicationUserRepository = _unitOfWork.GetRepository<IApplicationUserRepository>();
            _mapper = mapper;
        }
        public ApplicationUserDTO? GetById(Guid id, string includeProperties = "")
        {
            return _mapper.Map<ApplicationUserDTO>(_applicationUserRepository.GetById(id, includeProperties));
        }
    }
}
