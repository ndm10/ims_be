﻿using AutoMapper;
using IMS.Business.Service.Interface;
using IMS.Data;

namespace IMS.Business.Service.Implement
{
    public class PositionService : BaseServices<Position, PositionDTO, PositionDTO, PositionDTO>, IPositionService
    {
        public PositionService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }
    }
}
