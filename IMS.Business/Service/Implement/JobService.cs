﻿using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using IMS.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System.Linq.Expressions;
using System.Security.Claims;
namespace IMS.Business
{
    public class JobService : BaseServices<Job, JobDTO, JobCreateDTO, JobUpdateDTO>, IJobService
    {
        private IAttributeValueRepository _attributeValueRepository;
        private IConfiguration _configuration;
        private readonly IValidator<JobImportExcel> _validator;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private string _userId = string.Empty;
        public JobService(
            UnitOfWork unitOfWork,
            IMapper mapper,
            IConfiguration configuration,
            IValidator<JobImportExcel> validator,
            IHttpContextAccessor httpContextAccessor
            ) : base(unitOfWork, mapper)
        {
            _attributeValueRepository = _unitOfWork.GetRepository<IAttributeValueRepository>();
            _configuration = configuration;
            _validator = validator;
            _httpContextAccessor = httpContextAccessor;
            _userId = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value ?? string.Empty;
        }
        protected override Job EditDataBeforeExcuteCreate(Job entity)
        {
            entity.Id = Guid.NewGuid();
            entity.Status = (int)JobStatusEnum.Draft;
            entity.CreatedAt = DateTimeOffset.UtcNow;
            entity.CreatedBy = Guid.Parse(_userId);
            return entity;
        }
        protected override Job EditDataBeforeExcuteUpdate(Job entity)
        {
            entity.UpdatedAt = DateTimeOffset.UtcNow;
            entity.UpdatedBy = Guid.Parse(_userId);
            return entity;
        }
        //public override Task<JobDTO?> GetByIdAsync(Guid id)
        //{
        //    return base.GetByIdAsync(id);
        //}

        public override JobDTO Add(JobCreateDTO entityCreate)
        {
            if (entityCreate == null)
            {
                throw new UserException(["Entity can not be null"]);
            }

            var errors = new List<string>();

            var skillValue = _attributeValueRepository.GetNonExistentIds(entityCreate.Skills);

            if (skillValue.Count > 0)
            {
                foreach (var item in skillValue)
                {
                    errors.Add($"{item} is not valid");
                }
            }

            var levelValue = _attributeValueRepository.GetNonExistentIds(entityCreate.Levels);

            if (levelValue.Count > 0)
            {
                foreach (var item in levelValue)
                {
                    errors.Add($"{item} is not valid");
                }
            }

            var benefitValue = _attributeValueRepository.GetNonExistentIds(entityCreate.Benefits);

            if (benefitValue.Count > 0)
            {
                foreach (var item in benefitValue)
                {
                    errors.Add($"{item} is not valid");
                }
            }

            if (errors.Count > 0) throw new UserException(errors);

            // map entity create to entity
            var entity = _mapper.Map<Job>(entityCreate);

            // edit before add
            var entityNeedCreate = EditDataBeforeExcuteCreate(entity);

            var attributeValues = _unitOfWork.GetRepository<IAttributeValueRepository>()
           .GetQuery(x => ((entityCreate.Skills != null && entityCreate.Skills.Contains(x.Id))
                       || (entityCreate.Levels != null && entityCreate.Levels.Contains(x.Id))
                       || (entityCreate.Benefits != null && entityCreate.Benefits.Contains(x.Id))));
            entityNeedCreate.Attrs = attributeValues.ToList();

            _baseRepository.Add(entityNeedCreate);
            _unitOfWork.SaveChanges();

            //var jobDto = _mapper.Map<JobDTO>(entityNeedCreate);

            return _mapper.Map<JobDTO>(entityNeedCreate);
        }

        public override async Task<bool> UpdateAsync(JobUpdateDTO entityUpdate)
        {
            //throw new UserException(["Thực thể khởi tạo bị bỏ trống"]);
            if (entityUpdate == null)
            {
                throw new UserException(["Entity can not be null"]);
            }

            var entityUpdateExited = await _baseRepository.GetByIdAsync(entityUpdate.Id) ??
                throw new UserException(["Entity is not exist in system"]);


            var errors = new List<string>();

            var skillValue = _attributeValueRepository.GetNonExistentIds(entityUpdate.Skills);

            if (skillValue.Count > 0)
            {
                foreach (var item in skillValue)
                {
                    errors.Add($"{item} is not valid");
                }
            }

            var levelValue = _attributeValueRepository.GetNonExistentIds(entityUpdate.Levels);

            if (levelValue.Count > 0)
            {
                foreach (var item in levelValue)
                {
                    errors.Add($"{item} is not valid");
                }
            }

            var benefitValue = _attributeValueRepository.GetNonExistentIds(entityUpdate.Benefits);

            if (benefitValue.Count > 0)
            {
                foreach (var item in benefitValue)
                {
                    errors.Add($"{item} is not valid");
                }
            }

            if (errors.Count > 0) throw new UserException(errors);



            var attributeValues = _unitOfWork.GetRepository<IAttributeValueRepository>()
           .GetQuery(x => ((entityUpdate.Skills != null && entityUpdate.Skills.Contains(x.Id))
                       || (entityUpdate.Levels != null && entityUpdate.Levels.Contains(x.Id))
                       || (entityUpdate.Benefits != null && entityUpdate.Benefits.Contains(x.Id))));


            // edit before update
            _mapper.Map(entityUpdate, entityUpdateExited);
            //entityUpdateExited.Id = entityUpdate.Id;
            //entityUpdateExited.Title = entityUpdate.Title;
            //entityUpdateExited.Address = entityUpdate.Address;
            //entityUpdateExited.Description = entityUpdate.Description;
            //entityUpdateExited.SalaryMax = entityUpdate.SalaryMax;
            //entityUpdateExited.SalaryMin = entityUpdate.SalaryMin;
            //entityUpdateExited.StartDate = entityUpdate.StartDate;
            //entityUpdateExited.EndDate = entityUpdate.EndDate;
            //entityUpdateExited.Status = (int)entityUpdate.Status;
            //entityUpdateExited.Attrs = attributeValues.ToList();

            var entityNeedUpdate = EditDataBeforeExcuteUpdate(entityUpdateExited);

            _baseRepository.Update(entityNeedUpdate);

            return _unitOfWork.SaveChanges() > 0;
        }

        public Paginated<JobDTO> FilterJob(string? searchKey, JobStatusEnum? jobStatus, int pageIndex = 1, int pageSize = 10)
        {

            Expression<Func<Job, bool>> filter = f =>
            (searchKey == null || f.Title.Contains(searchKey)) &&
            (jobStatus == null || f.Status == (int)jobStatus);

            Func<IQueryable<Job>, IOrderedQueryable<Job>> order =
                q => q.OrderByDescending(j => j.UpdatedAt).ThenBy(j => j.CreatedAt);

            var jobs = _unitOfWork.GetRepository<IJobRepository>().Get(filter, order, "Attrs");

            var jobsPaginated = Paginated<Job>.Create(jobs, pageIndex, pageSize);

            var jobDTOs = _mapper.Map<List<JobDTO>>(jobsPaginated.Items);

            return new Paginated<JobDTO>
            {
                PageIndex = jobsPaginated.PageIndex,
                TotalPages = jobsPaginated.TotalPages,
                PageSize = jobsPaginated.PageSize,
                TotalRecord = jobsPaginated.TotalRecord,
                Items = jobDTOs
            };
        }

        public ExcelPackage ExportExcel(string? searchKey, JobStatusEnum? jobStatus)
        {
            Expression<Func<Job, bool>> filter = f =>
            (searchKey == null || f.Title.Contains(searchKey)) ||
            (jobStatus == null || f.Status == (int)jobStatus);

            Func<IQueryable<Job>, IOrderedQueryable<Job>> order =
                q => q.OrderByDescending(j => j.UpdatedAt).ThenBy(j => j.CreatedAt);

            var jobs = _unitOfWork.GetRepository<IJobRepository>().Get(filter, order, includeProperties: "Attrs");
            var jobDTOs = _mapper.Map<List<JobDTO>>(jobs);

            string currentProjectPath = Directory.GetCurrentDirectory();

            // Đường dẫn tương đối
            string relativePath = _configuration["TemplateExcelPath:Job"] ?? throw new Exception("Không tìm thấy template excel.");

            // Ghép đường dẫn tương đối vào đường dẫn project
            string templateFilePath = currentProjectPath + relativePath;

            FileInfo file = new(templateFilePath);

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            ExcelPackage excelPackage = new(file);

            ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Job"];

            int rowNumber = 2;
            int index = 1;
            foreach (var item in jobDTOs)
            {
                //STT
                worksheet.Cells[rowNumber, 1].Value = index;
                index++;
                // Job Title
                worksheet.Cells[rowNumber, 2].Value = item.Title;

                // Required skills
                List<string> skills = item.AttributeDTOs == null ? [] : item.AttributeDTOs.Where(a => a.Type == AttributeTypeEnum.Skill).Select(a => a.Value).ToList();
                worksheet.Cells[rowNumber, 3].Value = string.Join(", ", skills);

                // Start date
                worksheet.Cells[rowNumber, 4].Value = item.StartDate.ToString();

                // End date
                worksheet.Cells[rowNumber, 5].Value = item.EndDate.ToString();

                // Salary From
                worksheet.Cells[rowNumber, 6].Value = item.SalaryMin;

                // Salary To
                worksheet.Cells[rowNumber, 7].Value = item.SalaryMax;

                // Benefits
                List<string> benefits = item.AttributeDTOs == null ? [] : item.AttributeDTOs.Where(a => a.Type == AttributeTypeEnum.Benefit).Select(a => a.Value).ToList();
                worksheet.Cells[rowNumber, 8].Value = string.Join(", ", benefits);

                // Working Address
                worksheet.Cells[rowNumber, 9].Value = item.Address;

                // Benefits
                List<string> levels = item.AttributeDTOs == null ? [] : item.AttributeDTOs.Where(a => a.Type == AttributeTypeEnum.Level).Select(a => a.Value).ToList();
                worksheet.Cells[rowNumber, 10].Value = string.Join(", ", levels);

                // Description
                worksheet.Cells[rowNumber, 11].Value = item.Description;

                rowNumber++;
            }

            return excelPackage;
        }

        public async Task<ExcelValidatedForm> ImportExcel(IFormFile excelFile)
        {
            if (excelFile is null || excelFile.Length <= 0)
            {
                throw new UserException(["File is null"]);
            }

            string[] allowedExtensions = [".xlsx", ".xls"];
            var fileExtension = Path.GetExtension(excelFile.FileName).ToLower();

            // kiểm tra xem file có phải là file excel không
            if (!allowedExtensions.Contains(fileExtension))
            {
                throw new UserException(["File is not excel file"]);
            }

            // kiểm tra có đúng template không
            string currentProjectPath = Directory.GetCurrentDirectory();

            // Đường dẫn tương đối
            string relativePath = _configuration["TemplateExcelPath:Job"] ?? throw new Exception("Template excel does not found.");

            // Ghép đường dẫn tương đối vào đường dẫn project
            string templateFilePath = currentProjectPath + relativePath;

            var uploadedHeaders = GetHeaders(excelFile.OpenReadStream());
            var templateHeaders = GetHeaders(new FileStream(templateFilePath, FileMode.Open, FileAccess.Read));

            if (!uploadedHeaders.SequenceEqual(templateHeaders))
            {
                throw new UserException(["File is not template."]);
            }

            // Kiểm tra xem có file có lớn hơn 500MB không
            const long MAX_FILE_SIZE = 500 * 1024 * 1024;

            if (excelFile.Length > MAX_FILE_SIZE)
            {
                throw new UserException(["File is not over size 500MB."]);
            }

            // validate rows
            var excelValidatedFile = await ValidateExcelAsync(excelFile);

            if (excelValidatedFile.IsValid)
            {
                var jobCreateDTOs = new List<JobCreateDTO>();
                // insert to DB
                foreach (var item in excelValidatedFile.JobImportExcels)
                {
                    _ = DateTime.TryParse(item.StartDate, out DateTime startDate);
                    _ = DateTime.TryParse(item.EndDate, out DateTime endDate);
                    _ = double.TryParse(item.SalaryMin, out double salaryMin);
                    _ = double.TryParse(item.SalaryMax, out double salaryMax);

                    var skills = _attributeValueRepository.Get(x => item.Skills.Contains(x.Value));
                    var benefits = _attributeValueRepository.Get(x => item.Benefits.Contains(x.Value));
                    var levels = _attributeValueRepository.Get(x => item.Levels.Contains(x.Value));

                    jobCreateDTOs.Add(new JobCreateDTO()
                    {
                        Title = item.Title,
                        StartDate = startDate,
                        EndDate = endDate,
                        SalaryMax = salaryMax,
                        SalaryMin = salaryMin,
                        Skills = [.. skills.Select(s => s.Id)],
                        Benefits = [.. benefits.Select(s => s.Id)],
                        Levels = [.. levels.Select(s => s.Id)],
                        Address = item.Address,
                        Description = item.Description
                    });
                }
                // thêm vào DB
                var jobCreatedDTO = AddRangeForImportExcel(jobCreateDTOs);

                // Đường dẫn tương đối
                string relativeExcelValidatedPath = _configuration["ExcelValidatedPath:Job"];
                string excelValidatedPath = currentProjectPath + relativeExcelValidatedPath;
                // Kiểm tra xem thư mục có tồn tại hay không
                if (Directory.Exists(excelValidatedPath))
                {
                    // Lấy tất cả các tệp trong thư mục có tên bắt đầu bằng userId
                    string[] files = Directory.GetFiles(excelValidatedPath, _userId + "*");

                    // Xóa từng tệp
                    foreach (string file in files)
                    {
                        File.Delete(file);
                    }
                }

                return new ExcelValidatedForm()
                {
                    IsValid = true,
                    NumOfErrorRow = 0,
                    NumOfValidRow = jobCreatedDTO.Count,
                };
            }
            else
            {
                return excelValidatedFile;
            }
        }

        public async Task<ExcelValidatedForm> ValidateExcelAsync(IFormFile excelFile)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            var errors = new List<string>();
            long invalidRow = 0;
            long validRow = 0;
            string excelValidatedFileName;
            List<JobImportExcel> jobImportExcels = [];
            try
            {
                using var stream = new MemoryStream();
                await excelFile.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    var worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;

                    for (int row = 2; row <= rowCount; row++) // Giả sử hàng đầu tiên là header
                    {
                        if (!string.IsNullOrEmpty(worksheet.Cells[row, 1].Text))
                        {
                            var excelRowData = new JobImportExcel
                            {
                                Title = worksheet.Cells[row, 2].Text,
                                Skills = worksheet.Cells[row, 3].Text,
                                StartDate = worksheet.Cells[row, 4].Text,
                                EndDate = worksheet.Cells[row, 5].Text,
                                SalaryMin = worksheet.Cells[row, 6].Text,
                                SalaryMax = worksheet.Cells[row, 7].Text,
                                Benefits = worksheet.Cells[row, 8].Text,
                                Address = worksheet.Cells[row, 9].Text,
                                Levels = worksheet.Cells[row, 10].Text,
                                Description = worksheet.Cells[row, 11].Text,
                            };
                            jobImportExcels.Add(excelRowData);
                            ValidationResult result = _validator.Validate(excelRowData);

                            if (!result.IsValid)
                            {
                                foreach (var failure in result.Errors)
                                {
                                    errors.Add(failure.ErrorMessage);
                                }
                                worksheet.Cells[row, 12].Value = $"Row {row}:\n" + string.Join('\n', errors);
                                excelRowData.IsValid = false;
                                invalidRow++;
                            }
                            else
                            {
                                excelRowData.IsValid = true;
                                validRow++;
                            }

                            // reset danh sách lỗi
                            errors.Clear();
                        }
                        else continue;

                    }

                    // cố định cột lỗi
                    if (invalidRow > 0)
                    {
                        // Cố định dòng tiêu đề và cột cuối cùng
                        worksheet.View.FreezePanes(2, 1);
                    }

                    // Lưu lại file Excel đã được chỉnh sửa
                    string currentProjectPath = Directory.GetCurrentDirectory();

                    // Đường dẫn tương đối
                    string relativePath = _configuration["ExcelValidatedPath:Job"] ?? throw new Exception("File excel validated is not found.");

                    // Ghép đường dẫn tương đối vào đường dẫn project

                    excelValidatedFileName = $"{_userId}-{Guid.NewGuid()}.xlsx";

                    var filePath = currentProjectPath + relativePath + excelValidatedFileName;

                    string directoryPath = currentProjectPath + relativePath;

                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }

                    var fileBytes = package.GetAsByteArray();
                    await File.WriteAllBytesAsync(filePath, fileBytes);
                }
                var excelValidatedFile = new ExcelValidatedForm()
                {
                    IsValid = invalidRow == 0,
                    NumOfErrorRow = invalidRow,
                    NumOfValidRow = validRow,
                    JobImportExcels = jobImportExcels,
                    ExcelDetailFilePath = excelValidatedFileName
                };
                return excelValidatedFile;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ExcelPackage GetExcelTemplate()
        {
            // Đường dẫn tới tệp Excel template
            var relatedPath = _configuration["TemplateExcelPath:Job"] ?? throw new Exception("Template excel is not found.");

            string currentProjectPath = Directory.GetCurrentDirectory();

            var filePath = currentProjectPath + relatedPath;
            // Kiểm tra xem tệp có tồn tại không
            if (!System.IO.File.Exists(filePath)) throw new Exception("Template excel is not found.");

            FileInfo file = new(filePath);

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            ExcelPackage excelPackage = new(file);

            return excelPackage;
        }

        public ExcelPackage GetExcelValidated(string excelFileName)
        {
            string currentProjectPath = Directory.GetCurrentDirectory();

            // Đường dẫn tương đối
            string relativePath = _configuration["ExcelValidatedPath:Job"] ?? throw new Exception("File excel validated does not found.");


            var filePath = currentProjectPath + relativePath + excelFileName;

            // Kiểm tra xem tệp có tồn tại không
            if (!System.IO.File.Exists(filePath)) throw new UserException([$"File {excelFileName} does not found."]);

            FileInfo file = new(filePath);

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            ExcelPackage excelPackage = new(file);

            return excelPackage;
        }

        private string[] GetHeaders(Stream fileStream)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage(fileStream))
            {
                var worksheet = package.Workbook.Worksheets[0];
                int headerRowNumber = 1; // Assuming the header is in the first row
                var headerRow = worksheet.Cells[headerRowNumber, 1, headerRowNumber, worksheet.Dimension.End.Column];
                return headerRow.Select(cell => cell.Text).ToArray();
            }
        }

        private List<JobDTO> AddRangeForImportExcel(List<JobCreateDTO> jobCreateDTOs)
        {
            var jobs = new List<Job>();
            foreach (var entityCreate in jobCreateDTOs)
            {
                // map entity create to entity
                var entity = _mapper.Map<Job>(entityCreate);

                // edit before add
                var entityNeedCreate = EditDataBeforeExcuteCreate(entity);

                var attributeValues = _unitOfWork.GetRepository<IAttributeValueRepository>()
               .GetQuery(x => ((entityCreate.Skills != null && entityCreate.Skills.Contains(x.Id))
                           || (entityCreate.Levels != null && entityCreate.Levels.Contains(x.Id))
                           || (entityCreate.Benefits != null && entityCreate.Benefits.Contains(x.Id))));
                entityNeedCreate.Attrs = attributeValues.ToList();
                jobs.Add(entityNeedCreate);
                _baseRepository.Add(entityNeedCreate);
            }

            _unitOfWork.SaveChanges();

            return _mapper.Map<List<JobDTO>>(jobs);
        }
    }
}
