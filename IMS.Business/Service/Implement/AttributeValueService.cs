﻿using AutoMapper;
using IMS.Data;

namespace IMS.Business;

public class AttributeValueService : BaseServices<AttributeValue, AttributeDTO, AttributeDTO, AttributeDTO>, IAttributeValueService
{
    public AttributeValueService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    {
    }

    public IEnumerable<AttributeDTO> GetByType(AttributeTypeEnum attributeTypeEnum)
    {
        var attributeValues = _unitOfWork.GetRepository<IAttributeValueRepository>().GetQuery(x => attributeTypeEnum == 0 || x.Type == (int)attributeTypeEnum).ToList();
        return _mapper.Map<IEnumerable<AttributeDTO>>(attributeValues);
    }
}
