﻿
using System.ComponentModel.DataAnnotations;

namespace IMS.Business.Common
{
    public class FutureDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is DateTime dateTimeValue)
            {
                if (dateTimeValue > DateTime.Now)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
