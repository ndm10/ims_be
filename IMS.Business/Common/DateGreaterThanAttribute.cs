﻿using System.ComponentModel.DataAnnotations;

namespace IMS.Business
{
    public class DateGreaterThanAttribute : ValidationAttribute
    {
        private readonly string _dateToCompareToFieldName;

        public DateGreaterThanAttribute(string dateToCompareToFieldName)
        {
            _dateToCompareToFieldName = dateToCompareToFieldName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var earlierDate = (DateTime?)validationContext.ObjectType.GetProperty(_dateToCompareToFieldName).GetValue(validationContext.ObjectInstance, null);
            var laterDate = (DateTime?)value;

            if (laterDate == null || earlierDate == null)
            {
                return ValidationResult.Success;
            }

            if (laterDate > earlierDate)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(ErrorMessage);
            }
        }
    }
}
