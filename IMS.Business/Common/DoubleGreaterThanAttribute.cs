﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Business
{
    public class DoubleGreaterThanAttribute :ValidationAttribute
    {
        private readonly string _doubleToCompareToFieldName;
        public DoubleGreaterThanAttribute(string doubleToCompareToFieldName)
        {
            _doubleToCompareToFieldName = doubleToCompareToFieldName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var minDouble = (double?)validationContext.ObjectType.GetProperty(_doubleToCompareToFieldName).GetValue(validationContext.ObjectInstance, null);
            var maxDouble = (double?)value;

            if (maxDouble == null || minDouble == null)
            {
                return ValidationResult.Success;
            }

            if (maxDouble > minDouble)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(ErrorMessage);
            }
        }
    }
}
