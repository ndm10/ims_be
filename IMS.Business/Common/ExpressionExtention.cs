﻿using System.Linq.Expressions;

namespace IMS.Business
{
    public static class ExpressionExtention
    {
        public static Expression<Func<TDestination, bool>> Convert<TSource, TDestination>(Expression<Func<TSource, bool>> sourceExpression)
        {
            var parameter = Expression.Parameter(typeof(TDestination), sourceExpression.Parameters[0].Name);
            var body = new Visitor<TSource, TDestination>(parameter).Visit(sourceExpression.Body);
            return Expression.Lambda<Func<TDestination, bool>>(body, parameter);
        }

        private class Visitor<TSource, TDestination> : ExpressionVisitor
        {
            private readonly ParameterExpression _parameter;

            public Visitor(ParameterExpression parameter)
            {
                _parameter = parameter;
            }

            protected override Expression VisitMember(MemberExpression node)
            {
                if (node.Expression != null && node.Expression.NodeType == ExpressionType.Parameter)
                {
                    var property = typeof(TDestination).GetProperty(node.Member.Name);
                    if (property != null)
                    {
                        return Expression.Property(_parameter, property);
                    }
                }
                return base.VisitMember(node);
            }

            protected override Expression VisitUnary(UnaryExpression node)
            {
                if (node.NodeType == ExpressionType.Convert && node.Operand != null && node.Operand.NodeType == ExpressionType.MemberAccess)
                {
                    var operand = (MemberExpression)node.Operand;
                    if (operand.Expression != null && operand.Expression.NodeType == ExpressionType.Parameter)
                    {
                        var property = typeof(TDestination).GetProperty(operand.Member.Name);
                        if (property != null)
                        {
                            var convertedProperty = Expression.Property(_parameter, property);
                            var conversion = Expression.Convert(convertedProperty, node.Type);
                            return conversion;
                        }
                    }
                }
                return base.VisitUnary(node);
            }
        }
    }
}
