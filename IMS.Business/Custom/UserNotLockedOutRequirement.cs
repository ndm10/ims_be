﻿using Microsoft.AspNetCore.Authorization;

public class UserNotLockedOutRequirement : IAuthorizationRequirement
{
    // Custom requirement logic can be added here
}