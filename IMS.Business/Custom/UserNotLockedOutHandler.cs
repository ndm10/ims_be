﻿using IMS.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Business.Custom
{
    public class UserNotLockedOutHandler : AuthorizationHandler<UserNotLockedOutRequirement>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserNotLockedOutHandler(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
        protected override async Task<Task> HandleRequirementAsync(AuthorizationHandlerContext context, UserNotLockedOutRequirement requirement)
        {
            var email = context.User.FindFirst(ClaimTypes.Email).Value;
            ApplicationUser user = await _userManager.FindByEmailAsync(email);
            var isLockedOut = await _userManager.IsLockedOutAsync(user);

            if (isLockedOut)
            {
                return Task.CompletedTask;
            }

            context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}
