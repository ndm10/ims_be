﻿using IMS.Business.Service.Implement;
using IMS.Business.Service.Interface;
using IMS.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace IMS.Business.Custom
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public sealed class CheckBanUserAttribute : Attribute, IAuthorizationFilter
    {
        private readonly IApplicationUserService _applicationUserService;
        public CheckBanUserAttribute(IApplicationUserService applicationUserService)
        {
            _applicationUserService = applicationUserService;
        }

        public async void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context != null)
            {
                var userId = context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                if (userId is not null)
                {
                    var user = _applicationUserService.GetById(Guid.Parse(userId.ToString()));
                    if (user.LockoutEnd > DateTime.UtcNow)
                    {
                        context.Result = new JsonResult(new { message = "User is banned" })
                        {
                            StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status401Unauthorized
                        };
                    }
                }
            }
        }
    }
}
