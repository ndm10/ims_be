﻿using System.ComponentModel.DataAnnotations;

namespace IMS.Business
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CompareDatesAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public CompareDatesAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            var currentValue = (DateTime?)value;

            var comparisonProperty = validationContext.ObjectType.GetProperty(_comparisonProperty) ?? throw new ArgumentException($"Không tìm thấy thuộc tính {_comparisonProperty}");

            var comparisonValue = (DateTime?)comparisonProperty.GetValue(validationContext.ObjectInstance);

            if (currentValue.HasValue && comparisonValue.HasValue && currentValue.Value <= comparisonValue.Value)
            {
                return new ValidationResult(ErrorMessage ?? $"{validationContext.DisplayName} phải lớn hơn {_comparisonProperty}.");
            }
            else
            {
                return ValidationResult.Success;
            }

        }
    }
}