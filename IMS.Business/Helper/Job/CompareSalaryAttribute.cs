﻿using System.ComponentModel.DataAnnotations;

namespace IMS.Business
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    internal class CompareSalaryAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public CompareSalaryAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            var currentValue = (double?)value;

            var comparisonProperty = validationContext.ObjectType.GetProperty(_comparisonProperty) ?? throw new ArgumentException($"Không tìm thấy thuộc tính {_comparisonProperty}");

            var comparisonValue = (double?)comparisonProperty.GetValue(validationContext.ObjectInstance);

            if (currentValue.HasValue && comparisonValue.HasValue && currentValue.Value <= comparisonValue.Value)
            {
                return new ValidationResult(ErrorMessage ?? $"{validationContext.DisplayName} phải lớn hơn {_comparisonProperty}.");
            }
            else
            {
                return ValidationResult.Success;
            }

        }
    }
}
