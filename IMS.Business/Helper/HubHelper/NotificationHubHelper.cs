﻿using IMS.Business.ImsHub;
using Microsoft.AspNetCore.SignalR;

namespace IMS.Business.Helper.HubHelper
{
    public class NotificationHubHelper(IHubContext<NotificationHub> hubContext)
    {
        /// <summary>
        /// Method to send message to a specific user
        /// </summary>
        /// <param name="methodName">Name of the envent that client listen to</param>
        /// <param name="userId">ID of current user login to the system</param>
        /// <param name="message">Content of the message send to user</param>
        /// <returns>Task</returns>
        public async Task SendMessageToUser(string methodName, Guid userId, string message)
        {
            await hubContext.Clients.User(userId.ToString()).SendAsync(methodName, message);
        }

        /// <summary>
        /// Method to send message to a group of users
        /// </summary>
        /// <param name="methodName">Name of the envent that client listen to</param>
        /// <param name="groupName">Name of group user</param>
        /// <param name="message">Content of the message send to user</param>
        /// <returns></returns>
        public async Task SendMessageToGroup(string methodName, string groupName, string message)
        {
            await hubContext.Clients.Group(groupName).SendAsync(methodName, message);
        }
    }
}
