﻿using FluentValidation;
using IMS.Data;
using System.Data;
using System.Reflection;

namespace IMS.Business
{
    public class JobImportValidator : AbstractValidator<JobImportExcel>
    {
        private static List<string?> attributeValues = [];
        private readonly IAttributeValueRepository _attributeValueRepository;

        public JobImportValidator(IAttributeValueRepository attributeValueRepository)
        {
            _attributeValueRepository = attributeValueRepository;

            RuleLevelCascadeMode = CascadeMode.Stop;

            var stringProperties = typeof(JobImportExcel).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                              .Where(prop => prop.PropertyType == typeof(string));

            foreach (var prop in stringProperties)
            {
                RuleFor(model => (string?)prop.GetValue(model)).Cascade(CascadeMode.Stop)
                    .NotEmpty().WithMessage($"{prop.Name} không được để trống.");
            }

            RuleFor(j => j.StartDate)
                .Must(BeAValidDate).WithMessage("Start date không đúng định dạng.");

            RuleFor(j => j.EndDate)
                .Must(BeAValidDate).WithMessage("End date không đúng định dạng.");

            RuleFor(x => x)
                .Must(HaveEndDateGreaterThanStartDate)
                .WithMessage("End Date phải lớn hơn Start Date.");

            RuleFor(j => j.SalaryMin)
                .Must(IsGreaterThanZero).WithMessage("Salary from không được âm.");

            RuleFor(j => j.SalaryMax)
                .Must(IsGreaterThanZero)
                .WithMessage("Salary to không được âm.");

            RuleFor(x => x)
                .Must(HaveSalaryToGreaterThanSalaryFrom)
                .WithMessage("Salary to phải lớn hơn Salary from.");

            RuleFor(x => x.Skills)
                .Must(HasAttributeValue)
                .When(x => !string.IsNullOrEmpty(x.Skills))
                .WithMessage(model => $"Các skill không hợp lệ: {AttributeError(model.Skills)}");

            RuleFor(x => x.Benefits)
                .Must(HasAttributeValue)
                .When(x => !string.IsNullOrEmpty(x.Benefits))
                .WithMessage(model => $"Các benefit không hợp lệ: {AttributeError(model.Benefits)}");

            RuleFor(x => x.Levels)
                .Must(HasAttributeValue)
                .When(x => !string.IsNullOrEmpty(x.Levels))
                .WithMessage(model => $"Các level không hợp lệ: {AttributeError(model.Levels)}");

        }

        private bool BeAValidDate(string date)
        {
            DateTime dateTime;
            return DateTime.TryParse(date, out dateTime);
        }

        private bool HaveEndDateGreaterThanStartDate(JobImportExcel model)
        {
            if (DateTime.TryParse(model.StartDate, out DateTime startDate) &&
                DateTime.TryParse(model.EndDate, out DateTime endDate))
            {
                return endDate > startDate;
            }
            return false;
        }

        private bool HasAttributeValue(string attributeNames)
        {
            if (attributeValues.Count == 0)
            {
                attributeValues = _attributeValueRepository.DbSet.Select(a => a.Value).ToList();
            }

            var attributeNameList = attributeNames.Split(",").ToList();

            return attributeNameList.Any(a => attributeValues.Contains(a));
        }

        private string AttributeError(string names)
        {
            var attributeNameList = names.Split(",").ToList();
            string attrError = string.Join(',', attributeNameList.Except(attributeValues));
            return attrError;
        }

        private bool IsGreaterThanZero(string num)
        {
            double number;
            if (double.TryParse(num, out number))
            {
                return number > 0;
            }
            return false;
        }

        private bool HaveSalaryToGreaterThanSalaryFrom(JobImportExcel model)
        {
            if (double.TryParse(model.SalaryMax, out double salaryMax) &&
                double.TryParse(model.SalaryMin, out double salaryMin))
            {
                return salaryMax > salaryMin;
            }
            return false;
        }
    }
}
