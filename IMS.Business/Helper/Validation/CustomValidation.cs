﻿using System.ComponentModel.DataAnnotations;

namespace IMS.Business.Helper.CustomValidation
{
    #region PastDate Attribute
    public class FutureDateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is DateTime)
            {
                if ((DateTime)value >= DateTime.Now)
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult("Date must be in the future.");
                }
            }
            else if (value is DateOnly)
            {
                if ((DateOnly)value >= DateOnly.FromDateTime(DateTime.Now))
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult("Date must be in the future.");
                }
            }
            else
            {
                return new ValidationResult("Invalid date.");
            }
        }
    }
    #endregion

    #region TimeBefore Attribute
    public class TimeBeforeAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public TimeBeforeAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var currentValue = (TimeOnly)value;

            var property = validationContext.ObjectType.GetProperty(_comparisonProperty);

            if (property == null)
                throw new ArgumentException($"Property with name {_comparisonProperty} not found");

            var comparisonValue = (TimeOnly)property.GetValue(validationContext.ObjectInstance);

            if (currentValue < comparisonValue)
                return ValidationResult.Success;
            else
                return new ValidationResult(ErrorMessage);
        }
    }
    #endregion
}
