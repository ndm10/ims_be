﻿namespace IMS.Business;
public class PositionDTO
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }
}
