﻿namespace IMS.Business
{
    public class SearchUserRequest : PagingRequest
    {
        public string? Keyword { get; set; } = String.Empty;
        public string? RoleName { get; set; } = String.Empty;
    }
}
