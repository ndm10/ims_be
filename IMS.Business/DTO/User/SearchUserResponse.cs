﻿namespace IMS.Business.DTO.User
{
    public class SearchUserResponse
    {
        public Paginated<UserDTO> Paginated { get; set; }
    }
}
