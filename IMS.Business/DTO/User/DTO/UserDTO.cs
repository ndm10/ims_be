﻿namespace IMS.Business
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string? Note { get; set; }
        public Guid DepartmentId { get; set; }
        public string? DepartmentName { get; set; }
        public string RoleName { get; set; }
        public string Status { get; set; }
        public string PhoneNumber { get; set; }
    }
}
