﻿namespace IMS.Business
{
    public class UserCreateDTO
    {
        public Guid? Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string? Address { get; set; }
        public bool Gender { get; set; } = true;
        public string? Note { get; set; }
        //use this value 069d9e2c-8141-4f29-b98f-9d9453a101ea to departmentId
        public Guid DepartmentId { get; set; } = Guid.Parse("069d9e2c-8141-4f29-b98f-9d9453a101ea");
        public string RoleName { get; set; } = "Admin";
        public bool Status { get; set; } = true;
        public string? PhoneNumber { get; set; }
    }
}
