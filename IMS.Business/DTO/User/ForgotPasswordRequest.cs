﻿using System.ComponentModel.DataAnnotations;

namespace IMS.Business
{
    public class ForgotPasswordRequest
    {
        [Required]
        public string Email { get; set; }
    }
}
