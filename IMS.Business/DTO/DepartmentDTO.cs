﻿namespace IMS.Business;
public class DepartmentDTO
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }
}