using IMS.Business.DTO.ApplicationUser;

namespace IMS.Business;

public class CandidateDTO
{
    public Guid Id { get; set; }

    public string FullName { get; set; } = string.Empty;

    public DateOnly Dob { get; set; }

    public string PhoneNumber { get; set; } = string.Empty;

    public string Email { get; set; } = string.Empty;

    public string Address { get; set; } = string.Empty;

    public sbyte Gender { get; set; }

    public byte[] Resume { get; set; } = [];

    public ApplicationUserDTO OwnerHR { get; set; } = new();

    public string? Note { get; set; }

    public int Status { get; set; }

    public List<AttributeDTO> Skills { get; set; } = new();

    public AttributeDTO HighestLevel { get; set; } = new();

    public PositionDTO Position { get; set; } = new();

    public sbyte? Yoe { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public Guid CreatedBy { get; set; }

    public DateTimeOffset UpdateAt { get; set; }

    public Guid UpdateBy { get; set; }
}
