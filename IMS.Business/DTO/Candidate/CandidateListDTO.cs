﻿namespace IMS.Business;

public class CandidateListDTO
{

    public Guid Id { get; set; }

    public string FullName { get; set; } = string.Empty;

    public DateOnly Dob { get; set; }

    public string PhoneNumber { get; set; } = string.Empty;

    public string Email { get; set; } = string.Empty;

    public string Address { get; set; } = string.Empty;

    public sbyte Gender { get; set; }

    public string? Note { get; set; }

    public int Status { get; set; }

    public List<AttributeDTO> Skills { get; set; } = new();

    public PositionDTO Position { get; set; } = new();
}
