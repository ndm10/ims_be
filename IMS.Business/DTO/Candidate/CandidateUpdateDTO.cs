﻿using IMS.Data;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace IMS.Business;

public class CandidateUpdateDTO
{
    [Required(ErrorMessage = "Id is required")]
    public Guid Id { get; set; }

    [Required(ErrorMessage = "FullName is required")]
    [MinLength(8, ErrorMessage = "Full name has at least 8 characters")]
    public string FullName { get; set; } = string.Empty;

    [Required(ErrorMessage = "Dob is required")]
    public DateOnly Dob { get; set; }

    [Required(ErrorMessage = "Phone Number is required.")]
    public string PhoneNumber { get; set; } = string.Empty;

    [Required(ErrorMessage = "Email is required.")]
    [EmailAddress(ErrorMessage = "Invalid Email Address.")]
    public string Email { get; set; } = string.Empty;

    [Required(ErrorMessage = "Address is required.")]
    public string Address { get; set; } = string.Empty;

    [Required(ErrorMessage = "Gender is required.")]
    public sbyte Gender { get; set; }

    public byte[] Resume { get; set; } = [];

    [Required(ErrorMessage = "Resume file is required.")]
    public IFormFile ResumeFile { get; set; }

    [Required(ErrorMessage = "Account is required.")]
    public Guid AccountId { get; set; }

    [Required(ErrorMessage = "Skills is required.")]
    public List<Guid> SkillsId { get; set; } = new List<Guid>();

    [Required(ErrorMessage = "Position is required.")]
    public Guid PositionId { get; set; }

    public string? Note { get; set; }

    public CandidateStatusEnum? Status { get; set; }

    public sbyte? Yoe { get; set; }

    [Required(ErrorMessage = "Highest level is required.")]
    public Guid HighestLevelId { get; set; }
}
