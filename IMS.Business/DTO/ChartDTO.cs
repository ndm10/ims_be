﻿namespace IMS.Business;

public class ChartDTO
{
    public string Label { get; set; } = string.Empty;

    public int Value { get; set; }
}
