﻿using IMS.Business.DTO;
using IMS.Business.DTO.Candidate;
using IMS.Business.DTO.Schedule;
using IMS.Data;
using IMS.Data.Utility.Enum;

namespace IMS.Business;
public class OfferDTO
{
    public Guid Id { get; set; }
    public Guid CandidateId { get; set; }

    public Guid RecruiterId { get; set; }

    public Guid? ManagerId { get; set; }

    public Guid ScheduleId { get; set; }

    public Guid PositionId { get; set; }

    public DateTime? ContractPeriodStart { get; set; }

    public DateTime? ContractPeriodEnd { get; set; }

    public ContractTypeEnum ContractType { get; set; }

    public Guid DepartmentId { get; set; }

    public DateOnly? DueDate { get; set; }

    public double BasicSalary { get; set; }

    public OfferStatusEnum Status { get; set; }

    public string? Note { get; set; }

    public CandidateOfferDTO Candidate { get; set; }

    public AppUserDTO? Recruiter { get; set; }

    public AppUserDTO? Approver { get; set; }

    public DepartmentDTO Department { get; set; }

    public PositionDTO Position { get; set; }

    public ScheduleDTO? Schedule { get; set; }

    public List<AttributeDTO> AttributeDTOs { get; set; }
}