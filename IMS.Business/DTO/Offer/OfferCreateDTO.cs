﻿using IMS.Business.Common;
using IMS.Data.Utility.Enum;
using System.ComponentModel.DataAnnotations;

namespace IMS.Business;

public class OfferCreateDTO
{
    [Required(ErrorMessage = "Candidate is required")]
    public Guid CandidateId { get; set; }

    [Required(ErrorMessage = "Recruiter is required")]
    public Guid RecruiterId { get; set; }

    [Required(ErrorMessage = "Manager is required")]
    public Guid ManagerId { get; set; }

    public Guid? ScheduleId { get; set; }

    [Required(ErrorMessage = "Position is required")]
    public Guid PositionId { get; set; }

    public DateTime? ContractPeriodStart { get; set; }

    [DateGreaterThan("ContractPeriodStart", ErrorMessage = "Contract period end must be greater than contract period start")]
    public DateTime? ContractPeriodEnd { get; set; }

    [Required(ErrorMessage = "Contract type is required")]
    [Range(0, 4, ErrorMessage = "Contract type must be 0 to 4")]
    public ContractTypeEnum ContractType { get; set; }

    [Required(ErrorMessage = "Department is required")]
    public Guid DepartmentId { get; set; }

    [Required(ErrorMessage = "Due date is required")]
    [FutureDate(ErrorMessage = "Date must be in the future.")]
    public DateTime DueDate { get; set; }

    [Required(ErrorMessage = "Basic salary is required")]
    [Range(0, double.MaxValue, ErrorMessage = "Basic salary must be a positive number")]
    public double BasicSalary { get; set; }

    public string? Note { get; set; }

    public List<Guid> LevelsId { get; set; } = new List<Guid>();

}