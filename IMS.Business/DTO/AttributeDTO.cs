using IMS.Data;

namespace IMS.Business;

public class AttributeDTO
{
    public Guid Id { get; set; }

    public AttributeTypeEnum Type { get; set; }

    public string Value { get; set; }
}