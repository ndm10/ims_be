﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Business.DTO.Authenticate
{
    public class ResetPasswordResponse
    {
        public bool IsSuccess { get; set; }
    }
}
