﻿using IMS.Business.Helper.CustomValidation;
using System.ComponentModel.DataAnnotations;

namespace IMS.Business.DTO.Schedule
{
    public class ScheduleCreateDTO
    {
        [Required(ErrorMessage = "Title is required")]
        [MinLength(8, ErrorMessage = "Title has at least 8 characters")]
        public string Title { get; set; } = null!;

        [Required(ErrorMessage = "Job is required")]
        public Guid JobId { get; set; }

        [Required(ErrorMessage = "Candidate is required")]
        public Guid CandidateId { get; set; }

        [Required(ErrorMessage = "Schedule Date is required")]
        [FutureDate]
        public DateOnly ScheduleDate { get; set; }

        [Required(ErrorMessage = "Start Time is required")]
        [TimeBefore("EndTime", ErrorMessage = "Start Time must be before End Time")]
        public TimeOnly StartTime { get; set; }

        [Required(ErrorMessage = "End Time is required")]
        public TimeOnly EndTime { get; set; }

        public string? Location { get; set; }

        [Required(ErrorMessage = "Recruiter Owner is required")]
        public Guid RecruiterOwnerId { get; set; }

        public string? Note { get; set; }
        public string? MeetingId { get; set; }

        [Required(ErrorMessage = "Interviewer is required")]
        public Guid[] InterviewerId { get; set; } = [];
    }
}
