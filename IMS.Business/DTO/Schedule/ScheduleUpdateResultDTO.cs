﻿using System.ComponentModel.DataAnnotations;

namespace IMS.Business.DTO.Schedule
{
    public class ScheduleUpdateResultDTO
    {
        [Required(ErrorMessage = "Please enter id of schedule")]
        public Guid Id { get; set; }

        public string? Note { get; set; }

        [Required(ErrorMessage = "Please choose result")]
        public bool Result { get; set; }
    }
}
