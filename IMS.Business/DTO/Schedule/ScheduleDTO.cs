﻿using IMS.Business.DTO.ApplicationUser;

namespace IMS.Business.DTO.Schedule
{
    public class ScheduleDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; } = null!;
        public Guid JobId { get; set; }
        public Guid CandidateId { get; set; }
        public DateOnly ScheduleDate { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }
        public string? Location { get; set; }
        public Guid RecruiterOwnerId { get; set; }
        public string? Note { get; set; }
        public string? MeetingId { get; set; }
        public bool? Result { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public Guid? UpdatedBy { get; set; }
        public JobDTO Job { get; set; } = null!;
        public CandidateDTO Candidate { get; set; } = null!;
        public ApplicationUserDTO RecruiterOwner { get; set; } = null!;
        public ICollection<ApplicationUserDTO> Interviewers { get; set; } = new List<ApplicationUserDTO>();
        public ApplicationUserDTO CreatedByUser { get; set; } = null!;
        public ApplicationUserDTO UpdatedByUser { get; set; } = null!;
    }
}
