﻿using System.ComponentModel.DataAnnotations;

namespace IMS.Business.DTO.Schedule
{
    public class ScheduleCancelDTO
    {
        [Required(ErrorMessage = "Please enter id of schedule")]
        public Guid Id { get; set; }
    }
}
