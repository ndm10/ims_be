﻿namespace IMS.Business
{
    public class ExcelValidatedForm
    {
        public bool IsValid { get; set; }

        public List<JobImportExcel> JobImportExcels { get; set; }

        public long NumOfErrorRow { get; set; }

        public long NumOfValidRow { get; set; }

        public string ExcelDetailFilePath { get; set; }
    }
}
