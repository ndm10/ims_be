﻿using IMS.Data;
using System.ComponentModel.DataAnnotations;

namespace IMS.Business
{
    public class JobUpdateDTO
    {
        [Required]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Title is require.")]
        public string Title { get; set; } = string.Empty;

        [Required(ErrorMessage = "Start date is require.")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "Start date is require.")]
        [CompareDates("StartDate", ErrorMessage = "End date must greater than Start date")]
        public DateTime EndDate { get; set; }

        public double SalaryMin { get; set; }

        [DoubleGreaterThan("SalaryMin", ErrorMessage = "SalaryMax must greater than SalaryMin")]
        public double SalaryMax { get; set; }

        [Required(ErrorMessage = "Skill is require")]
        public List<Guid> Skills { get; set; } = [];

        [Required(ErrorMessage = "Benefit is require")]
        public List<Guid> Benefits { get; set; } = [];

        [Required(ErrorMessage = "Level is require")]
        public List<Guid> Levels { get; set; } = [];

        public string? Address { get; set; } = string.Empty;

        public string? Description { get; set; }

        [Required(ErrorMessage = "Job status is require.")]
        public JobStatusEnum Status { get; set; }
    }
}
