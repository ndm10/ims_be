﻿namespace IMS.Business
{
    public class JobImportExcel
    {
        public string Title { get; set; } = string.Empty;

        public string StartDate { get; set; } = string.Empty;

        public string EndDate { get; set; } = string.Empty;

        public string SalaryMin { get; set; } = string.Empty;

        public string SalaryMax { get; set; } = string.Empty;

        public string Skills { get; set; } = string.Empty;

        public string Benefits { get; set; } = string.Empty;

        public string Levels { get; set; } = string.Empty;

        public string? Address { get; set; }

        public string? Description { get; set; }

        public bool IsValid { get; set; } = false;
    }
}
