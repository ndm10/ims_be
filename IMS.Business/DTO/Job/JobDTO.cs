﻿namespace IMS.Business
{
    public class JobDTO
    {
        public Guid Id { get; set; }

        public string Title { get; set; } = string.Empty;

        public List<AttributeDTO>? AttributeDTOs { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public double SalaryMin { get; set; }

        public double SalaryMax { get; set; }

        public string Address { get; set; } = string.Empty;

        public string? Description { get; set; }

        public int Status { get; set; }
    }
}
