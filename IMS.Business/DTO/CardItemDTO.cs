﻿namespace IMS.Business;

public class CardItemDTO
{
    public long Total { get; set; }

    public double Percentage { get; set; }
}
