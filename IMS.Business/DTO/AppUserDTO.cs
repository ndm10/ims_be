﻿
namespace IMS.Business.DTO
{
    public class AppUserDTO
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
    }
}
