﻿namespace IMS.Business.DTO.ApplicationUser
{
    public class ApplicationUserDTO
    {
        public Guid Id { get; set; }
        public string FullName { get; set; } = null!;
        public string UserName { get; set; } = null!;
        public DateTime? DateOfBirth { get; set; }
        public string? Address { get; set; }
        public bool Gender { get; set; }
        public string? Note { get; set; }
        public Guid DepartmentId { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
    }
}
