﻿using System.Text.Json;

namespace IMS.Business
{
    public class BaseException
    {
        public List<string> Errors { get; set; }
        public string MessageResponse { get; set; }
        public bool IsSuccess { get; set; }
        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
