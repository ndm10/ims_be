﻿namespace IMS.Business
{
    public class BaseSuccess<T> : BaseResponse
    {
        public T? Data { get; set; }

        public BaseSuccess()
        {
            IsSuccess = true;
            MessageResponse = "Successfully";
        }

        public BaseSuccess(bool isSuccess, string message)
        {
            IsSuccess = isSuccess;
            MessageResponse = message;
        }

        public BaseSuccess(bool isSuccess, string message, T data)
        {
            IsSuccess = isSuccess;
            MessageResponse = message;
            Data = data;
        }
    }
}
