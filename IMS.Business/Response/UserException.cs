﻿namespace IMS.Business
{
    public class UserException : Exception
    {
        /// <summary>
        /// Lỗi chi tiết
        /// </summary>
        public List<string> Errors { get; set; }

        public bool IsSuccess { get; set; }

        /// <summary>
        /// Thông báo chung về lỗi
        /// </summary>
        public string MessageResponse { get; set; }

        public UserException()
        {

        }

        public UserException(List<string> error, string messageResponse = "Thất bại", bool isSuccess = false)
        {
            Errors = error;
            MessageResponse = messageResponse;
            IsSuccess = isSuccess;
        }
    }
}
