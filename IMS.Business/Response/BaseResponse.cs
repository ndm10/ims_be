﻿namespace IMS.Business
{
    public class BaseResponse
    {
        public bool IsSuccess { get; set; }
        public string MessageResponse { get; set; }
    }
}
