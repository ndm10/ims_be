﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMS.Data.Configurations
{
    public class JobConfig : IEntityTypeConfiguration<Job>
    {
        public void Configure(EntityTypeBuilder<Job> builder)
        {
            builder.HasKey(e => e.Id).HasName("PRIMARY");

            builder.ToTable("Job");

            builder.Property(e => e.Description).HasColumnType("text");
            builder.Property(e => e.EndDate).HasColumnType("datetime");
            builder.Property(e => e.StartDate).HasColumnType("datetime");
            builder.Property(e => e.Title)
                .HasMaxLength(255)
                .UseCollation("utf8mb3_general_ci")
                .HasCharSet("utf8mb3");

            builder.HasMany(d => d.Attrs).WithMany(p => p.Jobs)
                .UsingEntity<Dictionary<string, object>>(
                    "JobAttribute",
                    r => r.HasOne<AttributeValue>().WithMany()
                        .HasForeignKey("AttributeId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("jobattr_ibfk_2"),
                    l => l.HasOne<Job>().WithMany()
                        .HasForeignKey("JobId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("jobattr_ibfk_1"),
                    j =>
                    {
                        j.HasKey("JobId", "AttributeId")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("JobAttribute");
                        j.HasIndex(new[] { "AttributeId" }, "AttributeId");
                    });
            // Relationship with the ApplicationUser who created the job
            builder.HasOne(j => j.CreatedByUser)
                  .WithMany(u => u.CreatedJobs)
                  .HasForeignKey(j => j.CreatedBy)
                  .IsRequired();

            // Relationship with the ApplicationUser who last updated the job
            builder.HasOne(j => j.UpdateByUser)
                  .WithMany(u => u.UpdatedJobs)
                  .HasForeignKey(j => j.UpdatedBy)
                  .IsRequired(false);
        }
    }
}
