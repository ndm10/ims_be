﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Data.Configurations
{
    public class OfferConfig : IEntityTypeConfiguration<Offer>
    {
        public void Configure(EntityTypeBuilder<Offer> builder)
        {
            builder.HasKey(e => e.Id).HasName("PRIMARY");

            builder.ToTable("Offer");

            builder.HasIndex(e => e.CandidateId, "CandidateId");

            builder.HasIndex(e => e.DepartmentId, "DepartmentId");

            builder.HasIndex(e => e.ScheduleId, "ScheduleId");

            builder.HasIndex(e => e.PositionId, "offer_ibfk_4_idx");

            builder.Property(e => e.ContractPeriodEnd).HasColumnType("datetime");

            builder.Property(e => e.ContractPeriodStart).HasColumnType("datetime");

            builder.Property(e => e.ManagerId).HasColumnName("ManagerId");

            builder.Property(e => e.RecruiterId).HasColumnName("RecruiterId");

            builder.Property(e => e.Note).HasColumnType("text");

            builder.HasOne(d => d.Candidate).WithMany(p => p.Offers)
                .HasForeignKey(d => d.CandidateId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("offer_ibfk_1");

            builder.HasOne(d => d.Department).WithMany(p => p.Offers)
                .HasForeignKey(d => d.DepartmentId)
                .HasConstraintName("offer_ibfk_3");

            builder.HasOne(d => d.Position).WithMany(p => p.Offers)
                .HasForeignKey(d => d.PositionId)
                .HasConstraintName("offer_ibfk_4");

            builder.HasOne(d => d.Schedule).WithMany(p => p.Offers)
                .HasForeignKey(d => d.ScheduleId)
                .HasConstraintName("offer_ibfk_2");
            builder.HasOne(d => d.Approver)
               .WithMany()
               .HasForeignKey(d => d.ManagerId)
               .HasConstraintName("FK_Offer_AspNetUsers_ManagerId");
            builder.HasOne(d => d.Recruiter)
               .WithMany()
               .HasForeignKey(d => d.RecruiterId)
               .HasConstraintName("FK_Offer_AspNetUsers_RecruiterId");
            builder.HasMany(d => d.Attrs).WithMany(p => p.Offers)
                .UsingEntity<Dictionary<string, object>>(
                    "OfferAttribute",
                    r => r.HasOne<AttributeValue>().WithMany()
                        .HasForeignKey("AttributeId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("offerattr_ibfk_2"),
                    l => l.HasOne<Offer>().WithMany()
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("offerattr_ibfk_1"),
                    j =>
                    {
                        j.HasKey("OfferId", "AttributeId")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("OfferAttribute");
                        j.HasIndex(new[] { "AttributeId" }, "AttributeId");
                    });
            builder.HasMany(d => d.Attrs).WithMany(p => p.Offers)
                .UsingEntity<Dictionary<string, object>>(
                    "OfferAttribute",
                    r => r.HasOne<AttributeValue>().WithMany()
                        .HasForeignKey("AttributeId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("offerattr_ibfk_2"),
                    l => l.HasOne<Offer>().WithMany()
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("offerattr_ibfk_1"),
                    j =>
                    {
                        j.HasKey("OfferId", "AttributeId")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("OfferAttribute");
                        j.HasIndex(new[] { "AttributeId" }, "AttributeId");
                    });
        }
    }
}
