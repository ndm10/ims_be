﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMS.Data.Configurations
{
    public class ScheduleConfig : IEntityTypeConfiguration<Schedule>
    {
        public void Configure(EntityTypeBuilder<Schedule> builder)
        {
            builder.HasKey(e => e.Id).HasName("PRIMARY");

            builder.ToTable("Schedule");

            builder.HasIndex(e => e.CandidateId, "CandidateId");

            builder.HasIndex(e => e.JobId, "JobId");

            builder.Property(e => e.Location)
                .HasMaxLength(255)
                .UseCollation("utf8mb3_general_ci")
                .HasCharSet("utf8mb3");
            builder.Property(e => e.MeetingId)
                .HasMaxLength(255)
                .UseCollation("utf8mb3_general_ci")
                .HasCharSet("utf8mb3");
            builder.Property(e => e.Note).HasColumnType("text");
            builder.Property(e => e.Title)
                .HasMaxLength(255)
                .UseCollation("utf8mb3_general_ci")
                .HasCharSet("utf8mb3");

            builder.HasOne(d => d.Candidate).WithMany(p => p.Schedules)
                .HasForeignKey(d => d.CandidateId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("schedule_ibfk_1");

            builder.HasOne(d => d.Job).WithMany(p => p.Schedules)
                .HasForeignKey(d => d.JobId)
                .HasConstraintName("schedule_ibfk_2");

            builder.HasOne(d => d.CreatedByUser).WithMany(p => p.CreatedSchedules)
                .HasForeignKey(d => d.CreatedBy)
                .HasConstraintName("schedule_ibfk_3");

            builder.HasOne(d => d.UpdatedByUser).WithMany(p => p.UpdatedSchedules)
                .HasForeignKey(d => d.UpdatedBy)
                .HasConstraintName("schedule_ibfk_4");

            builder.HasOne(d => d.RecruiterOwner).WithMany(p => p.RecuiterOwnerSchedules)
                .HasForeignKey(d => d.RecruiterOwnerId)
                .HasConstraintName("schedule_ibfk_5");

            builder.HasMany(d => d.Interviewers).WithMany(p => p.InterviewSchedules)
            .UsingEntity<Dictionary<string, object>>(
                    "Interviewer_Schedule",
                    r => r.HasOne<ApplicationUser>().WithMany()
                        .HasForeignKey("InterviewerId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("interviewer_schedule_ibfk_2"),
                    l => l.HasOne<Schedule>().WithMany()
                        .HasForeignKey("ScheduleId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("interviewer_schedule_ibfk_1"),
                    j =>
                    {
                        j.HasKey("InterviewerId", "ScheduleId")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("Interviewer_Schedule");
                        j.HasIndex(new[] { "InterviewerId" }, "InterviewerId");
                    });
        }
    }
}
