﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMS.Data.Configurations
{
    public class DepartmentConfig : IEntityTypeConfiguration<Department>
    {
        public void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.HasKey(e => e.Id).HasName("PRIMARY");

            builder.ToTable("Department");

            builder.Property(e => e.Description).HasColumnType("text");
            builder.Property(e => e.Name)
                .HasMaxLength(255)
                .UseCollation("utf8mb3_general_ci")
                .HasCharSet("utf8mb3");
            //Navigate Department has many users
            builder.HasMany(d => d.Users)
                  .WithOne(u => u.Department)
                  .HasForeignKey(u => u.DepartmentId)
                  .IsRequired();
        }
    }
}
