﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Data.Configurations
{
    public class ApplicationUserConfig : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.ToTable("AppUser");

            //Constraints for the FullName column hava max 255 character and can be Vietnamese
            builder.Property(e => e.FullName)
                .HasMaxLength(255)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            //Constraints for the Note column hava max 1080 character and can be Vietnamese
            builder.Property(e => e.Note)
                .HasMaxLength(1080)
                .UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            // Navigation property for jobs created by this user
            builder.HasMany(u => u.CreatedJobs)
                  .WithOne(j => j.CreatedByUser)
                  .HasForeignKey(j => j.CreatedBy)
                  .IsRequired();

            // Navigation property for jobs updated by this user
            builder.HasMany(u => u.UpdatedJobs)
                  .WithOne(j => j.UpdateByUser)
                  .HasForeignKey(j => j.UpdatedBy)
                  .IsRequired(false);

            // Navigation property for department what user is in
            builder.HasOne(u => u.Department)
                  .WithMany(d => d.Users)
                  .HasForeignKey(u => u.DepartmentId)
                  .IsRequired();

            // Navigation property for notifications sent to this user
            builder.HasMany(u => u.Notifications)
                  .WithOne(n => n.ToUser)
                  .HasForeignKey(n => n.ToUserId)
                  .IsRequired();
        }
    }
}
