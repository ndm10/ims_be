﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMS.Data
{
    public class CandidateConfig : IEntityTypeConfiguration<Candidate>
    {
        public void Configure(EntityTypeBuilder<Candidate> builder)
        {
            builder.HasKey(e => e.Id).HasName("PRIMARY");

            builder.ToTable("Candidate");

            builder.HasIndex(e => e.Email, "Email").IsUnique();

            builder.Property(e => e.Address)
                .HasMaxLength(100)
                .HasColumnName("Address")
                .UseCollation("utf8mb3_general_ci")
                .HasCharSet("utf8mb3")
                .HasDefaultValue(string.Empty);
            builder.Property(e => e.Dob).HasColumnName("DOB");
            builder.Property(e => e.Email)
                .HasMaxLength(50)
                .IsFixedLength()
                .HasColumnName("Email")
                .HasDefaultValue(string.Empty);
            builder.Property(e => e.FullName)
                .HasMaxLength(255)
                .HasColumnName("FullName")
                .UseCollation("utf8mb3_general_ci")
                .HasCharSet("utf8mb3")
                .HasDefaultValue(string.Empty);
            builder.Property(e => e.Gender)
                .HasColumnName("Gender");
            builder.Property(e => e.Note)
                .HasColumnType("text")
                .HasColumnName("Note");
            builder.Property(e => e.PhoneNumber)
                .HasMaxLength(20)
                .IsFixedLength()
                .HasColumnName("PhoneNumber")
                .HasDefaultValue(string.Empty);
            builder.Property(e => e.Resume)
                .HasColumnName("Resume");
            builder.Property(e => e.Status).HasColumnName("Status");
            builder.Property(e => e.Yoe).HasColumnName("YOE");

            builder.HasMany(d => d.Attrs).WithMany(p => p.Candidates)
                .UsingEntity<Dictionary<string, object>>(
                    "CandidateAttribute",
                    r => r.HasOne<AttributeValue>().WithMany()
                        .HasForeignKey("AttributeId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("candidateattr_ibfk_2"),
                    l => l.HasOne<Candidate>().WithMany()
                        .HasForeignKey("CandidateId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("candidateattr_ibfk_1"),
                    j =>
                    {
                        j.HasKey("CandidateId", "AttributeId")
                            .HasName("PRIMARY")
                            .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                        j.ToTable("CandidateAttribute");
                        j.HasIndex(new[] { "AttributeId" }, "AttributeId");
                    });
        }
    }
}
