﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMS.Data.Configurations
{
    public class PositionConfig : IEntityTypeConfiguration<Position>
    {
        public void Configure(EntityTypeBuilder<Position> builder)
        {
            builder.HasKey(e => e.Id).HasName("PRIMARY");

            builder.ToTable("Position");

            builder.Property(e => e.Id)
                .HasDefaultValueSql("'UUID'")
                .HasColumnName("Id");
            builder.Property(e => e.Description)
                .HasColumnType("text")
                .HasColumnName("Description");
            builder.Property(e => e.Name)
                .HasMaxLength(255)
                .HasColumnName("Name");
        }
    }
}
