﻿namespace IMS.Data
{
    public class AttributeValue : BaseEntity
    {
        public int Type { get; set; }

        public string? Value { get; set; }

        public virtual ICollection<Candidate> Candidates { get; set; } = new List<Candidate>();

        public virtual ICollection<Job> Jobs { get; set; } = new List<Job>();

        public virtual ICollection<Offer> Offers { get; set; } = new List<Offer>();
    }
}
