﻿
using IMS.Data.Utility.Enum;

namespace IMS.Data
{
    public class Offer : BaseEntity
    {
        public Guid CandidateId { get; set; }

        public Guid RecruiterId { get; set; }

        public Guid? ManagerId { get; set; }

        public Guid? ScheduleId { get; set; }

        public Guid PositionId { get; set; }

        public DateTime? ContractPeriodStart { get; set; }

        public DateTime? ContractPeriodEnd { get; set; }

        public ContractTypeEnum ContractType { get; set; }

        public Guid DepartmentId { get; set; }

        public DateOnly? DueDate { get; set; }

        public double BasicSalary { get; set; }

        public OfferStatusEnum Status { get; set; }

        public string? Note { get; set; }

        public Candidate? Candidate { get; set; }

        public ApplicationUser? Recruiter { get; set; }

        public ApplicationUser? Approver { get; set; }

        public Department? Department { get; set; }

        public Position? Position { get; set; }

        public Schedule? Schedule { get; set; }
        public ICollection<AttributeValue>? Attrs { get; set; } = new List<AttributeValue>();

    }
}
