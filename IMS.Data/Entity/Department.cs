﻿namespace IMS.Data
{
    public class Department : BaseEntity
    {
        public string? Name { get; set; }

        public string? Description { get; set; }

        public ICollection<Offer> Offers { get; set; } = new List<Offer>();

        public ICollection<ApplicationUser> Users { get; set; } = new List<ApplicationUser>();

    }
}
