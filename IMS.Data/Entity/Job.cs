﻿namespace IMS.Data
{
    public class Job : BaseEntity
    {
        public string Title { get; set; } = string.Empty;

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public double SalaryMin { get; set; }

        public double SalaryMax { get; set; }

        public string? Address { get; set; } = string.Empty;

        public string? Description { get; set; }

        public int Status { get; set; }

        public virtual ICollection<Schedule> Schedules { get; set; } = new List<Schedule>();

        public virtual ICollection<AttributeValue> Attrs { get; set; } = new List<AttributeValue>();

        public ApplicationUser CreatedByUser { get; set; }

        public ApplicationUser UpdateByUser { get; set; }
    }
}
