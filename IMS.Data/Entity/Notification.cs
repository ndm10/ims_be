﻿namespace IMS.Data.Entity
{
    public class Notification : BaseEntity
    {
        public string Title { get; set; } = null!;
        public string Content { get; set; } = null!;
        public Guid ToUserId { get; set; }
        public string UrlRedirect { get; set; } = null!;
        public bool IsRead { get; set; }
        public ApplicationUser ToUser { get; set; } = null!;
    }
}
