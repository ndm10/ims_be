﻿namespace IMS.Data
{
    public class Position : BaseEntity
    {

        public string? Name { get; set; }

        public string? Description { get; set; }

        public ICollection<Offer> Offers { get; set; } = new List<Offer>();

        public ICollection<Candidate> Candidates { get; set; } = new List<Candidate>();
    }
}
