﻿namespace IMS.Data
{
    public class Schedule : BaseEntity
    {
        public string Title { get; set; } = null!;
        public Guid JobId { get; set; }
        public Guid CandidateId { get; set; }
        public DateOnly ScheduleDate { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }
        public string? Location { get; set; }
        public Guid RecruiterOwnerId { get; set; }
        public string? Note { get; set; }
        public string? MeetingId { get; set; }
        public bool? Result { get; set; }
        public int? Status { get; set; }
        public virtual Job Job { get; set; } = null!;
        public virtual Candidate Candidate { get; set; } = null!;
        public virtual ApplicationUser RecruiterOwner { get; set; } = null!;
        public virtual ApplicationUser CreatedByUser { get; set; } = null!;
        public virtual ApplicationUser UpdatedByUser { get; set; } = null!;
        public virtual ICollection<Offer> Offers { get; set; } = new List<Offer>();
        public virtual ICollection<ApplicationUser> Interviewers { get; set; } = new List<ApplicationUser>();
    }
}
