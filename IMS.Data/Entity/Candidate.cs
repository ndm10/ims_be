﻿namespace IMS.Data
{
    public class Candidate : BaseEntity
    {
        public string FullName { get; set; } = string.Empty;

        public DateOnly Dob { get; set; }

        public string PhoneNumber { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public sbyte Gender { get; set; }

        public byte[] Resume { get; set; } = [];

        public ApplicationUser OwnerHR { get; set; } = new ApplicationUser();

        public string? Note { get; set; }

        public int Status { get; set; }

        public sbyte? Yoe { get; set; }

        public virtual Position Position { get; set; } = new Position();

        public virtual ICollection<Offer> Offers { get; set; } = new List<Offer>();

        public virtual ICollection<Schedule> Schedules { get; set; } = new List<Schedule>();

        public virtual ICollection<AttributeValue> Attrs { get; set; } = new List<AttributeValue>();

    }
}
