﻿using IMS.Data.Entity;
using Microsoft.AspNetCore.Identity;

namespace IMS.Data
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public string FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string? Address { get; set; }
        public bool Gender { get; set; }
        public string? Note { get; set; }

        //One to many relationship with Department
        public Guid DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public virtual ICollection<Job> CreatedJobs { get; set; } = new List<Job>();
        public virtual ICollection<Job> UpdatedJobs { get; set; } = new List<Job>();
        public virtual ICollection<Candidate> ManagementCandidates { get; set; } = new List<Candidate>();
        public virtual ICollection<Notification> Notifications { get; set; } = new List<Notification>();

        #region Schedule Relationships
        public virtual ICollection<Schedule> RecuiterOwnerSchedules { get; set; } = new List<Schedule>();
        public virtual ICollection<Schedule> CreatedSchedules { get; set; } = new List<Schedule>();
        public virtual ICollection<Schedule> UpdatedSchedules { get; set; } = new List<Schedule>();
        public virtual ICollection<Schedule> InterviewSchedules { get; set; } = new List<Schedule>();
        #endregion

        public string? RefreshToken { get; set; }

        public DateTime RefreshTokenExpirationDateTime { get; set; }

        public DateTimeOffset? CreatedAt { get; set; }

        public Guid? CreatedBy { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        public Guid? UpdatedBy { get; set; }
    }
}
