﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class updateschedule_entity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "schedule_ibfk_5",
                table: "Schedule");

            migrationBuilder.AlterColumn<Guid>(
                name: "RecruiterOwnerId",
                table: "Schedule",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci",
                oldClrType: typeof(Guid),
                oldType: "char(36)",
                oldNullable: true)
                .OldAnnotation("Relational:Collation", "ascii_general_ci");

            migrationBuilder.AddForeignKey(
                name: "schedule_ibfk_5",
                table: "Schedule",
                column: "RecruiterOwnerId",
                principalTable: "AppUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "schedule_ibfk_5",
                table: "Schedule");

            migrationBuilder.AlterColumn<Guid>(
                name: "RecruiterOwnerId",
                table: "Schedule",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci",
                oldClrType: typeof(Guid),
                oldType: "char(36)")
                .OldAnnotation("Relational:Collation", "ascii_general_ci");

            migrationBuilder.AddForeignKey(
                name: "schedule_ibfk_5",
                table: "Schedule",
                column: "RecruiterOwnerId",
                principalTable: "AppUser",
                principalColumn: "Id");
        }
    }
}
