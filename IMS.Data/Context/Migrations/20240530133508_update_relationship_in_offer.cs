﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class update_relationship_in_offer : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttributeValue_Offer_OfferId",
                table: "AttributeValue");

            migrationBuilder.DropIndex(
                name: "IX_AttributeValue_OfferId",
                table: "AttributeValue");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "AttributeValue");

            migrationBuilder.CreateTable(
                name: "OfferAttribute",
                columns: table => new
                {
                    OfferId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    AttributeId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.OfferId, x.AttributeId })
                        .Annotation("MySql:IndexPrefixLength", new[] { 0, 0 });
                    table.ForeignKey(
                        name: "offerattr_ibfk_1",
                        column: x => x.OfferId,
                        principalTable: "Offer",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "offerattr_ibfk_2",
                        column: x => x.AttributeId,
                        principalTable: "AttributeValue",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_0900_ai_ci");

            migrationBuilder.CreateIndex(
                name: "AttributeId2",
                table: "OfferAttribute",
                column: "AttributeId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OfferAttribute");

            migrationBuilder.AddColumn<Guid>(
                name: "OfferId",
                table: "AttributeValue",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_AttributeValue_OfferId",
                table: "AttributeValue",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttributeValue_Offer_OfferId",
                table: "AttributeValue",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id");
        }
    }
}
