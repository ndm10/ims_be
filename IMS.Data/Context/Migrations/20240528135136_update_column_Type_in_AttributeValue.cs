﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class update_column_Type_in_AttributeValue : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Offer",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "AttributeValue",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "enum('value1','value2')")
                .OldAnnotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("Relational:Collation", "utf8mb4_0900_ai_ci");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Offer");

            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "AttributeValue",
                type: "enum('value1','value2')",
                nullable: false,
                collation: "utf8mb4_0900_ai_ci",
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
