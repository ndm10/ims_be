﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class update_structure_schedule : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "RecruiterOwnerId",
                table: "Schedule",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_Schedule_RecruiterOwnerId",
                table: "Schedule",
                column: "RecruiterOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Schedule_UpdateBy",
                table: "Schedule",
                column: "UpdateBy");

            migrationBuilder.AddForeignKey(
                name: "schedule_ibfk_4",
                table: "Schedule",
                column: "UpdateBy",
                principalTable: "AppUser",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "schedule_ibfk_5",
                table: "Schedule",
                column: "RecruiterOwnerId",
                principalTable: "AppUser",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "schedule_ibfk_4",
                table: "Schedule");

            migrationBuilder.DropForeignKey(
                name: "schedule_ibfk_5",
                table: "Schedule");

            migrationBuilder.DropIndex(
                name: "IX_Schedule_RecruiterOwnerId",
                table: "Schedule");

            migrationBuilder.DropIndex(
                name: "IX_Schedule_UpdateBy",
                table: "Schedule");

            migrationBuilder.DropColumn(
                name: "RecruiterOwnerId",
                table: "Schedule");
        }
    }
}
