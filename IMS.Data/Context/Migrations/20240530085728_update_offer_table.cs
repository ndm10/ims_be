﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class update_offer_table : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ApproverId",
                table: "Offer",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<Guid>(
                name: "OfferId",
                table: "AttributeValue",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_ApproverId",
                table: "Offer",
                column: "ApproverId");

            migrationBuilder.CreateIndex(
                name: "IX_AttributeValue_OfferId",
                table: "AttributeValue",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttributeValue_Offer_OfferId",
                table: "AttributeValue",
                column: "OfferId",
                principalTable: "Offer",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AppUser_ApproverId",
                table: "Offer",
                column: "ApproverId",
                principalTable: "AppUser",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttributeValue_Offer_OfferId",
                table: "AttributeValue");

            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AppUser_ApproverId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_ApproverId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_AttributeValue_OfferId",
                table: "AttributeValue");

            migrationBuilder.DropColumn(
                name: "ApproverId",
                table: "Offer");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "AttributeValue");
        }
    }
}
