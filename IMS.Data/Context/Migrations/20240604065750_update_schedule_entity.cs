﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class update_schedule_entity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "schedule_ibfk_3",
                table: "Schedule");

            migrationBuilder.DropColumn(
                name: "RecruiterOwner",
                table: "Schedule");

            migrationBuilder.CreateIndex(
                name: "IX_Schedule_CreatedBy",
                table: "Schedule",
                column: "CreatedBy");

            migrationBuilder.AddForeignKey(
                name: "schedule_ibfk_3",
                table: "Schedule",
                column: "CreatedBy",
                principalTable: "AppUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "schedule_ibfk_3",
                table: "Schedule");

            migrationBuilder.DropIndex(
                name: "IX_Schedule_CreatedBy",
                table: "Schedule");

            migrationBuilder.AddColumn<Guid>(
                name: "RecruiterOwner",
                table: "Schedule",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.AddForeignKey(
                name: "schedule_ibfk_3",
                table: "Schedule",
                column: "JobId",
                principalTable: "AppUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
