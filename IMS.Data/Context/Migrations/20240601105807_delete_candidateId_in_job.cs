﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class delete_candidateId_in_job : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_Candidate_CandidateId",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Job_CandidateId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "CandidateId",
                table: "Job");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CandidateId",
                table: "Job",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_Job_CandidateId",
                table: "Job",
                column: "CandidateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_Candidate_CandidateId",
                table: "Job",
                column: "CandidateId",
                principalTable: "Candidate",
                principalColumn: "Id");
        }
    }
}
