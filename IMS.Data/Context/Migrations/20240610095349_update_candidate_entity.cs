﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class update_candidate_entity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AccountId",
                table: "Candidate",
                newName: "OwnerHRId");

            migrationBuilder.CreateIndex(
                name: "IX_Candidate_OwnerHRId",
                table: "Candidate",
                column: "OwnerHRId");

            migrationBuilder.AddForeignKey(
                name: "FK_Candidate_AppUser_OwnerHRId",
                table: "Candidate",
                column: "OwnerHRId",
                principalTable: "AppUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Candidate_AppUser_OwnerHRId",
                table: "Candidate");

            migrationBuilder.DropIndex(
                name: "IX_Candidate_OwnerHRId",
                table: "Candidate");

            migrationBuilder.RenameColumn(
                name: "OwnerHRId",
                table: "Candidate",
                newName: "AccountId");
        }
    }
}
