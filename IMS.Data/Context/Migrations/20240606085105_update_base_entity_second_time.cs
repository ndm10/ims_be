﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class update_base_entity_second_time : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_AppUser_UpdateBy",
                table: "Job");

            migrationBuilder.RenameColumn(
                name: "UpdateBy",
                table: "Schedule",
                newName: "UpdatedBy");

            migrationBuilder.RenameColumn(
                name: "UpdateAt",
                table: "Schedule",
                newName: "UpdatedAt");

            migrationBuilder.RenameIndex(
                name: "IX_Schedule_UpdateBy",
                table: "Schedule",
                newName: "IX_Schedule_UpdatedBy");

            migrationBuilder.RenameColumn(
                name: "UpdateBy",
                table: "Position",
                newName: "UpdatedBy");

            migrationBuilder.RenameColumn(
                name: "UpdateAt",
                table: "Position",
                newName: "UpdatedAt");

            migrationBuilder.RenameColumn(
                name: "UpdateBy",
                table: "Offer",
                newName: "UpdatedBy");

            migrationBuilder.RenameColumn(
                name: "UpdateAt",
                table: "Offer",
                newName: "UpdatedAt");

            migrationBuilder.RenameColumn(
                name: "UpdateBy",
                table: "Job",
                newName: "UpdatedBy");

            migrationBuilder.RenameColumn(
                name: "UpdateAt",
                table: "Job",
                newName: "UpdatedAt");

            migrationBuilder.RenameIndex(
                name: "IX_Job_UpdateBy",
                table: "Job",
                newName: "IX_Job_UpdatedBy");

            migrationBuilder.RenameColumn(
                name: "UpdateBy",
                table: "Department",
                newName: "UpdatedBy");

            migrationBuilder.RenameColumn(
                name: "UpdateAt",
                table: "Department",
                newName: "UpdatedAt");

            migrationBuilder.RenameColumn(
                name: "UpdateBy",
                table: "Candidate",
                newName: "UpdatedBy");

            migrationBuilder.RenameColumn(
                name: "UpdateAt",
                table: "Candidate",
                newName: "UpdatedAt");

            migrationBuilder.RenameColumn(
                name: "UpdateBy",
                table: "AttributeValue",
                newName: "UpdatedBy");

            migrationBuilder.RenameColumn(
                name: "UpdateAt",
                table: "AttributeValue",
                newName: "UpdatedAt");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_AppUser_UpdatedBy",
                table: "Job",
                column: "UpdatedBy",
                principalTable: "AppUser",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_AppUser_UpdatedBy",
                table: "Job");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy",
                table: "Schedule",
                newName: "UpdateBy");

            migrationBuilder.RenameColumn(
                name: "UpdatedAt",
                table: "Schedule",
                newName: "UpdateAt");

            migrationBuilder.RenameIndex(
                name: "IX_Schedule_UpdatedBy",
                table: "Schedule",
                newName: "IX_Schedule_UpdateBy");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy",
                table: "Position",
                newName: "UpdateBy");

            migrationBuilder.RenameColumn(
                name: "UpdatedAt",
                table: "Position",
                newName: "UpdateAt");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy",
                table: "Offer",
                newName: "UpdateBy");

            migrationBuilder.RenameColumn(
                name: "UpdatedAt",
                table: "Offer",
                newName: "UpdateAt");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy",
                table: "Job",
                newName: "UpdateBy");

            migrationBuilder.RenameColumn(
                name: "UpdatedAt",
                table: "Job",
                newName: "UpdateAt");

            migrationBuilder.RenameIndex(
                name: "IX_Job_UpdatedBy",
                table: "Job",
                newName: "IX_Job_UpdateBy");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy",
                table: "Department",
                newName: "UpdateBy");

            migrationBuilder.RenameColumn(
                name: "UpdatedAt",
                table: "Department",
                newName: "UpdateAt");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy",
                table: "Candidate",
                newName: "UpdateBy");

            migrationBuilder.RenameColumn(
                name: "UpdatedAt",
                table: "Candidate",
                newName: "UpdateAt");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy",
                table: "AttributeValue",
                newName: "UpdateBy");

            migrationBuilder.RenameColumn(
                name: "UpdatedAt",
                table: "AttributeValue",
                newName: "UpdateAt");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_AppUser_UpdateBy",
                table: "Job",
                column: "UpdateBy",
                principalTable: "AppUser",
                principalColumn: "Id");
        }
    }
}
