﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class fix_missing_field_in_offer_entity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Offer_RecruiterId",
                table: "Offer",
                column: "RecruiterId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offer_AspNetUsers_RecruiterId",
                table: "Offer",
                column: "RecruiterId",
                principalTable: "AppUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offer_AspNetUsers_RecruiterId",
                table: "Offer");

            migrationBuilder.DropIndex(
                name: "IX_Offer_RecruiterId",
                table: "Offer");
        }
    }
}
