﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IMS.Data.Context.Migrations
{
    /// <inheritdoc />
    public partial class delete_space_in_user_table : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: " Note",
                table: "Candidate",
                newName: "Note");

            migrationBuilder.RenameColumn(
                name: " FullName",
                table: "Candidate",
                newName: "FullName");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Note",
                table: "Candidate",
                newName: " Note");

            migrationBuilder.RenameColumn(
                name: "FullName",
                table: "Candidate",
                newName: " FullName");
        }
    }
}
