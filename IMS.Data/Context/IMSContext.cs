﻿using IMS.Data.Configurations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IMS.Data
{

    public partial class IMSContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        //private readonly string _connectionString;

        // public IMSContext()
        // {
        // }

        public IMSContext(DbContextOptions<IMSContext> options)
            : base(options)
        {
        }

        //public IMSContext(string? connectionString)
        //{
        //    this._connectionString = connectionString;
        //}

        ////implement onConcfiguring to get connection string from appsettings.json
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseMySql(_connectionString
        //            , Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.25-mysql"));
        //    }
        //}

        public virtual DbSet<AttributeValue> Attributevalues { get; set; }

        public virtual DbSet<Candidate> Candidates { get; set; }

        public virtual DbSet<Department> Departments { get; set; }

        public virtual DbSet<Job> Jobs { get; set; }

        public virtual DbSet<Offer> Offers { get; set; }

        public virtual DbSet<Position> Positions { get; set; }

        public virtual DbSet<Schedule> Schedules { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public DbSet<ApplicationRole> ApplicationRoles { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.UseCollation("utf8mb4_0900_ai_ci").HasCharSet("utf8mb4");
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationRoleConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationUserConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AttributeValueConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CandidateConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DepartmentConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IdentityRoleClaimConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IdentityUserClaimConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IdentityUserLoginConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IdentityUserRoleConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IdentityUserTokenConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(JobConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(OfferConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PositionConfig).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ScheduleConfig).Assembly);

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }

}
