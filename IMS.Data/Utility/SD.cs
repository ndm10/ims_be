﻿namespace IMS.Data
{
    public class SD
    {
        public const string Role_Admin = "Admin";
        public const string Role_HR = "Recruiter";
        public const string Role_Interviewer = "Interviewer";
        public const string Role_Manager = "Manager";
    }
}
