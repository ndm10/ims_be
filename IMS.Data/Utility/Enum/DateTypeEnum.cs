﻿namespace IMS.Data;

public enum DateTypeEnum
{
    Week = 0,
    Month = 1,
    Year = 2
}
