﻿namespace IMS.Data
{
    public enum OfferStatusEnum
    {
        WaitingForApproval = 0,
        Approved,
        Rejected,
        WaitingForResponse,
        AcceptedOffer,
        DesclinedOffer,
        Cancelled,
    }
}
