﻿namespace IMS.Data
{
    public enum JobStatusEnum
    {
        Draft, // when job is created
        Open, // when start date is due
        Close, // when end date is due
    }
}
