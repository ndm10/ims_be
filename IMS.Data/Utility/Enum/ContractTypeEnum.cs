﻿namespace IMS.Data.Utility.Enum
{
    public enum ContractTypeEnum
    {
        TrialTwoMonths = 0,
        TraineeThreeMonths,
        OneYear,
        ThreeYears,
        Unlimited,
    }
}
