﻿namespace IMS.Data;

public enum AttributeTypeEnum
{
    HighestLevel = 1,
    Level = 2,
    Skill = 3,
    Benefit = 4
}
