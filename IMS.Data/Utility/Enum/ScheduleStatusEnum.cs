﻿namespace IMS.Data.Utility.Enum
{
    public enum ScheduleStatusEnum
    {
        Open,
        Invited,
        Interviewed,
        Cancelled,
    }
}
