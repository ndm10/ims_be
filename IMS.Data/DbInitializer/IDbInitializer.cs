﻿namespace IMS.Data
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
