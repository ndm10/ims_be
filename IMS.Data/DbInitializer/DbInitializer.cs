﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IMS.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMSContext _context;
        public DbInitializer(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IMSContext context)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _context = context;
        }

        public async void Initialize()
        {
            //migrations if they are not applied
            try
            {
                if (_context.Database.GetPendingMigrations().Count() > 0)
                {
                    _context.Database.Migrate();
                }
            }
            catch (Exception ex) { }
            //check if not exist any department
            if (_context.Departments.Count() == 0)
            {
                //generate GUID for department
                Guid adminDepartmentId = Guid.NewGuid();
                //seed data of department
                _context.Departments.AddRange(new Department
                {
                    Id = adminDepartmentId,
                    Name = "Admin Department",
                    Description = "Administration Department"
                });


                if (!_roleManager.RoleExistsAsync(SD.Role_Interviewer).GetAwaiter().GetResult())
                {
                    _roleManager.CreateAsync(new ApplicationRole
                    {
                        Name = SD.Role_Admin
                    }).GetAwaiter().GetResult();
                    _roleManager.CreateAsync(new ApplicationRole
                    {
                        Name = SD.Role_HR
                    }).GetAwaiter().GetResult();
                    _roleManager.CreateAsync(new ApplicationRole
                    {
                        Name = SD.Role_Interviewer
                    }).GetAwaiter().GetResult();
                    _roleManager.CreateAsync(new ApplicationRole
                    {
                        Name = SD.Role_Manager
                    }).GetAwaiter().GetResult();

                    //if roles are not created, then we will create admin user as well
                    _userManager.CreateAsync(new ApplicationUser
                    {
                        UserName = "IMSAdmin",
                        Email = "imsadmin@gmail.com",
                        NormalizedUserName = "IMSAdmin",
                        NormalizedEmail = "IMSADMIN@GMAIL.COM",
                        PhoneNumber = "1112223333",
                        Address = "HaNoi",
                        FullName = "Admin IMS",
                        DepartmentId = adminDepartmentId
                    }, "Admin123@").GetAwaiter().GetResult();

                    ApplicationUser user = _context.ApplicationUsers.FirstOrDefault(u => u.Email == "imsadmin@gmail.com");
                    _userManager.AddToRoleAsync(user, SD.Role_Admin).GetAwaiter().GetResult();
                }
            }
            if (_context.Departments.Count() == 1)
            {
                //generate GUID for department
                Guid hrDepartmentId = Guid.NewGuid();
                Guid interviewerDepartment = Guid.NewGuid();
                Guid managerDepartment = Guid.NewGuid();

                //seed data of department
                _context.Departments.AddRange(new Department
                {
                    Id = hrDepartmentId,
                    Name = "HR Department",
                    Description = "Human Resources Department"
                },
                new Department
                {
                    Id = interviewerDepartment,
                    Name = "Interviewer Department",
                    Description = "Interviewer Department"
                },
                new Department
                {
                    Id = managerDepartment,
                    Name = "Manager Department",
                    Description = "Manager Department"
                });
                //if roles are not created, then we will create admin user as well
                _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "IMSHR",
                    Email = "imsHR@gmail.com",
                    NormalizedUserName = "IMSHR",
                    NormalizedEmail = "IMSHR@GMAIL.COM",
                    PhoneNumber = "1112223333",
                    Address = "HaNoi",
                    FullName = "HR IMS",
                    DepartmentId = hrDepartmentId
                }, "Admin123@").GetAwaiter().GetResult();

                ApplicationUser hrUser = _context.ApplicationUsers.FirstOrDefault(u => u.Email == "imsHR@gmail.com");
                _userManager.AddToRoleAsync(hrUser, SD.Role_HR).GetAwaiter().GetResult();

                _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "IMSInterviewer",
                    Email = "IMSInterviewer@gmail.com",
                    NormalizedUserName = "IMSINTERVIEWER",
                    NormalizedEmail = "IMSINTERVIEWER@GMAIL.COM",
                    PhoneNumber = "1112223333",
                    Address = "HaNoi",
                    FullName = "Interviewer IMS",
                    DepartmentId = interviewerDepartment
                }, "Admin123@").GetAwaiter().GetResult();

                ApplicationUser user = _context.ApplicationUsers.FirstOrDefault(u => u.Email == "IMSInterviewer@gmail.com");
                _userManager.AddToRoleAsync(user, SD.Role_Interviewer).GetAwaiter().GetResult();

                _userManager.CreateAsync(new ApplicationUser
                {
                    UserName = "IMSManager",
                    Email = "IMSManager@gmail.com",
                    NormalizedUserName = "IMSMANAGER",
                    NormalizedEmail = "IMSMANAGER@GMAIL.COM",
                    PhoneNumber = "1112223333",
                    Address = "HaNoi",
                    FullName = "Manager IMS",
                    DepartmentId = managerDepartment
                }, "Admin123@").GetAwaiter().GetResult();

                ApplicationUser managerUser = _context.ApplicationUsers.FirstOrDefault(u => u.Email == "IMSManager@gmail.com");
                _userManager.AddToRoleAsync(managerUser, SD.Role_Manager).GetAwaiter().GetResult();
            }

            _context.SaveChangesAsync();
            return;

        }

    }
}
