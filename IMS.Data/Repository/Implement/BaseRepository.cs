﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace IMS.Data
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected IMSContext _dbContext;

        protected readonly DbSet<T> _dbSet;
        public DbSet<T> DbSet => _dbSet;

        public BaseRepository(IMSContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public virtual void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        public virtual void Add(IEnumerable<T> entities)
        {
            _dbSet.AddRange(entities);
        }

        public virtual void Delete(T entity, bool isHardDelete = false)
        {
            if (isHardDelete)
            {
                _dbSet.Remove(entity);
            }
            else
            {
                entity.IsDeleted = true;
                _dbSet.Entry(entity).State = EntityState.Modified;
            }
        }

        public virtual void Delete(IEnumerable<T> entities, bool isHardDelete = false)
        {
            if (isHardDelete)
            {
                _dbSet.RemoveRange(entities);
            }
            else
            {
                foreach (var entity in entities)
                {
                    entity.IsDeleted = true;
                }
            }
        }

        public virtual void Delete(Expression<Func<T, bool>> where, bool isHardDelete = false)
        {
            var entities = GetQuery(where).AsEnumerable();

            Delete(entities, isHardDelete);
        }

        public virtual T? GetById(Guid id, string includeProperties = "")
        {
            IQueryable<T> query = _dbSet;
            foreach (var includeProperty in
                includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return query.FirstOrDefault(x => x.Id == id);
        }

        public virtual async Task<T?> GetByIdAsync(Guid id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual IQueryable<T> GetQuery(Expression<Func<T, bool>> where)
        {
            return _dbSet.Where(where);
        }

        public virtual void Update(T entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Modified)
            {
                _dbContext.Entry(entity).State = EntityState.Modified;
            }
            _dbSet.Update(entity);
        }

        public virtual IQueryable<T> Get(Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            string includeProperties = "", bool isGetDeleted = false)
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            // không lấy các phần tử chưa bị xóa mềm
            if (isGetDeleted == false)
            {
                query = query.Where(x => x.IsDeleted == isGetDeleted);
            }

            foreach (var includeProperty in
                includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return orderBy != null ? orderBy(query) : query;
        }

        public virtual IQueryable<T> GetInclude(Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            bool isGetDeleted = false,
            params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            // không lấy các phần tử chưa bị xóa mềm
            if (isGetDeleted == false)
            {
                query = query.Where(x => x.IsDeleted == isGetDeleted);
            }

            // Include other properties
            foreach (var include in includes)
            {
                query = query.Include(include);
            }

            return orderBy != null ? orderBy(query) : query;
        }

        public virtual IQueryable<T> GetByPage(Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            string includeProperties = "", bool isGetDeleted = false, int page = 1, int size = 10)
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            // không lấy các phần tử chưa bị xóa mềm
            if (isGetDeleted == false)
            {
                query = query.Where(x => x.IsDeleted == isGetDeleted);
            }

            foreach (var includeProperty in
                includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }
            return query.Skip(size * (page - 1)).Take(size).ToList().AsQueryable();
        }

    }
}
