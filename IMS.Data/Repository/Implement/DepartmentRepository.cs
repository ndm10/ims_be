﻿using IMS.Data.Repository.Interface;

namespace IMS.Data.Repository.Implement
{
    public class DepartmentRepository : BaseRepository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(IMSContext dbContext) : base(dbContext)
        {

        }
    }
}
