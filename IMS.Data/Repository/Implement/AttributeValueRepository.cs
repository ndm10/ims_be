﻿
namespace IMS.Data;

public class AttributeValueRepository : BaseRepository<AttributeValue>, IAttributeValueRepository
{
    public AttributeValueRepository(IMSContext dbContext) : base(dbContext)
    {

    }


    public List<AttributeValue> GetByIds(List<Guid> ids)
    {
        return _dbSet.Where(a => ids.Contains(a.Id)).ToList();
    }

    public List<Guid> GetNonExistentIds(List<Guid> ids)
    {
        // Lấy những id tồn tại trong _dbSet
        var existingIds = _dbSet.Where(a => ids.Contains(a.Id)).Select(a => a.Id).ToList();

        // So sánh để lấy ra những id không tồn tại trong existingIds
        var nonExistentIds = ids.Except(existingIds).ToList();

        return nonExistentIds;
    }

    public List<string> GetNonExistentNames(List<string> names)
    {
        // Lấy những id tồn tại trong _dbSet
        var existingNames = _dbSet.Where(a => names.Contains(a.Value)).Select(a => a.Value).ToList();

        // So sánh để lấy ra những id không tồn tại trong existingIds
        var nonExistentNames = names.Except(existingNames).ToList();

        return nonExistentNames;
    }
}
