﻿
using Microsoft.EntityFrameworkCore;

namespace IMS.Data;

public class CandidateRepository : BaseRepository<Candidate>, ICandidateRepository
{
    public CandidateRepository(IMSContext dbContext) : base(dbContext)
    {

    }

    public async Task<List<Candidate>> GetAllAsync()
    {
        return await _dbSet.ToListAsync();
    }
}
