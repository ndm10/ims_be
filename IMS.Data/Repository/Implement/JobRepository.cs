﻿namespace IMS.Data
{
    public class JobRepository : BaseRepository<Job>, IJobRepository
    {
        public JobRepository(IMSContext dbContext) : base(dbContext)
        {
        }
    }
}
