﻿using IMS.Data.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Data.Repository.Implement
{
    public class ApplicationUserRepository : IApplicationUserRepository
    {
        protected IMSContext _dbContext;
        protected readonly DbSet<ApplicationUser> _dbSet;

        public ApplicationUserRepository(IMSContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<ApplicationUser>();
        }

        public ApplicationUser? GetById(Guid id, string includeProperties = "")
        {
            IQueryable<ApplicationUser> query = _dbSet;
            foreach (var includeProperty in
                includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return query.FirstOrDefault(x => x.Id == id);
        }
    }
}
