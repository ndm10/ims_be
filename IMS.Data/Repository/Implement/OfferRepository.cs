﻿namespace IMS.Data
{
    public class OfferRepository : BaseRepository<Offer>, IOfferRepository
    {
        public OfferRepository(IMSContext dbContext) : base(dbContext)
        {
        }

    }
}
