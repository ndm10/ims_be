﻿using IMS.Data.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace IMS.Data.Repository.Implement
{
    public class ScheduleRepository : BaseRepository<Schedule>, IScheduleRepository
    {
        public ScheduleRepository(IMSContext dbContext) : base(dbContext)
        {

        }

        public Schedule? FindByCandidateAndJob(Guid candidateId, Guid jobId)
        {
            return _dbContext.Schedules.FirstOrDefault(p => p.CandidateId == candidateId && p.JobId == jobId);
        }

        public Schedule? FindById(Guid id)
        {
            return _dbContext.Schedules
             .Include(s => s.Interviewers)
             .Include(s => s.Candidate)
             .Include(s => s.Job)
             .Include(s => s.RecruiterOwner)
             .SingleOrDefault(s => s.Id == id);
        }

        public List<Schedule> GetSchedules()
        {
            return _dbContext.Schedules.ToList();
        }

        public bool IsOverLapping(Expression<Func<Schedule, bool>> filter, DateOnly scheduleDate, TimeOnly startTime,
            TimeOnly endTime)
        {
            return _dbContext.Schedules.Where(filter).Any(p => startTime < p.StartTime && p.StartTime < endTime && p.ScheduleDate == scheduleDate);
        }
    }
}
