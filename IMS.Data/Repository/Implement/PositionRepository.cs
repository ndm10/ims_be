﻿namespace IMS.Data;

public class PositionRepository : BaseRepository<Position>, IPositionRepository
{
    public PositionRepository(IMSContext dbContext) : base(dbContext)
    {

    }
}
