﻿namespace IMS.Data;

public interface ICandidateRepository : IBaseRepository<Candidate>
{
    /// <summary>
    /// Get all candidates
    /// </summary>
    /// <returns>List candidates</returns>
    Task<List<Candidate>> GetAllAsync();
}
