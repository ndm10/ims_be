﻿namespace IMS.Data.Repository.Interface
{
    public interface IDepartmentRepository : IBaseRepository<Department>
    {
    }
}
