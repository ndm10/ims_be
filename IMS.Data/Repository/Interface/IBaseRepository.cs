﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace IMS.Data
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        DbSet<T> DbSet { get; }
        T? GetById(Guid id, string includeProperties = "");

        Task<T?> GetByIdAsync(Guid id);

        void Add(T entity);

        void Add(IEnumerable<T> entities);

        void Update(T entity);

        void Delete(T entity, bool isHardDelete = false);

        void Delete(IEnumerable<T> entities, bool isHardDelete = false);

        /// <summary>
        /// Xóa theo điều kiện
        /// </summary>
        /// <param name="where"></param>
        /// <param name="isHardDelete"></param>
        void Delete(Expression<Func<T, bool>> where, bool isHardDelete = false);

        IQueryable<T> GetQuery(Expression<Func<T, bool>> where);

        /// <summary>
        /// Lấy danh sách thực thể theo điều kiện
        /// dùng cho những bảng ít phần tử và ít bị thay đổi
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties"></param>
        /// <param name="canLoadDeleted"></param>
        /// <returns></returns>
        IQueryable<T> Get(Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            string includeProperties = "", bool isGetDeleted = false);

        /// <summary>
        /// Lấy danh sách thực thể theo điều kiện
        /// dùng cho những bảng ít phần tử và ít bị thay đổi
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="isGetDeleted"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        IQueryable<T> GetInclude(Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            bool isGetDeleted = false, params Expression<Func<T, object>>[] includes);

        /// <summary>
        /// lấy danh sách thưc thể theo điều kiện và phân trang
        /// dùng cho những bảng nhiều dữ liệu, dữ liệu thay đổi nhiều
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties"></param>
        /// <param name="isGetDeleted"></param>
        /// <param name="size"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        IQueryable<T> GetByPage(Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            string includeProperties = "", bool isGetDeleted = false, int page = 1, int size = 10);
    }
}
