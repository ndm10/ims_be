﻿using System.Security.Claims;

namespace IMS.Data
{
    public interface ITokenRepository
    {
        string CreateJwtToken(ApplicationUser user, List<string> roles);
        ClaimsPrincipal? GetPrincipalFromExpiredToken(string? token);
    }
}
