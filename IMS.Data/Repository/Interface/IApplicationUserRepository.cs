﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMS.Data.Repository.Interface
{
    public interface IApplicationUserRepository
    {
        ApplicationUser? GetById(Guid id, string includeProperties = "");
    }
}
