﻿
using System.Linq.Expressions;

namespace IMS.Data.Repository.Interface
{
    public interface IScheduleRepository : IBaseRepository<Schedule>
    {
        public Schedule? FindByCandidateAndJob(Guid candidateId, Guid jobId);
        public Schedule? FindById(Guid id);
        public List<Schedule> GetSchedules();
        public bool IsOverLapping(Expression<Func<Schedule, bool>> filter, DateOnly scheduleDate, TimeOnly startTime,
            TimeOnly endTime);
    }
}
