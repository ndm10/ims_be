﻿namespace IMS.Data;

public interface IAttributeValueRepository : IBaseRepository<AttributeValue>
{
    /// <summary>
    /// get attribute value by list id
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    List<AttributeValue> GetByIds(List<Guid> ids);

    /// <summary>
    /// get attribute value by list value
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    //List<AttributeValue> GetByValues(List<string> values);


    /// <summary>
    /// get all id is not exist in database of list ids
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    List<Guid> GetNonExistentIds(List<Guid> ids);

    /// <summary>
    /// get all name is not exist in database of list names
    /// </summary>
    /// <param name="names"></param>
    /// <returns></returns>
    List<string> GetNonExistentNames(List<string> names);
}
